#!/usr/bin/env python3

import sys
import music
import numpy as np
import os
import matplotlib.pyplot as plt
import nest
from tools import generate_click_task_data

from grid_environments import GridWorld, WaterMaze
from parameters import *


symm = 1
training = 1

if task == "miniDebug":
    nvp = 1
    n_episodes = 2
    n_batch = 3
    n_trial_moves = 4
    seq_len = 10
    L = 3
    n_in = 2*L
    n_adaptive = 36
    n_regular = 36
    n_out = 4
    V_th = 0.2

elif task == "Debug":
    nvp = 1
    n_episodes = 10
    n_batch = 5
    n_trial_moves = 25
    seq_len = 20
    L = 3
    n_in = 2*L
    n_adaptive = 36
    n_regular = 36
    n_out = 4
    V_th = 0.2

elif task == "GridWorld":
    nvp = 1
    n_episodes = 100#250
    n_batch = 64
    n_trial_moves = 180
    seq_len = 20
    L = 9
    n_in = 2*L
    n_adaptive = 36
    n_regular = 36
    n_out = 4
    V_th = 0.2

elif task == "WaterMaze":
    nvp = 1
    n_episodes = 150#250
    n_batch = 64
    n_trial_moves = 180
    seq_len = 20
    L = 9
    n_in = 2*L
    n_adaptive = 36
    n_regular = 36
    n_out = 4
    V_th = 0.2


elif task == "Pong":
    nvp = 1
    n_episodes = 42#250
    n_batch = 64
    n_trial_moves = 180
    seq_len = 20
    L = 10
    H = 9
    n_in = 4 + (L+H) + (L-2)
    n_adaptive = 36
    n_regular = 36
    n_out = 3
    V_th = 0.03

elif task == "CartPole":
    nvp = 1
    n_episodes = 35#100
    n_batch = 64
    n_trial_moves = 200
    seq_len = 30

    dim = 4
    n_in_d = 100
    n_in = n_in_d * dim

    n_adaptive = 64
    n_regular = 64

    n_out = 2
    V_th = 0.1

# This should be here, right before the following if statements
affix = f'{task}_epis_{n_episodes}_bs_{n_batch}'

if training:
    learning_rate = 0.01
    # if True, input, target, and weights are loaded from files that were created in a previous simulation
    cont_simulation = False
    # if True, the weight matrices after training are saved as npy files and the input and target are saved as a json file
    save_weights = True

else:
    learning_rate = 0.
    cont_simulation = 1
    save_weights = False
    n_episodes = 1
    n_batch = 1



'''
flags
'''
# if True, the membrane potential of the recurrent neurons is recorded
record_rec_nrns = False

'''
parameters
'''
# === task ===
# length of time window for the network to make a decision
recall_duration = 1.
#filtering_duration 
filtering_duration = 4.
# firing rate of frozen input noise in Hz
f0 = 900.
# firing rate of frozen input noise in kHz
input_f0 = f0 / 1000.
# starting point of recall
recall_start = float(seq_len - recall_duration)
filtering_start = float(seq_len - filtering_duration)

# === simulation parameters ===
# fix numpy random seed
seed = 302687
np.random.seed(seed)
# number of simulation steps
sim_steps = n_episodes*n_batch*n_trial_moves*seq_len
# temporal resolution of the simulation
resolution = 1.
# dendritic delay of all synapses
delay = resolution

# === neurons  ===
# membrane time constant of recurrent neuron
#tau_v = 5.
tau_v = 10.
# membrane time constant of output neuron
#tau_out = 5.
tau_out = 10.
# number of refractory time steps
#n_ref = 5
n_ref = 2
# membrane capacitance
C_m = tau_v
# reset value for V_m after a spike
V_reset = 0.
# leak reversal potential
E_L = 0.
# duration of refractory period
t_ref = n_ref*resolution
# leak conductance
g_L = C_m / tau_v
# external current input
I_e = 0.
# initial value of the membrane potential
V_m = 0.

# === adam optimizer ===
# if >= 1. use adam optimizer
use_adam = 1.
#use_adam = 0.
# exponential decay rate for the first moment estimate
beta1 = 0.9
# exponential decay rate for the second moment estimate
beta2 = 0.999
# small constant used for numerical stabilization
epsilon = 1.e-8

# === adaptation ===
# time constant of the adaptive threshold
#tau_a = 200.
tau_a = 2000.
# prefactor of the adaptive threshold
beta_a = 1.7 * (1. - np.exp(-1 / tau_a)) / (1. - np.exp(-1. / tau_v))

# ==== regularization ===
# prefactor of firing rate regularization
reg_f = 1.
# target firing rate for rate regularization
reg_rate = 10.
# regulariztion rate
regularization_f0 = reg_rate / 1000.

# === network ===
# number of total recurrent neurons
n_rec = n_adaptive + n_regular
# number of input neurons
n_critic = 1  ##############

# === plasticity ===
# interval for updating the synapse weights
#update_interval = float(seq_len) #float(seq_len*n_trial_moves)
update_interval = float(seq_len*n_trial_moves)
# if True learning_type is performed, if False classification
learning_type = 3
# RL gamma
rl_gamma = 1.
# if <= 1. the membrane potentials and spike trains are resetted after each example
keep_traces = 0.

'''
path where weight matrices are stored
'''
datapath = './nest_weights/'
if not os.path.exists(datapath):
    os.mkdir(datapath)




def _extract_weight_matrix(pop_pre, pop_post):
    '''
    returns weight matrix W_ij of connection between pop_pre and pop_post, where the first index i corresponds to the
    target and j to the source.  The shape if the returnd matrix is (len(pop_post), len(pop_pre)).
    '''
    conns_pre_post = nest.GetConnections(pop_pre, pop_post)
    sources = np.array(list(conns_pre_post.sources()))
    targets = np.array(list(conns_pre_post.targets()))
    weights = np.array(conns_pre_post.get('weight'))
    weight_matrix = np.zeros((len(pop_post), len(pop_pre)))
    pop_pre_list = pop_pre.tolist()
    pop_post_list = pop_post.tolist()

    for (s, t, w) in zip(sources, targets, weights):
        j = np.where(np.array(pop_pre_list) == s)[0][0]
        i = np.where(np.array(pop_post_list) == t)[0][0]
        weight_matrix.itemset(i, j, w)

    return weight_matrix


def get_weights(in_nrns, rec_nrns, out_nrns, critic_nrns):
    in_rec = _extract_weight_matrix(in_nrns, rec_nrns)
    rec_rec = _extract_weight_matrix(rec_nrns, rec_nrns)
    rec_out = _extract_weight_matrix(rec_nrns, out_nrns)
    rec_critic = _extract_weight_matrix(rec_nrns, critic_nrns)

    print(np.linalg.norm(in_rec), np.linalg.norm(rec_rec), np.linalg.norm(rec_out), np.linalg.norm(rec_critic))


def save_current_weights(in_nrns, rec_nrns, out_nrns, critic_nrnsc, path='./', affix=''):
    '''
    save weight matrices as npy files.
    '''
    if not path[-1] == "/":
        path += "/"

    in_rec = _extract_weight_matrix(in_nrns, rec_nrns)
    rec_rec = _extract_weight_matrix(rec_nrns, rec_nrns)
    rec_out = _extract_weight_matrix(rec_nrns, out_nrns)
    rec_cri = _extract_weight_matrix(rec_nrns, critic_nrns)
    out_rec = _extract_weight_matrix(out_nrns, rec_nrns)
    cri_rec = _extract_weight_matrix(critic_nrns, rec_nrns)

    np.save(path + 'in_weights_' + affix, in_rec)
    print('wrote weights in->rec: {}'.format(path + 'in_weights_' + affix))
    np.save(path + 'rec_weights_' + affix, rec_rec)
    print('wrote weights rec->rec: {}'.format(path + 'rec_weights_' + affix))
    np.save(path + 'out_weights_' + affix, rec_out)
    print('wrote weights rec->out: {}'.format(path + 'out_weights_' + affix))
    np.save(path + 'cri_weights_' + affix, rec_cri)
    print('wrote weights rec->critic: {}'.format(path + 'cri_weights_' + affix))

    np.save(path + 'feedback_out_weights_' + affix, out_rec)
    print('wrote feedback_out_weights: {}'.format(path + 'feedback_out_weights_' + affix))
    np.save(path + 'feedback_cri_weights_' + affix, cri_rec)
    print('wrote critic broadcast weights: {}'.format(path + 'feedback_cri_weights_' + affix))



def reshape_tf_data(data):
    '''
    Reshape 3D arrays of dimensions (n_batch, seq_len, n_in) to (n_in, n_batch*seq_len) and 2D arrays of dimensions
    (nbatch, seq_len) to (nbatch*seq_len).
    '''
    tf_shape = np.shape(data)
    if len(tf_shape) == 2:
        reshaped_data = np.array(data.reshape(tf_shape[0]*tf_shape[1]), dtype=int)

    elif len(tf_shape) == 3:
        reshaped_data = np.array(
            np.swapaxes(np.swapaxes(data, 0, 1), 0, 2).reshape(tf_shape[2], tf_shape[0]*tf_shape[1]), dtype=int)
    return reshaped_data


def get_data(n_batch):
    '''
    Generate input spikes, sequence of active populations and targets of the evidence accumulation task.
    '''
    input_spikes, input_nums, target_nums, _ = \
        generate_click_task_data(batch_size=n_batch, seq_len=seq_len, n_neuron=n_in, recall_duration=int(filtering_duration),
                                 p_group=p_group, t_cue=t_cue, n_cues=n_cues, t_interval=t_cue_spacing, f0=input_f0,
                                 n_input_symbols=n_input_symbols)
    return reshape_tf_data(input_spikes), reshape_tf_data(input_nums), reshape_tf_data(target_nums)


'''
reset NEST kernel
'''
nest.ResetKernel()
nest.SetKernelStatus({'resolution': resolution, 'total_num_virtual_procs': nvp, 'print_time': True})

'''
neuron parameters
'''
neuron_params_adaptive = {
    'C_m': C_m,
    't_ref': t_ref,
    'V_reset': V_reset,
    'E_L': E_L,
    'tau_m': tau_v,
    'beta': beta_a,
    'tau_a': tau_a,
    'V_m': V_m,
    'V_th': V_th,
    'learning_type': learning_type,
    'update_interval': float(seq_len), # OJOOOO
    'batch_elem_interval': seq_len * n_trial_moves,
    }

neuron_params_regular = {
    'C_m': C_m,
    'tau_m': tau_v,
    't_ref': t_ref,
    'E_L': E_L,
    'V_reset': V_reset,
    'V_m': V_m,
    'V_th': V_th,
    'learning_type': learning_type,
    'update_interval': float(seq_len),
    'batch_elem_interval': seq_len * n_trial_moves,
    }

readout_params = {
    'C_m': C_m,
    'tau_m': tau_out,
    'E_L': E_L,
    'I_e': I_e,
    'V_m': V_m,
    'start': recall_start,
    'start_filtering': filtering_start,
    'ce': 2.0,
    'ce_decay': 500.,
    'learning_type': learning_type,
    'update_interval': float(seq_len),
    'batch_elem_interval': seq_len * n_trial_moves,
    'iteration_interval': seq_len * n_batch * n_trial_moves,
#    'batch_size': float(n_batch),
    }

critic_params = {
    'C_m': C_m,
    'tau_m': tau_out,
    'E_L': E_L,
    'I_e': I_e,
    'V_m': V_m,
    'start': recall_start,
    'start_filtering': filtering_start,
    'cv': 1.,
    'rl_gamma': rl_gamma,
    'learning_type': learning_type,
    'update_interval': float(seq_len),
    'batch_elem_interval': seq_len * n_trial_moves,
#    'batch_elem_interval': seq_len * n_trial_moves,
#    'batch_size': float(n_batch),
    }



'''
draw random weights
'''
if not cont_simulation:
    out_weights = np.random.randn(n_rec,n_out).T / np.sqrt(n_rec)
    critic_weights = np.random.randn(n_rec,1).T / np.sqrt(n_rec)

    in_weights = np.random.randn(n_in, n_rec).T / np.sqrt(n_in)
    rec_weights = np.random.randn(n_rec, n_rec).T / np.sqrt(n_rec)
    np.fill_diagonal(rec_weights, 0.)  # set diagonal to zero (no autapses)
    rand_feedback_weights = np.random.randn(n_rec, n_out)
    rand_critic_feedback_weights = np.random.randn(n_rec, n_critic)
    #--------------------------

#    out_weights = 0.1*np.ones((n_rec,n_out)).T 
#    critic_weights = 0.1*np.ones((n_rec,1)).T 
#
#    in_weights = 0.1*np.ones((n_in, n_rec)).T 
#    rec_weights = 0.1*np.ones((n_rec, n_rec)).T 
#    np.fill_diagonal(rec_weights, 0.)  # set diagonal to zero (no autapses)
#    rand_feedback_weights = 0.1*np.ones((n_rec, n_out))
#    rand_critic_feedback_weights = 0.1*np.ones((n_rec, n_critic))

else:
    in_weights = np.load(datapath + 'in_weights_' + affix + '.npy', allow_pickle=True)
    rec_weights = np.load(datapath + 'rec_weights_' + affix + '.npy', allow_pickle=True)
    out_weights = np.load(datapath + 'out_weights_' + affix + '.npy', allow_pickle=True)
    critic_weights = np.load(datapath + 'cri_weights_' + affix + '.npy', allow_pickle=True)
    rand_feedback_weights = np.load(datapath + 'feedback_out_weights_' + affix + '.npy', allow_pickle=True)
    rand_critic_weights = np.load(datapath + 'feedback_cri_weights_' + affix + '.npy', allow_pickle=True)

'''
draw spikes and targets for task
'''

#input_spikes_all = np.zeros((n_in, sim_steps))
#target_nums_all = np.zeros((n_batch*n_episodes))
#target_rates = np.zeros((n_out, sim_steps))
#
#for iteration in np.arange(n_episodes):
#    start = iteration*n_batch
#    end = (iteration + 1)*n_batch
#
#    input_spikes, input_nums, target_nums = get_data(n_batch)
#    input_spikes_all[:, start*seq_len:end*seq_len] = input_spikes
#    target_nums_current = target_nums[::seq_len]
#    target_nums_all[start:end] = target_nums_current
#
#    for batch, target_num in enumerate(target_nums_current):
#        target_rates[target_num, (start + batch)*seq_len:(start + batch + 1)*seq_len] = 1.
#
#spike_generator_dicts = []
#for in_idx in range(n_in):
#    spks = input_spikes_all[in_idx]
#    spktimes = np.array(np.unique(spks*np.arange(sim_steps))[1:], dtype=np.float32)
#    spike_generator_dicts.append({'spike_times': spktimes})

#spike_generators = nest.Create('spike_generator', n_in)
#nest.SetStatus(spike_generators, spike_generator_dicts)

'''
create neurons, generators and devices
'''

nest.SetKernelStatus({"action_space_dimension" : n_out})
in_nrns = nest.Create('parrot_neuron', n_in)
neurons_reg = nest.Create('iaf_psc_delta_eprop', n_regular, params=neuron_params_regular)
neurons_ad = nest.Create('aif_psc_delta_eprop', n_adaptive, params=neuron_params_adaptive)
rec_nrns = neurons_reg + neurons_ad
out_nrns = nest.Create('actor_neuron', n_out, params=readout_params)
for actor_id, neuron in enumerate(out_nrns):
    nest.SetStatus(neuron, {"actor_id": actor_id})
critic_nrns = nest.Create('critic_neuron', n_critic, params=critic_params)

print("in:", in_nrns.get()["global_id"])
print("rec:", rec_nrns.get()["global_id"])
print("act:", out_nrns.get()["global_id"])
print("cri:", critic_nrns.get()["global_id"])



music_reward_in = nest.Create("music_rate_in_proxy", 1, params = {'port_name': 'net_rwrd_in'})
music_done_in = nest.Create("music_rate_in_proxy", 1, params = {'port_name': 'net_done_in'})
music_in = nest.Create("music_event_in_proxy", n_in, params = {'port_name': 'net_in'})
music_out = nest.Create('music_event_out_proxy', 1, params = {'port_name':'net_out'})
for channel, n in enumerate(music_in):
    nest.SetStatus(n, {'music_channel': channel})
nest.SetAcceptableLatency('net_in', resolution)


mm_out_params = {'interval': resolution, 'record_from': ['V_m', 'len_eprop_hist', 'learning_signal']}
mm_critic_params = {'interval': resolution, 'record_from': ['V_m']}

if record_rec_nrns:
    mm_rec = nest.Create("multimeter", params=mm_out_params)
    sd = nest.Create('spike_detector')
    sd_in = nest.Create('spike_detector')

mm_out = nest.Create('multimeter', params=mm_out_params)
mm_critic = nest.Create('multimeter', params=mm_critic_params)

'''
create connections
'''

def create_Wmin_Wmax(weight_matrix):
    Wmin = np.zeros_like(weight_matrix)
    Wmax = np.zeros_like(weight_matrix)

    signs = np.sign(weight_matrix)

    Wmin[np.where(signs == -1.)] = -100.
    Wmax[np.where(signs == 1.)] = 100.
    return Wmin, Wmax


Wmin_in, Wmax_in = create_Wmin_Wmax(in_weights)
Wmin_rec, Wmax_rec = create_Wmin_Wmax(rec_weights)
Wmin_out, Wmax_out = create_Wmin_Wmax(out_weights)
Wmin_critic, Wmax_critic = create_Wmin_Wmax(critic_weights)


synapse_in = {'synapse_model': 'eprop_synapse', 'learning_rate': learning_rate, 'weight': in_weights, 'delay': delay,
              'update_interval': update_interval, 'tau_decay': tau_out, 'keep_traces': keep_traces,
              'target_firing_rate': reg_rate, 'rate_reg': reg_f, 'batch_size': float(n_batch), 'use_adam': use_adam,
              'beta1_adam': beta1, 'beta2_adam': beta2, 'epsilon_adam': epsilon, 'recall_duration': recall_duration,
              'Wmin': Wmin_in, 'Wmax': Wmax_in, 'learning_type': learning_type, 'move_interval':seq_len, 'rl_gamma': rl_gamma}
synapse_rec = {'synapse_model': 'eprop_synapse', 'learning_rate': learning_rate, 'weight': rec_weights, 'delay': delay,
               'update_interval': update_interval, 'tau_decay': tau_out, 'keep_traces': keep_traces,
               'target_firing_rate': reg_rate, 'rate_reg': reg_f, 'batch_size': float(n_batch), 'use_adam': use_adam,
               'beta1_adam': beta1, 'beta2_adam': beta2, 'epsilon_adam': epsilon, 'recall_duration': recall_duration,

               'Wmin': Wmin_rec, 'Wmax': Wmax_rec, 'learning_type': learning_type, 'move_interval':seq_len, 'rl_gamma': rl_gamma}
synapse_out = {'synapse_model': 'eprop_synapse', 'weight': out_weights, 'delay': delay,
            'update_interval': update_interval, 'tau_decay': tau_out, 'keep_traces': keep_traces, 'learning_rate': learning_rate,
               'batch_size': float(n_batch), 'use_adam': use_adam, 'beta1_adam': beta1, 'beta2_adam': beta2,
               'epsilon_adam': epsilon, 'recall_duration': recall_duration,
               'Wmin': Wmin_out, 'Wmax': Wmax_out, 'learning_type': learning_type, 'move_interval':seq_len, 'rl_gamma': rl_gamma}
synapse_critic = {'synapse_model': 'eprop_synapse', 'weight': critic_weights, 'delay': delay,
            'update_interval': update_interval, 'tau_decay': tau_out,'keep_traces': keep_traces, 'learning_rate': learning_rate,
               'batch_size': float(n_batch), 'use_adam': use_adam, 'beta1_adam': beta1, 'beta2_adam': beta2,
               'epsilon_adam': epsilon, 'recall_duration': recall_duration,
               'Wmin': Wmin_critic, 'Wmax': Wmax_critic, 'learning_type': learning_type, 'move_interval':seq_len, 'rl_gamma': rl_gamma}



synapse_broadcast = {'synapse_model': 'learning_signal_connection_delayed', 'weight': out_weights.T,
                     'delay': delay}
synapse_broadcast_critic = {'synapse_model': 'learning_signal_connection_delayed', 'weight': critic_weights.T,
                     'delay': delay}
synapse_out_out = {'synapse_model': 'rate_connection_delayed', 'weight': 1., 'delay': delay,
                   'receptor_type':1}
#synapse_target = {'synapse_model': 'rate_connection_delayed', 'weight': 1., 'delay': delay,
#                  'receptor_type':2}
static_synapse = {'synapse_model': 'static_synapse', 'weight': 1., 'delay': delay}
out_critic_synapse = {'synapse_model': 'critique_request_connection_delayed', 'weight': 1., 'delay': delay}
critic_out_synapse = {'synapse_model': 'critique_feedback_connection_delayed', 'weight': 1., 'delay': delay}
reward_synapse = {'synapse_model': 'rate_connection_instantaneous', 'weight': 1., 'receptor_type': 0}
done_synapse = {'synapse_model': 'rate_connection_instantaneous', 'weight': 1., 'receptor_type': 1}

# NEST does not support a direct connection of devises to neurons via plastic synapses. This is why
# we have the parrot neurons between the spike generators and the recurrent network. Parrot neurons
# simply send a spike whenever they are receiving one.
conn_ata = {'rule': 'all_to_all', 'allow_autapses': False}
conn_oto = {'rule': 'one_to_one'}

srecorder = nest.Create('spike_recorder') # DEBUG
nest.Connect(in_nrns, srecorder, syn_spec=static_synapse) # DEBUG

nest.Connect(music_in, in_nrns, syn_spec=static_synapse, conn_spec=conn_oto)
nest.Connect(in_nrns, rec_nrns, syn_spec=synapse_in, conn_spec=conn_ata)
nest.Connect(rec_nrns, rec_nrns, syn_spec=synapse_rec, conn_spec=conn_ata)
nest.Connect(rec_nrns, out_nrns, syn_spec=synapse_out, conn_spec=conn_ata)
nest.Connect(rec_nrns, critic_nrns, syn_spec=synapse_critic, conn_spec=conn_ata) ###############
nest.Connect(out_nrns, rec_nrns, syn_spec=synapse_broadcast, conn_spec=conn_ata)
nest.Connect(critic_nrns, rec_nrns, syn_spec=synapse_broadcast_critic, conn_spec=conn_ata)
nest.Connect(out_nrns, out_nrns, syn_spec=synapse_out_out, conn_spec=conn_ata)
nest.Connect(out_nrns, critic_nrns, syn_spec=out_critic_synapse, conn_spec=conn_ata) #################
nest.Connect(critic_nrns, out_nrns, syn_spec=critic_out_synapse, conn_spec=conn_ata) #################

nest.Connect(music_reward_in, out_nrns, syn_spec=reward_synapse, conn_spec=conn_ata)
nest.Connect(music_reward_in, critic_nrns, syn_spec=reward_synapse, conn_spec=conn_oto)
nest.Connect(music_done_in, out_nrns, syn_spec=done_synapse, conn_spec=conn_ata)
nest.Connect(music_done_in, critic_nrns, syn_spec=done_synapse, conn_spec=conn_oto)

for channel, neuron in enumerate(out_nrns):
    nest.Connect(neuron, music_out, "one_to_one", {'music_channel': channel})

#sg_params = {'spike_times': np.array([1.,2.,4.,8.,40.,43.,44.,60.,61.,62.,80.,81.,82.])}
#spike_generator = nest.Create('spike_generator', 1, params=sg_params)
#for nid, neuron in zip(in_nrns.tolist(), in_nrns):
#    if nid == 6:
#        nest.Connect(spike_generator, neuron, syn_spec=static_synapse)
##exit(0)

#sg_params.append({'spike_times': spktimes})
#nest.SetStatus(spike_generators, sg_params)



#nest.Connect(out_nrns, srecorder)
#nest.Connect(reward_generator, critic_nrns, syn_spec=reward_synapse)

#for i in range(n_out):
#    nest.Connect(target_generators[i], out_nrns[i], syn_spec=synapse_target)

if record_rec_nrns:
    nest.Connect(mm_rec, rec_nrns, syn_spec=static_synapse)
    nest.Connect(rec_nrns, sd, syn_spec=static_synapse)
    nest.Connect(in_nrns, sd_in, syn_spec=static_synapse)

nest.Connect(mm_out, out_nrns, syn_spec=static_synapse)
nest.Connect(mm_critic, critic_nrns, syn_spec=static_synapse) ##############

# need to reshape since nest.Connect destroys the original shape
rand_feedback_weights = np.reshape(rand_feedback_weights, (n_rec, n_out))

'''
run simulation
'''

#simTime = n_episodes * n_batch * n_trial_moves * seq_len - 4.0
#nest.Simulate(simTime) # <========================== SIM 
#exit(0)

if not symm:
    print("No symm")
    simTime = n_episodes * n_batch * n_trial_moves * seq_len - 4.0
    nest.Simulate(simTime) # <========================== SIM 
else:
#    for i_episode in range(1):
    for i_episode in range(n_episodes):
#        print(f"[Nest] epi: {i_episode}")

        episodeTime = n_batch * n_trial_moves * seq_len
        nest.Simulate(episodeTime) 

##        feedback_out_old = _extract_weight_matrix(out_nrns, rec_nrns)
##        feedback_cri_old = _extract_weight_matrix(critic_nrns, rec_nrns)

        forward_out = _extract_weight_matrix(rec_nrns, out_nrns)
        forward_cri = _extract_weight_matrix(rec_nrns, critic_nrns)


        feedback_out = []
        feedback_cri = []

        conns_feedback_out = nest.GetConnections(out_nrns, rec_nrns)
        conns_feedback_cri = nest.GetConnections(critic_nrns, rec_nrns)

        outs_fo = np.array(list(conns_feedback_out.sources()))
        recs_fo = np.array(list(conns_feedback_out.targets()))
        cris_fc = np.array(list(conns_feedback_cri.sources()))
        recs_fc = np.array(list(conns_feedback_cri.targets()))

        outs_fo = outs_fo - np.min(outs_fo)
        recs_fo = recs_fo - np.min(recs_fo)
            
        cris_fc = cris_fc - np.min(cris_fc)
        recs_fc = recs_fc - np.min(recs_fc)

        for r, o in zip(recs_fo, outs_fo): feedback_out.append( forward_out[o, r] ) 
        for r, c in zip(recs_fc, cris_fc): feedback_cri.append( forward_cri[c, r] ) 
        feedback_out = np.array(feedback_out)
        feedback_cri = np.array(feedback_cri)


        conns_feedback_out.set(weight = feedback_out)
        conns_feedback_cri.set(weight = feedback_cri)
        
if save_weights:
    print("save")
    save_current_weights(in_nrns, rec_nrns, out_nrns,
                      critic_nrns, path=datapath, affix=affix )

#        print(feedback_out, feedback_cri)
#        exit(0)

#        episodeTime = n_batch * n_trial_moves * seq_len
#        nest.Simulate(episodeTime) 
 

#        feedback_out_new = _extract_weight_matrix(out_nrns, rec_nrns)
#        feedback_cri_new = _extract_weight_matrix(critic_nrns, rec_nrns)
        
#        print(feedback_cri_old)
#        print(feedback_out_old)
#        print()
#        print(forward_cri.T)
#        print(forward_out.T)
#        print()
#        print(feedback_cri_new/feedback_cri_old)
#        print(feedback_out_new/feedback_out_old)




#_n = args.plot_freq
#_ext_seq_len = n_batch*seq_len
#targets_reshaped = target_nums_all.reshape(n_episodes, n_batch)
#tiled_targets = target_rates.reshape(n_out, n_episodes, n_batch, seq_len)[:, :, :, -int(filtering_duration):]

#print("in:", in_nrns.get()["global_id"])
#print("rec:", rec_nrns.get()["global_id"])

#-------------------------------------------------------------------
#print("out:", out_nrns.get()["global_id"])
#
#mean_latencies = []
#t = 0.
#for i_episode in range(n_episodes):
#    latencies = []
#    for batch_elem in range(n_batch):
#        done = False
#        state = env.reset() 
#        for move in range(n_trial_moves):
#            if not done:
#                advance_time = float(seq_len)
#                if i_episode==0 and batch_elem==0 and move == 0: advance_time -= 1.0
#        
#                frozen_poisson_noise_input = np.zeros((n_in, seq_len-3)) # Check out this 3
#                frozen_poisson_noise_input[state[0]] = (np.random.rand(int(seq_len-3)) < input_f0)
#                frozen_poisson_noise_input[state[1]+L] = (np.random.rand(int(seq_len-3)) < input_f0)
#                sg_params = []
#                for spks in frozen_poisson_noise_input:
#                    spktimes = np.array(np.unique(spks * np.arange(t, t + int(seq_len-3)))[1:], dtype=np.float32)
#                    sg_params.append({'spike_times': spktimes})
#                nest.SetStatus(spike_generators, sg_params)
#
#                nest.Simulate(advance_time) # <========================== SIM 
#                t += advance_time
#
#                events = nest.GetStatus(srecorder)[0]['events'] 
#                senders = events['senders']
#                out_spike_times =[events['times'][np.where(senders == i)] for i in out_nrns.tolist()]
#                hot_encoded_action = np.array([len(times[times == t]) for times in out_spike_times])
#                action = np.argmax(hot_encoded_action)
#    
#                state, reward, done, latency = env.step(action) # <================== ENV
#
#                rg_params={'amplitude_times': np.array([t + 1.0]), 'amplitude_values': np.array([reward], dtype='float')}
#                nest.SetStatus(reward_generator, rg_params)
#    
#                env.render()
#
#            else:
#                advance_time = (n_trial_moves - move)*float(seq_len)
##                print(f"DONE, batch elem: {batch_elem}, ad_t: {advance_time} latency: {latency} ",
##                                                                                file=sys.stderr)
#                nest.Simulate(advance_time) # <========================== SIM 
#                t += advance_time
#                break
#            
#        latencies.append(latency)
#    mean_latency = np.mean(latencies)
#    mean_latencies.append(mean_latency)
#    print(f'i_episode: {i_episode}, mean latency: {mean_latency}', file=sys.stderr)
#
#print(mean_latencies)
#
##fig = plt.figure()
##plt.plot(np.arange(len(mean_latencies)), mean_latencies )
##plt.show()
#-------------------------------------------------------------------

#            print("\nTime: ", t, " trial move:", move, "\nout_spike_time: ", out_spike_times) 
#            print("\nhot_enc: ", hot_encoded_action, "\naction: ", action)

#print("fatal error", file=sys.stderr)

#    '''
#    process data of recording devices
#    '''
#    times_all = np.arange(sim_steps)
#    
#    events_out = nest.GetStatus(mm_out)[0]['events']
#    events_critic = nest.GetStatus(mm_critic)[0]['events'] ###############
#    senders_out = events_out['senders']
#    
#    Vms_out = np.array([events_out['V_m'][np.where(senders_out == i)][1:1 + _sim_steps] for i in out_nrns.tolist()])
#    Vms_critic = np.array(events_critic['V_m'])
#    
#    output_logits = Vms_out.reshape(n_out, it, n_batch, seq_len)[:, :, :, -int(filtering_duration):]
#    
#    if record_rec_nrns:
#        events_rec = nest.GetStatus(mm_rec)[0]["events"]
#        senders_rec = events_rec['senders']
#        Vms_rec = [events_rec['V_m'][np.where(senders_rec == i)][1:1 + _sim_steps] for i in rec_nrns.tolist()]
#        learning_signals_rec = \
#            [events_rec['learning_signal'][np.where(senders_rec == i)][1:1 + _sim_steps] for i in rec_nrns.tolist()]
#    
#        spikes_in = nest.GetStatus(sd_in, "events")[0]
#        in_spike_times_all = np.array(spikes_in['times']) - 2.*delay
#        senders_spikes_in = spikes_in['senders']
#    
#        spikes_recurrent = nest.GetStatus(sd, "events")[0]
#        rec_spike_times_all = np.array(spikes_recurrent['times']) - 2.*delay
#        senders_spikes_rec = spikes_recurrent['senders']
#    '''
#    calculate recall errors
#    '''
#   
#    _targets_reshaped = targets_reshaped[:it]
#    _tiled_targets = tiled_targets[:, :it]
#    
#    
#    softmax_y = np.exp(output_logits) / np.sum(np.exp(output_logits), axis=0)  # sum over output neurons in denominator
#    print("soft: ", np.mean(softmax_y[:, -(_n+2):, :3, :], axis=3).T)
#    loss = - np.mean(np.sum(_tiled_targets * np.log(softmax_y), axis=0), axis=(1, 2))  # sum over output neurons, mean over
#    # recall and batches (n_episodes)
#    y_predict = np.argmax(np.mean(output_logits, axis=3), axis=0)  # average over time, argmax over neurons
#    accuracy = np.mean((_targets_reshaped == y_predict), axis=1)  # averaged over batch here
#    recall_errors = 1. - accuracy
#    print('\naccuracy: {}'.format(accuracy))
#    
#    get_weights(in_nrns, rec_nrns, out_nrns, critic_nrns)
#
#    np.save(f'./output/loss_IT_{it:04d}.npy', loss)
#    np.save(f'./output/recall_IT_{it:04d}.npy', recall_errors)
#
#    plot = args.plot
#    if plot:
#        '''
#        plot results
#        axA: recall errors over training
#        axB: loss over training
#        '''
#        lw = 3.
#        
#        fig1, (axA, axB) = plt.subplots(2, figsize=(15, 8))
#        
#        axA.plot(np.arange(len(recall_errors)), recall_errors, lw=lw)
#        axA.plot(np.arange(len(recall_errors)), 0.5*np.ones_like(recall_errors), lw=lw, c='r')
#        axA.set_xticks(np.arange(len(recall_errors)))
#        axA.set_ylim(0.,0.8)
#        axA.set_xlabel('episodes')
#        axA.set_ylabel('recall errors')
#        axA.set_yticks(np.arange(0.1,0.9,0.1))  
#        axA.yaxis.grid()
#
#        axB.plot(np.arange(len(loss)), loss, lw=lw)
#        axB.set_xticks(np.arange(len(recall_errors)))
#        axB.set_xlabel('episodes')
#        axB.set_ylabel('loss')
#        
#        fig1.tight_layout()
#        plt.savefig(f"./imgs/rl_nvp_{nvp}_nb_{n_batch}_its_{n_episodes}_cs_{n_cues}_IT_{it:04d}.png")
#        plt.close()
#    
#
#if save_weights:
#    save_current_weights(in_nrns, rec_nrns, out_nrns, critic_nrns, rand_feedback_weights, rand_critic_feedback_weights, path=datapath, affix=affix)
