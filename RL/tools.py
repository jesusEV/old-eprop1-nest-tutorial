import numpy as np
import numpy.random as rd


def generate_poisson_noise_np(prob_pattern, freezing_seed=None):
    '''
    TODO
    Generate poisson noise for evidence accumulation task
    :param prob_pattern
    :param freezing_seed
    :return spikes
    '''
    if isinstance(prob_pattern, list):
        return [generate_poisson_noise_np(pb, freezing_seed=freezing_seed) for pb in prob_pattern]

    shp = prob_pattern.shape

    spikes = prob_pattern > rd.rand(prob_pattern.size).reshape(shp)
    return spikes


def generate_click_task_data(batch_size, seq_len, n_neuron, recall_duration, p_group, f0=0.5,
                             n_cues=7, t_cue=100, t_interval=150,
                             n_input_symbols=4):
    '''
    Generate spikes and targets for evidence accumulation task
    :param batch_size: number of examples in a batch
    :param seq_len: duration of one example
    :param n_neuron: number of input neurons
    :param recall_duration: duration of the period where the network makes a decision
    :param p_group: probability of one group being active
    :param f0: firing rate of frozen input noise in Hz
    :param n_cues: overall number of cues
    :param t_cue: duration of one cue presentation
    :param t_interval: duration of one cue presentation and the consecutive break
    :param n_input_symbols: number of cues in a sequence
    :return input_spikes: (nbatch, seq_len, n_in) [False, True] - input spikes
            input_nums: (nbatch, seq_len) [0, 1, 2, 3] - indicates active population
                        [0: left, 1: right, 2:recall, 3:background noise]
            target_nums: (nbatch, seq_len) [0, 1] - indicates target 0: left, 1: right constant throughout a sequence
            target_mask: (nabtch, seq_len) [True, False] - indicates when target active
    '''
    t_seq = seq_len
    n_channel = n_neuron // n_input_symbols

    # randomly assign group A and B
    prob_choices = np.array([p_group, 1 - p_group], dtype=np.float32)
    idx = rd.choice([0, 1], batch_size)
    probs = np.zeros((batch_size, 2), dtype=np.float32)
    # assign input spike probabilities
    probs[:, 0] = prob_choices[idx]
    probs[:, 1] = prob_choices[1 - idx]

    cue_assignments = np.zeros((batch_size, n_cues), dtype=np.int)
    # for each example in batch, draw which cues are going to be active (left or right)
    for b in range(batch_size):
        cue_assignments[b, :] = rd.choice([0, 1], n_cues, p=probs[b])

    # generate input nums - 0: left, 1: right, 2:recall, 3:background noise
    input_nums = 3*np.ones((batch_size, seq_len), dtype=np.int)
    input_nums[:, :n_cues] = cue_assignments
    input_nums[:, -1] = 2

    # generate input spikes
    input_spike_prob = np.zeros((batch_size, t_seq, n_neuron))
    d_silence = t_interval - t_cue
    for b in range(batch_size):
        for k in range(n_cues):
            # input channels only fire when they are selected (left or right)
            c = cue_assignments[b, k]
            t_start, t_end = d_silence+k*t_interval, d_silence+k*t_interval+t_cue
            channel_start, channel_end = c*n_channel, (c+1)*n_channel
            input_spike_prob[b, t_start:t_end, channel_start:channel_end] = f0

    # recall cue
    input_spike_prob[:, -recall_duration:, 2*n_channel:3*n_channel] = f0
    # background noise
    input_spike_prob[:, :, 3*n_channel:] = f0/4.
    input_spikes = generate_poisson_noise_np(input_spike_prob)
    # make sure that there are no spikes in the first and last time step due to unexpected behaviour of the tf code
    input_spikes[:, 0, :] = 0
    input_spikes[:, -1, :] = 0

    # generate targets
    target_mask = np.zeros((batch_size, seq_len), dtype=np.bool)
    target_mask[:, -1] = True
    target_nums = np.zeros((batch_size, seq_len), dtype=np.int)
    target_nums[:, :] = np.transpose(np.tile(np.sum(cue_assignments, axis=1) > int(n_cues/2), (seq_len, 1)))

    return input_spikes, input_nums, target_nums, target_mask
