#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import time

class Pong:
    def __init__(self, L, H, MaxT):
        self.L = L
        self.H = H
        self.max_time = MaxT
        self.ball_coords = np.array([0,0])
        self.actions = np.array([[[-1,0],[-1,0],[-1,0]], # L
                                  [[0,0],[0,0],[0,0]],   # _
                                  [[1,0],[1,0],[1,0]]])  # R

        self.platform = np.array([[0,0],[1,0],[2,0]])
        self.curr_ball_dir = np.array([0,0])

    def reset(self):
        self.ball_coords[0] = np.random.choice(self.L)
        self.ball_coords[1] = 4 + np.random.choice(self.H-4)
        self.curr_ball_dir[0] = np.random.choice([-1,1])
        self.curr_ball_dir[1] = 1#np.random.choice([-1,1])

        self.grid = np.zeros((self.L, self.H), dtype='int')
        self.grid[tuple(self.ball_coords)] = 1
        for coords in self.platform: self.grid[tuple(coords)] = 1
        self.reward = 0
        self.t = 0
        self.num_hits = 0
        return self.return_env_state()

    def verify_new_platform_pos(self, prop_plat_pos):
        if prop_plat_pos[0,0] < 0: return False
        if prop_plat_pos[-1,0] > self.L-1: return False
        return True

    def update_platform(self, action):
        for coords in self.platform: self.grid[tuple(coords)] = 0
        prop_plat_pos = self.platform + self.actions[action]
        if self.verify_new_platform_pos(prop_plat_pos):
            self.platform = np.copy(prop_plat_pos)
        for coords in self.platform: self.grid[tuple(coords)] = 1

    def update_ball_and_status(self, action):
        reward = 1
        done = False
        recoil = False

        self.grid[tuple(self.ball_coords)] = 0
        new_dir = np.copy(self.curr_ball_dir)

        if self.ball_coords[0] == 0: new_dir[0] = 1
        if self.ball_coords[0] == self.L-1: new_dir[0] = -1
        if self.ball_coords[1] == self.H-1: new_dir[1] = -1

        if self.ball_coords[1] == 1:
            beneath = self.ball_coords + np.array([0,-1])
            if self.grid[tuple(beneath)] == 1:
                if action != 1 and self.platform[-1,0] < self.L-2 and self.platform[0,0] > 1:
                    recoil = True
#                    print("recoil")
                new_dir[1] = 1
                reward += 1
                self.num_hits += 1
            else:
                beneath_left = self.ball_coords + np.array([-1,-1]) #  |___---*____|
                beneath_right = self.ball_coords + np.array([1,-1]) #                 |___*---____|
                if np.sum(np.abs(beneath_left-self.platform[-1])) == 0:
                    if self.ball_coords[0] != self.L-1: new_dir[0] = 1
                    new_dir[1] = 1
                    reward += 1
                    self.num_hits += 1
                if np.sum(np.abs(beneath_right-self.platform[0])) == 0:
                    if self.ball_coords[0] != 0: new_dir[0] = -1
                    new_dir[1] = 1
                    reward += 1
                    self.num_hits += 1

        if recoil:
            if action == 0 and new_dir[0] == 1:
                new_dir[0] = -1
#                print("case <-")
            if action == 2 and new_dir[0] == -1:
                new_dir[0] = 1
#                print("case ->")
            #if self.ball_coords[0]>0 and self.ball_coords[0]<self.L-1:
            self.ball_coords += self.actions[action][0]

        self.curr_ball_dir = new_dir
        prev_ball_coords = np.copy(self.ball_coords)
#        print(prev_ball_coords)
        self.ball_coords += self.curr_ball_dir
        try:
            self.grid[tuple(self.ball_coords)] = 1
        except:
            print(prev_ball_coords, self.ball_coords, self.curr_ball_dir)
            exit(1)

        if self.ball_coords[1] <= 0: 
            done = True
            reward = -2
        return reward, done


    def move(self, action):
        self.update_platform(action)
        reward, done = self.update_ball_and_status(action)
        return reward, done

    def step(self, action):
        done = False
        reward = 0
        self.t += 1
        if self.t == self.max_time:
            done = True
#            return self.return_env_state(), reward, done, self.t, self.num_hits
            return self.return_env_state(), reward, done, self.num_hits
        else:
            reward, done = self.move(action)
#            return self.return_env_state(), reward, done, self.t, self.num_hits
            return self.return_env_state(), reward, done, self.num_hits

    def return_env_state(self):
        env_state = np.zeros(4 + (self.L+self.H) + (self.L-2), dtype='int')
        if tuple(self.curr_ball_dir) == (-1,-1): env_state[0] = 1
        if tuple(self.curr_ball_dir) == (-1, 1): env_state[1] = 1
        if tuple(self.curr_ball_dir) == ( 1,-1): env_state[2] = 1
        if tuple(self.curr_ball_dir) == ( 1, 1): env_state[3] = 1
        env_state[4 + self.ball_coords[0]] = 1
        env_state[4 + self.L + self.ball_coords[1]] = 1
        env_state[4 + self.L + self.H + (self.platform[1,0]-1)] = 1
        return env_state

    def render(self):
        return self.grid


fig = plt.figure(figsize=(7,7))
ax = fig.add_subplot(1,1,1)
plt.xticks([])  
plt.yticks([])  
def animate(i, comm):
    req = comm.irecv(source=0, tag=11)
    grid = req.wait()
    ax.pcolor(grid.T, edgecolors='k', linewidths=0.05)
    ax.set_aspect('equal')
    done_plotting = comm.recv(source=0, tag=22)
    if done_plotting: exit(0) 


def main():

    from mpi4py import MPI
    import matplotlib.animation as animation

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    if rank == 0:
        eps = 10000
        #env = Pong(10,9,100)
        env = Pong(15,30,100)
        done_plotting = False
        for i in range(eps): 
            _ = env.reset()
            done = False
            while not done:
                grid = env.render()
                action = np.random.choice(3)
                _, reward, done, t, _ = env.step(action)
                req = comm.isend(grid, dest=1, tag=11)
                req.wait()
                #if reward == 1: print("Hit!!")
                if i == eps - 1 and done: done_plotting = True
                comm.send(done_plotting, dest=1, tag=22)
            print(f"Done episode #: {i}")
  
    elif rank == 1:
        ani = animation.FuncAnimation(fig, animate, fargs=(comm,), interval=0.2)
        plt.show()
  
if __name__ == "__main__":
    main()


#    env = Pong(10,9,100)
#    _ = env.reset()
#    done = False
#    while not done:
#        env.render()
#        action = np.random.choice(3)
#        _, _, done, t = env.step(action)
#        print(f"done: {done}, t: {t}")
#


#    action = input("actions: a:<, s:_, d:>\n")
#    if action == 'a': action = 0
#    elif action == 's': action = 1
#    else: action = 2
 


#    def render(self):
#        fig = plt.figure()
#        plt.pcolormesh(self.grid.T, edgecolors='k', linewidth=2)
#        ax = plt.gca()
#        ax.set_aspect('equal')
#        plt.show()


