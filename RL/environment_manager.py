#!/usr/bin/env python3

import music
import sys
import numpy as np
import queue
from grid_environments import GridWorld, WaterMaze
from pong_environments import Pong 

import matplotlib.pyplot as plt
from parameters import *

np.random.seed(12)
#np.random.seed(13)

in_q = queue.Queue()
def inhandler(t, indextype, channel_id):
    in_q.put([t, channel_id])


#    for _l in range(L):
#        frozen_poisson_noise_input[_l] = (np.random.rand(seq_len-6) < 0.1)
#        frozen_poisson_noise_input[_l+L] = (np.random.rand(seq_len-6) < 0.1)
def send_poisson_input_GRID(outport, index, state, t_ms, timeStep, n_in, seq_len, input_f0):
    frozen_poisson_noise_input = np.zeros((n_in, seq_len-6))
    frozen_poisson_noise_input[state[0]] = (np.random.rand(seq_len-6) < input_f0)
    frozen_poisson_noise_input[state[1]+L] = (np.random.rand(seq_len-6) < input_f0)
    spktimes = []  
    for spks in frozen_poisson_noise_input:
        spktimes.append( timeStep*np.unique(spks * np.arange(t_ms+1, t_ms+1 + seq_len-6))[1:] )
    for target, spikes in enumerate(spktimes):
        for spike_t in spikes:
            outport.insertEvent(spike_t , target, index)

def send_poisson_input_PONG(outport, index, state, t_ms, timeStep, n_in, seq_len, input_f0):
    frozen_poisson_noise_input = np.zeros((n_in, seq_len-6))
    for i, entry in enumerate(state):
        if entry == 1: 
            frozen_poisson_noise_input[i] = (np.random.rand(seq_len-6) < input_f0)
    spktimes = []  
    for spks in frozen_poisson_noise_input:
        spktimes.append( timeStep*np.unique(spks * np.arange(t_ms+1, t_ms+1 + seq_len-6))[1:] )
    for target, spikes in enumerate(spktimes):
        for spike_t in spikes:
            outport.insertEvent(spike_t , target, index)



gaussian_envelope = lambda x, mu, sigma: np.exp(-0.5*((x-mu)/sigma)**2)
def send_poisson_input_CARTPOLE(outport, index, state, t_ms, timeStep, n_in, seq_len, input_f0):
    dim = 4
    sigma = 0.5

    norm_factors = 1./np.array([2.4, 3.45,0.2094, 3.6])
    norm_state = 0.5 + 0.5 * state * norm_factors

    n_in_d = int(n_in/dim)
    offsets = np.array([i*n_in_d for i in range(dim) ])
    state_to_ids_ = np.floor(norm_state*n_in_d)
    state_to_ids = np.floor(norm_state*n_in_d) + offsets

    frozen_poisson_noise_input = np.zeros((n_in, seq_len-6))
    for mu in state_to_ids:
        for i in range(n_in_d*dim):
            factor = gaussian_envelope(i, mu, sigma)
            frozen_poisson_noise_input[i] += 1.*(np.random.uniform(0,1,seq_len-6) < input_f0*factor)

    spktimes = []  
    for spks in frozen_poisson_noise_input:
        spktimes.append( timeStep*np.unique(spks * np.arange(t_ms+1, t_ms+1 + seq_len-6))[1:] )
    for target, spikes in enumerate(spktimes):
        for spike_t in spikes:
            outport.insertEvent(spike_t , target, index)


symm = 1
training = 1
render = False
plot = False
plot_freq = 1

if task == 'miniDebug':
    nvp = 1
    n_episodes = 2
    n_batch = 3
    n_trial_moves = 4
    seq_len = 10
    L = 3
    n_in = 2*L
    n_out = 4
    V_th = 0.2
    env = GridWorld(L, n_trial_moves)
    send_poisson_input = send_poisson_input_GRID

elif task == 'Debug':
    nvp = 1
    n_episodes = 10
    n_batch = 5
    n_trial_moves = 25
    seq_len = 20
    L = 3
    n_in = 2*L
    n_out = 4
    V_th = 0.2
    env = GridWorld(L, n_trial_moves)
    send_poisson_input = send_poisson_input_GRID


elif task == 'GridWorld':
    nvp = 1
    n_episodes = 100#250
    n_batch = 64
    n_trial_moves = 180
    seq_len = 20
    L = 9
    n_in = 2*L
    n_out = 4
    V_th = 0.2
    env = GridWorld(L, n_trial_moves)
    send_poisson_input = send_poisson_input_GRID

elif  task == 'WaterMaze':
    nvp = 1
    n_episodes = 150#250
    n_batch = 64
    n_trial_moves = 180
    seq_len = 20
    L = 9
    n_in = 2*L
    n_out = 4
    V_th = 0.2
    env = WaterMaze(L, n_trial_moves)
    send_poisson_input = send_poisson_input_GRID

elif  task == 'Pong':
    nvp = 1
    n_episodes = 10#250
    n_batch = 64
    n_trial_moves = 180
    seq_len = 20
    L = 10
    H = 9
    n_in = 4 + (L+H) + (L-2)
    n_out = 3
    V_th = 0.03
    env = Pong(L, H,  n_trial_moves)
    send_poisson_input = send_poisson_input_PONG

elif  task == 'CartPole':
    import gym
    nvp = 1
    n_episodes = 35#100
    n_batch = 64
    n_trial_moves = 200
    seq_len = 30

    dim = 4
    n_in_d = 100
    n_in = n_in_d * dim
    
    n_out = 2

    env = gym.make('CartPole-v0')
    env.seed(0)

    send_poisson_input = send_poisson_input_CARTPOLE

if not training:
    kk = 0#12
    n_episodes = 1
    n_batch = 1
    render = True

    for _ in range(kk): state = env.reset() 


prefix = f'{task}_symm_{symm}_epis_{n_episodes}_b_{n_batch}_moves_{n_trial_moves}'

f0 = 900.
input_f0 = f0 / 1000.
_actions = {0: "v", 1: "<", 2: "^", 3: ">"}

miliseconds = 0.001
timeStep= 1. * miliseconds
simtime=0.1


setup = music.Setup() 
index = music.Index.GLOBAL
 
inp = setup.publishEventInput("env_in")
outp = setup.publishEventOutput("env_out")
rwrd_outp = setup.publishContOutput("env_rwrd_out")
done_outp = setup.publishContOutput("env_done_out")

inp.map(inhandler, index,  base=0,  size=n_out, accLatency=0.002)
outp.map(index, base=0, size=n_in)

reward_buffer = np.array([0.], dtype=np.double)
done_buffer = np.array([0.], dtype=np.double)

rwrd_outp.map(reward_buffer, base=0)
done_outp.map(done_buffer, base=0)

width = outp.width()    
     
def main():

    runtime = setup.runtime(timeStep) 
    tickt = runtime.time()   
    tickt_ms = int(tickt / miliseconds)
    print("init tickt: ", tickt_ms)
    
    trial_not_done = 0.0
    trial_done = 1.0
    reward = 0.0

    mean_latencies = []
    std_latencies = []
    for i_episode in range(n_episodes):
        latencies = []
        for batch_elem in range(n_batch):
            latency = 0 

            done = False
    #        state = env.reset(special_reset=True) # <<<<================ OJOOOOOOO 
            state = env.reset() 
#            if task=='CartPole': state = env.reset() 
#            else: state = env.reset(special_reset = True, prepare_rendering=True) 
    #        print("Init State: ", state)
            for move in range(n_trial_moves):
#                print(f"------------> episode: {i_episode}, batch_elem: {batch_elem}, move: {move}")
                if not done:
    
                    advance_time_ms = seq_len-2 if move == 0 else seq_len
                    t_move_end_ms = tickt_ms + advance_time_ms
    #                send_poisson_input(outp, tickt_ms, state)
                    send_poisson_input(outp, index, state, tickt_ms, timeStep, n_in, seq_len, input_f0)
    
                    done_buffer[0] = trial_not_done
    
                    while tickt_ms < t_move_end_ms:
    
                        db_tickt_i = tickt
                        db_tickt_ms_i = tickt_ms
    
                        runtime.tick()
                        tickt = runtime.time()
                        tickt_ms = int(tickt / miliseconds)
    
                        db_tickt_f = tickt
                        db_tickt_ms_f = tickt_ms
    
                        if db_tickt_ms_f == db_tickt_ms_i: tickt_ms += 1
    
                        if tickt_ms > t_move_end_ms:
                            msg = f"[BUG] {db_tickt_i} {db_tickt_f} {db_tickt_ms_i} {db_tickt_ms_f} "\
                            + f"{t_move_end_ms}"
                            print(msg)
                            exit(0)
                    
                    actor_id = 0
                    while not in_q.empty():
                        event = in_q.get()
                        event_time = event[0]
                        actor_id = event[1]
     #                   if in_q.qsize() == 0: print(f"(D)[ENV][spk] t: {event_time*1000:0.0f}, Actor: {actor_id}")
    
                    action = actor_id
                    if task=='CartPole': 
                        state, reward, done, _  = env.step(action)
                        latency += reward
                    else:
                        state, reward, done, latency = env.step(action)
                    if render:
                        print(f"Rendering: {latency}", end='\r')
                        env.render()
    
    #                reward = reward - 0.01*tickt
                    msg = f"(E)[ENV][act] t: {tickt_ms}, t_end: {t_move_end_ms}, Actor: {_actions[action]} ({action}), "
                    msg += f"Done: {done}, state: {state}, lat: {latency}, reward: {reward}" 
#                    print(msg)
    
                    reward_buffer[0] = reward
    
                    if move == n_trial_moves-1 or done:
    #                    if done: 
    #                        done_buffer[0] = trial_done
    
                        while tickt_ms < t_move_end_ms + 2:
        
                            db_tickt_i = tickt
                            db_tickt_ms_i = tickt_ms
        
                            runtime.tick()
                            tickt = runtime.time()
                            tickt_ms = int(tickt / miliseconds)
        
                            db_tickt_f = tickt
                            db_tickt_ms_f = tickt_ms
        
                            if db_tickt_ms_f == db_tickt_ms_i: tickt_ms += 1
    
                else:
                    reward_buffer[0] = 0.0
                    done_buffer[0] = trial_done
    
                    t_trial_end_ms = tickt_ms + (n_trial_moves - move) * seq_len
                    while tickt_ms < t_trial_end_ms:
    
                        db_tickt_i = tickt
                        db_tickt_ms_i = tickt_ms
    
                        runtime.tick()
                        tickt = runtime.time()
                        tickt_ms = int(tickt / miliseconds)
    
                        db_tickt_f = tickt
                        db_tickt_ms_f = tickt_ms
    
                        if db_tickt_ms_f == db_tickt_ms_i: tickt_ms +=1
                    break
            if not render:
                print(f">> batch elem: {batch_elem}, latency: {latency} ")
    
            latencies.append(latency)
        mean_latency = np.mean(latencies)
        std_latency = np.std(latencies)
        mean_latencies.append(mean_latency)
        std_latencies.append(std_latency)
        if not render:
            print(f'i_episode: {i_episode}, mean latency: {mean_latency:0.3f}', file=sys.stderr)
                    

        if plot and i_episode % plot_freq == 0 and i_episode>0 and not render:
            np_mean = np.array(mean_latencies)
            np_std = np.array(std_latencies)
            epis = np.arange(len(np_mean))
            fig = plt.figure()
            plt.plot(epis, np_mean)
            plt.fill_between(epis, np_mean - np_std, np_mean + np_std, alpha=0.3)
            plt.title(prefix, fontsize=15)
            plt.xlabel('Episodes', fontsize=15)
            plt.ylabel('Average Latency', rotation=90, fontsize=15)
            plt.savefig(f'./imgs/{prefix}_tdrl_bs_{n_batch}_it_{i_episode:04d}.png')
            plt.close('all')
                       
    runtime.finalize()


if __name__ == "__main__":
#    if training:
    main()
#    else: 
#        pass
    #    test_trained_agent()





