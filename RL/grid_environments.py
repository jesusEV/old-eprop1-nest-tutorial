#!/usr/bin/env python

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import time

color_obstacle = (75,0,130)
color_exit = (255,255,255)
color_exit = (220,20,60)
color_maze = (70,130,180)
color_agent = (255,255,0)

import pyglet
from pyglet.window import key
from pyglet import shapes
 

def close_event():
    plt.close()


class GridWorld:
    def __init__(self, L, MaxT):
        self.L = L
        self.max_time = MaxT
        self.reward_pos = np.array([L-2, L-1])
#        self.reward_pos = np.array([L-1, L-1]) #<<<======= OJO
        self.state = np.array([0,0])
        self.proposed_state = np.array([0,0])
        self.actions = np.array([[0, -1], [-1, 0], [0, 1], [1, 0]]) #(v), (<), (^), (>)
        self.inner_box = []
    def reset(self, special_reset=False, prepare_rendering=False):
        if not special_reset:
            while True:
                proposed_state = np.random.choice(self.L, 2)
                if self.verify_new_state(proposed_state): break
        else:
            proposed_state = np.zeros(2, dtype='int')  #<<<<====== OJO
#            while True: 
#                proposed_state[0] = np.random.choice(self.L)
#                proposed_state[1] = np.random.choice(3)
#                if self.verify_new_state(proposed_state): break

        self.state = proposed_state
        self.grid = np.zeros((self.L, self.L))
        self.grid[tuple(self.reward_pos)] = -2
        self.grid[tuple(self.state)] = 1
        self.reward = 0
        self.t = 0
        if prepare_rendering: self._prepare_rendering()
        return self.state

    def verify_new_state(self, proposed_state):
        if proposed_state[0] < 0: return False
        if proposed_state[1] < 0: return False
        if proposed_state[0] >= self.L: return False
        if proposed_state[1] >= self.L: return False
        return True

    def move(self, action):
        proposed_state = self.state + self.actions[action]
#        print(f"state: {self.state}, action: {self.actions[action]}, prop state: {proposed_state}")
        if self.verify_new_state(proposed_state):
            self.grid[tuple(self.state)] = 0
            self.state = np.copy(proposed_state)
            self.grid[tuple(self.state)] = 1

    def step(self, action):
        done = False
        self.t += 1
        if self.t == self.max_time:
            done = True
            return self.state, self.reward, done, self.t
        else:
            self.move(action)
            if np.sum(np.abs(self.state - self.reward_pos)) == 0:
                done = True
                self.reward = 1
            else: 
                self.reward = -1
                #self.reward -= 1
            return self.state, self.reward, done, self.t

    def _prepare_rendering(self):
   
#        env = WaterMaze(L,200)
#        init_grid = env.reset()
        self.action = [-1]
    
        self.game_window = pyglet.window.Window(
            width=550,
            height=550,
            caption="Catch your husband!"
        )
        
        pyglet.gl.glClearColor(0.1, 0.1, 0.1, 0.1)
     
        self.env_batch = pyglet.graphics.Batch()
        self.agent_batch = pyglet.graphics.Batch()
        self.env_shapes = []
        self.agent_shapes = []
        self.l = 50
        self.offset = 25
        self.rescale = 0.98
    
   
        for i in range(self.L):
            for j in range(self.L):
                if self.grid[i,j] == 0:
                    self.env_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                                         color=color_maze, batch=self.env_batch))
                elif self.grid[i,j] == -1:
                    self.env_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                                         color=color_obstacle, batch=self.env_batch))
                elif self.grid[i,j] == -2:
                    self.env_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                                         color=color_exit, batch=self.env_batch))
                elif self.grid[i,j] == 1:
                    self.env_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                                         color=color_maze, batch=self.env_batch))
                    self.agent_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                             color=color_agent, batch=self.agent_batch))
    
        self.labels = []
        self.labels_batch = pyglet.graphics.Batch()
        self.labels.append(pyglet.text.Label(text=f'Latency: {0}', font_name='Times New Roman', font_size=24,
                                  x=300, y=500, batch=self.labels_batch))
    
#    @self.game_window.event
#        def on_draw():
    def on_draw(self):
        self.game_window.clear()
        self.game_window.switch_to()
        self.game_window.dispatch_events()
        self.env_batch.draw()
        self.agent_batch.draw()
        self.labels_batch.draw()
        self.game_window.flip()
    
    def update(self):
        for agent in self.agent_shapes:
            agent.x = self.l*self.state[0] + 2*self.offset
            agent.y = self.l*self.state[1] + self.offset
            self.labels[0].text = f'Latency: {self.t}'
 
    def render(self):
        time.sleep(0.1)
        self.update()
        self.on_draw()


class WaterMaze:
    def __init__(self, L, MaxT):
        self.L = L
        self.max_time = MaxT
        self.reward_pos = np.array([(L-1)//2, (L-1)//2])
        self.state = np.array([0,0])
        self.proposed_state = np.array([0,0])
        self.actions = np.array([[0, -1], [-1, 0], [0, 1], [1, 0]]) #(v), (<), (^), (>)
        self.inner_box = np.array([(L-5)//2, L-6]) + np.array([[0,3], [0,2], [0,1], [0,0],\
                                                                                    [1,0],\
                                                                                    [2,0],\
                                                                                    [3,0],\
                                                               [4,3], [4,2], [4,1], [4,0]])

    def reset(self, special_reset=False, prepare_rendering=False):
        if not special_reset:
            while True:
                proposed_state = np.random.choice(self.L, 2)
                if self.verify_new_state(proposed_state): break
        else:
            proposed_state = np.zeros(2, dtype='int')
            while True:
                proposed_state[0] = np.random.choice(self.L)
                proposed_state[1] = np.random.choice(3)
                if self.verify_new_state(proposed_state): break
        self.state = proposed_state
        self.grid = np.zeros((self.L, self.L))
        self.grid[tuple(self.reward_pos)] = -2
        for coords in self.inner_box: self.grid[tuple(coords)] = -1
        self.grid[tuple(self.state)] = 1
        self.reward = 0
        self.t = 0
        if prepare_rendering: self._prepare_rendering()
        return self.state

    def verify_new_state(self, proposed_state):
        if proposed_state[0] < 0: return False
        if proposed_state[1] < 0: return False
        if proposed_state[0] >= self.L: return False
        if proposed_state[1] >= self.L: return False
        for coords in self.inner_box: 
            if np.sum(np.abs(proposed_state - coords)) == 0:
 #               print(f"barrier: {coords}")
                return False
        return True

    def move(self, action):
        proposed_state = self.state + self.actions[action]
  #      print(f"state: {self.state}, action: {self.actions[action]}, prop state: {proposed_state}")
        if self.verify_new_state(proposed_state):
            self.grid[tuple(self.state)] = 0
            self.state = np.copy(proposed_state)
            self.grid[tuple(self.state)] = 1

    def step(self, action):
        done = False
        self.t += 1
        if self.t == self.max_time:
            done = True
            return self.state, self.reward, done, self.t
        else:
            self.move(action)
            if np.sum(np.abs(self.state - self.reward_pos)) == 0:
                done = True
                self.reward = 1
            else: 
                self.reward = -1
                #self.reward -= 1
            return self.state, self.reward, done, self.t

    def _prepare_rendering(self):
   
#        env = WaterMaze(L,200)
#        init_grid = env.reset()
        self.action = [-1]
    
        self.game_window = pyglet.window.Window(
            width=550,
            height=550,
            caption="Catch your husband!"
        )
        
        pyglet.gl.glClearColor(0.1, 0.1, 0.1, 0.1)
     
        self.env_batch = pyglet.graphics.Batch()
        self.agent_batch = pyglet.graphics.Batch()
        self.env_shapes = []
        self.agent_shapes = []
        self.l = 50
        self.offset = 25
        self.rescale = 0.98
    
   
        for i in range(self.L):
            for j in range(self.L):
                if self.grid[i,j] == 0:
                    self.env_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                                         color=color_maze, batch=self.env_batch))
                elif self.grid[i,j] == -1:
                    self.env_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                                         color=color_obstacle, batch=self.env_batch))
                elif self.grid[i,j] == -2:
                    self.env_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                                         color=color_exit, batch=self.env_batch))
                elif self.grid[i,j] == 1:
                    self.env_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                                         color=color_maze, batch=self.env_batch))
                    self.agent_shapes.append(
                            shapes.Rectangle(
                                self.l*i+2*self.offset, self.l*j+self.offset, self.rescale*self.l, self.rescale*self.l,
                                             color=color_agent, batch=self.agent_batch))
    
        self.labels = []
        self.labels_batch = pyglet.graphics.Batch()
        self.labels.append(pyglet.text.Label(text=f'Latency: {0}', font_name='Times New Roman', font_size=24,
                                  x=300, y=500, batch=self.labels_batch))
    
#    @self.game_window.event
#        def on_draw():
    def on_draw(self):
        self.game_window.clear()
        self.game_window.switch_to()
        self.game_window.dispatch_events()
        self.env_batch.draw()
        self.agent_batch.draw()
        self.labels_batch.draw()
        self.game_window.flip()
    
    def update(self):
        for agent in self.agent_shapes:
            agent.x = self.l*self.state[0] + 2*self.offset
            agent.y = self.l*self.state[1] + self.offset
            self.labels[0].text = f'Latency: {self.t}'
 
    def render(self):
#        print(self.state)
        time.sleep(0.1)
        self.update()
        self.on_draw()




def main():

    from mpi4py import MPI
    import matplotlib.animation as animation

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    if rank == 0:
        eps = 10000
        env = WaterMaze(9,200)
        done_plotting = False
        for i in range(eps): 
            _ = env.reset()
            done = False
            while not done:
                grid = env.render()
                action = np.random.choice(3)
                _, reward, done, t = env.step(action)
                req = comm.isend(grid, dest=1, tag=11)
                req.wait()
                #if reward == 1: print("Hit!!")
                if i == eps - 1 and done: done_plotting = True
                comm.send(done_plotting, dest=1, tag=22)
            print(f"Done episode #: {i}")
  
    elif rank == 1:
        ani = animation.FuncAnimation(fig, animate, fargs=(comm,), interval=0.2)
        plt.show()
  
if __name__ == "__main__":
    main()


