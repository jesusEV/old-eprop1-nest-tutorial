import numpy as np
import nest
from matplotlib import pyplot as plt
from models import sum_of_sines_target
import json
import os

'''
Plot Results
'''
plot = False

'''
paths were results are stored
'''
savepath = 'results/'
if not os.path.exists(savepath):
    os.mkdir(savepath)

'''
simulation parameters
'''
# temporal resolution of the simulation
resolution = 1.0
# dendritic delay of all synapses
delay = resolution
# firing rate of frozen input noise
input_f0 = 50. / 1000.
# number of input neurons
n_in = 100
# number of recurrent neurons
n_rec = 100
# number of output neurons
n_out = 1
# number of iterations
n_iter = 150
# duration of one training period
seq_len = 1000
# total simulation time
sim_time = n_iter * seq_len + 5.0
# interval for updating the synapse weights
update_interval = float(seq_len)
# learning rate
learning_rate = 0.5e-3
# prefactor of firing rate regularization
reg = 300.0
# number of virtual processes for the simulation
nvp = 4
# fix numpy random seed
np.random.seed(302687)
# record spikes of recurrent neurons only for small networks and short sim times
record_spikes = True

'''
reset NEST kernel
'''
nest.ResetKernel()
nest.SetKernelStatus({'resolution': resolution, 'total_num_virtual_procs': nvp,
                      'print_time': True})

'''
definition of model names
'''
in_model = 'parrot_neuron'
rec_model = 'iaf_psc_delta_eprop'
out_model = 'error_neuron'
# rate generators provide the target signal
rg_model = 'step_rate_generator'
# spike generators provide the frozen input noise
sg_model = 'spike_generator'
eprop_model = 'eprop_synapse'
# broadcast model -> random feedback weights
broadcast_model = 'learning_signal_connection_delayed'
# neuron parameters
rec_neuron_params = {
    'V_m': 0.0,
    'V_th': 0.6,
    'V_reset': 0.0,
    'E_L': 0.0,
    'tau_m': 30.0,
    't_ref': 0.0,
    'dampening_factor': 0.3,
    'update_interval': update_interval,
}
out_neuron_params = {
    'V_m': 0.0,
    'E_L': 0.0,
    'tau_m': 30.0,
    'dampening_factor': 1.0,
    'update_interval': update_interval,
}

'''
draw random weights
'''
in_weights = np.random.randn(n_in, n_rec) / np.sqrt(n_in)
rec_weights = np.random.randn(n_rec, n_rec) / np.sqrt(n_rec)
# set diagonal to zero (no autapses)
np.fill_diagonal(rec_weights, 0.0)
out_weights = np.random.randn(n_rec, n_out) / np.sqrt(n_rec)
rand_feedback_weights = np.random.randn(n_rec, n_out) / np.sqrt(n_rec)

'''
create neurons and devices
'''
in_nrns = nest.Create(in_model, n_in)
rec_nrns = nest.Create(rec_model, n_rec, params=rec_neuron_params)
out_nrns = nest.Create(out_model, n_out, params=out_neuron_params)
frozen_poisson_noise_input = (np.random.rand(seq_len, n_in) < resolution * input_f0).T
sg_params = []
for spks in frozen_poisson_noise_input:
    spktimes = np.array(np.unique(spks * np.arange(seq_len))[1:], dtype=np.float32)
    sg_params.append({'spike_times': np.hstack([spktimes + j * seq_len
                                                for j in range(n_iter)])})

spike_generators = nest.Create(sg_model, n_in, params=sg_params)
# create target signal
target_sinusoidal_outputs = np.tile([sum_of_sines_target(seq_len) for i in range(n_out)], n_iter)
rg_times = np.arange(resolution, n_iter * seq_len + 1, resolution)

rg_params = []
for ts in target_sinusoidal_outputs:
    rg_params.append({'amplitude_times': rg_times, 'amplitude_values': ts})

rate_generators = nest.Create(rg_model, n_out, params=rg_params)
# multimeter that records the membrane potential and the target signal of the outpu neuron
mm_out = nest.Create('multimeter', params={'interval': resolution, 'record_from': ['V_m', ]})
if record_spikes:
    sd = nest.Create('spike_recorder', 1)

# create connections


def create_Wmin_Wmax(weight_matrix):
    Wmin = np.zeros_like(weight_matrix)
    Wmax = np.zeros_like(weight_matrix)

    signs = np.sign(weight_matrix)

    Wmin[np.where(signs == -1.)] = -100.
    Wmax[np.where(signs == 1.)] = 100.
    return Wmin, Wmax


Wmin_in, Wmax_in = create_Wmin_Wmax(in_weights.T)
Wmin_rec, Wmax_rec = create_Wmin_Wmax(rec_weights.T)
Wmin_out, Wmax_out = create_Wmin_Wmax(out_weights.T)


synapse_in = {'synapse_model': eprop_model, 'learning_rate': learning_rate,
              'weight': in_weights.T, 'delay': delay, 'update_interval': update_interval,
              'tau_decay': 30.0, 'keep_traces': 0.0,
              'target_firing_rate': 10.0, 'rate_reg': reg, 'Wmin': Wmin_in, 'Wmax': Wmax_in}
synapse_rec = {'synapse_model': eprop_model, 'learning_rate': learning_rate,
               'weight': rec_weights.T, 'delay': delay, 'update_interval': update_interval,
               'tau_decay': 30.0, 'keep_traces': 0.0,
               'target_firing_rate': 10.0, 'rate_reg': reg, 'Wmin': Wmin_rec, 'Wmax': Wmax_rec}
synapse_out = {'synapse_model': eprop_model,
               'weight': out_weights.T, 'delay': delay, 'tau_decay': 30.0, 'update_interval': update_interval,
               'keep_traces': 0., 'learning_rate': learning_rate, 'Wmin': Wmin_out, 'Wmax': Wmax_out}
synapse_broadcast = {'synapse_model': broadcast_model,
                     'weight': rand_feedback_weights, 'delay': delay}
synapse_target = {'synapse_model': 'rate_connection_delayed',
                  'weight': 1., 'delay': delay, 'receptor_type': 2}
static_synapse = {'synapse_model': 'static_synapse',
                  'weight': 1.0, 'delay': delay}
conn_spec = {'rule': 'all_to_all', 'allow_autapses': False}

# NEST does not support a direct connection of devises to neurons via plastic synapses. This is why we have the parrot
# neurons between the spike generators and the recurrent network. Parrot neurons simply send a spike whenever they are
# receiving one.
nest.Connect(spike_generators, in_nrns, syn_spec=static_synapse, conn_spec={'rule': 'one_to_one'})
nest.Connect(in_nrns, rec_nrns, syn_spec=synapse_in, conn_spec=conn_spec)
nest.Connect(rec_nrns, rec_nrns, syn_spec=synapse_rec, conn_spec=conn_spec)
nest.Connect(rec_nrns, out_nrns, syn_spec=synapse_out, conn_spec=conn_spec)
nest.Connect(out_nrns, rec_nrns, syn_spec=synapse_broadcast, conn_spec=conn_spec)
nest.Connect(rate_generators, out_nrns, syn_spec=synapse_target, conn_spec={'rule': 'one_to_one'})

nest.Connect(mm_out, out_nrns, syn_spec=static_synapse)
if record_spikes:
    nest.Connect(rec_nrns, sd, syn_spec=static_synapse)

'''
run simulation
'''
nest.Simulate(sim_time)

'''
process data of recording devices
'''
times_all = np.arange(n_iter * seq_len)
if record_spikes:
    # compute average firing rate
    spikes_events = nest.GetStatus(sd, 'events')[0]
    senders_rec = spikes_events['senders']
    rec_spike_times = np.array(spikes_events['times']) - 2.0 * delay
    afrs_nest = np.array([len(np.where(
        (rec_spike_times > seq_len * i) & (rec_spike_times <= seq_len * (i + 1)))[0])
        / float(n_rec) for i in np.arange(n_iter)])
    rec_spike_times_per_neuron = \
        [rec_spike_times[np.where(senders_rec == i)] for i in rec_nrns.tolist()]

# membrane potential of readout neuron
events_out = nest.GetStatus(mm_out)[0]['events']
senders_out = events_out['senders']
Vms_out = [events_out['V_m'][
    np.where(senders_out == i)][2:2 + n_iter * seq_len] for i in out_nrns.tolist()]

# compute loss
loss_times = seq_len * (np.arange(n_iter) + 1)
output_error = np.power(Vms_out[0] - target_sinusoidal_outputs[0], 2)
loss_nest = 0.5 * np.add.reduceat(output_error, np.arange(0, len(output_error), seq_len))
print('loss: {}'.format(loss_nest))

results_dict = {
    'times': times_all.tolist(),
    'network_signal': [arr.tolist() for arr in Vms_out],
    'target_signal': target_sinusoidal_outputs.tolist(),
    'loss_times': loss_times.tolist(),
    'loss_nest': loss_nest.tolist(),
    'avg_firing_rate': afrs_nest.tolist(),
    'rec_spike_times_per_neuron': [arr.tolist() for arr in rec_spike_times_per_neuron],
     }

with open(savepath + 'results_pattern_gen.json', 'w') as f:
        json.dump(results_dict, f)
'''
plot results
axA: membrane potential of readout neuron and target signal
axB: loss, (average firing rate
(axC: spikes of recurrent neurons)
'''
if plot:
    lw = 3.0
    if record_spikes:
        fig1, (axA, axB, axC) = plt.subplots(3, 1, sharex=True, figsize=(15, 8))
    else:
        fig1, (axA, axB) = plt.subplots(2, 1, sharex=True, figsize=(15, 8))
    
    for out_nrn in np.arange(n_out):
        axA.plot(times_all, Vms_out[out_nrn], ls='--', lw=lw, color='b',
                 label=r'$V^{}_{}$ NEST'.format('{out}', out_nrn))
        axA.plot(times_all, target_sinusoidal_outputs[out_nrn], color='r', lw=lw, ls='--',
                 label='target NEST')
    
    axA.legend(loc='upper left', frameon=True, ncol=1)
    
    axB.plot(loss_times, loss_nest, color='g', lw=lw, ls='--', label='loss NEST')
    axB.legend(loc='upper left', frameon=True, ncol=1)
    
    if record_spikes:
        axB2 = axB.twinx()
        axB2.plot(loss_times, afrs_nest, color='y', lw=lw, ls='--', label='avg firing rate NEST')
        axB2.legend(loc='upper right', frameon=True, ncol=1)
        for i, spk_ts in enumerate(rec_spike_times_per_neuron):
            axC.scatter(spk_ts, i * np.ones_like(spk_ts), color='k', marker='o', s=10)
    
    fig1.tight_layout()
    plt.show()
