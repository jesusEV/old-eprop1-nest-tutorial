import numpy as np
from matplotlib import pyplot as plt
import plotfuncs_PRL as pf
import json
import csv
import argparse

sim_dict = {
        'losses': {
            'path': './data/losses.npy',
            'labels': ['NEST', 'NEST (Dale)', 'tensorflow'],
            },
        'weights': {
            'path': './data/weight_traces.json',
            },
        'weights_dale': {
            'path': './data/weight_traces_dale.json',
            },
        'traces_rec': {
            'path': './data/time_traces_rec_nest.npy',
            'from_iter': 0,
            'to_iter': 1,
            'indices': [3, 4, 5],
            'seq_len': 1000,
            'labels': [r'$4$', r'$5$', r'$6$', ],
            },
        'traces_out': {
            'path': './data/time_traces_out_nest.npy',
            'from_iter': 0,
            'to_iter': 1,
            'seq_len': 1000,
            },
        'syn_traces_rec': {
            'path': './data/syn_traces.json',
            'idx_t': 0,
            'idx_pseudo': 1,
            'idx_z_hat': 2,
            'idx_elig': 3,
            'idx_grad': 4,
            'data_labels': ['104_to_105', '105_to_106', '106_to_104'],
            'labels': [r'$4 \rightarrow 5$', r'$5 \rightarrow 6$', r'$6 \rightarrow 4$', ],
            },
        'spikes_rec': {
            'path': './data/spike_times.json',
            'indices': [3, 4, 5],
            },
        }

parser = argparse.ArgumentParser(description='plotting program')
parser.add_argument('--savefig', dest='path', default=None, const='../', nargs='?')
args = parser.parse_args()

# load loss
losses = np.load(sim_dict['losses']['path'])
loss_nest_mean = losses[0]
loss_nest_std = losses[1]
loss_nest_dale_mean = losses[2]
loss_nest_dale_std = losses[3]
loss_tf_mean = losses[4]
loss_tf_std = losses[5]

# load time traces of weights
with open(sim_dict['weights']['path'], 'r') as d:
    data = json.load(d)
    times_in = np.array(data['times_in'])
    weights_in = np.array(data['weights_in'])
    times_rec = np.array(data['times_rec'])
    weights_rec = np.array(data['weights_rec'])
    times_out = np.array(data['times_out'])
    weights_out = np.array(data['weights_out'])

# load time traces of weights with dale's law
with open(sim_dict['weights_dale']['path'], 'r') as d:
    data = json.load(d)
    times_dale_in = np.array(data['times_in'])
    weights_dale_in = np.array(data['weights_in'])
    times_dale_rec = np.array(data['times_rec'])
    weights_dale_rec = np.array(data['weights_rec'])
    times_dale_out = np.array(data['times_out'])
    weights_dale_out = np.array(data['weights_out'])

# load recurrent time traces
traces_rec = np.load(sim_dict['traces_rec']['path'])
times_tr_rec = traces_rec[0]
Vms_rec = traces_rec[1]
pseudo_deriv_rec = traces_rec[2]
learning_signal_rec = traces_rec[3]

# load output time traces
traces_out = np.load(sim_dict['traces_out']['path'])
times__tr_out = traces_out[0]
Vms_out = traces_out[1]
learning_signal_out = traces_out[2]
target_rate = traces_out[3]

# load synaptic time traces
simd = sim_dict['syn_traces_rec']
with open(simd['path'], 'r') as d:
    data = json.load(d)
    for k, v in data.items():
        simd[k] = np.array(v)[0]

# load synaptic time traces
simd = sim_dict['spikes_rec']
with open(simd['path'], 'r') as d:
    data = json.load(d)
    for k, v in data.items():
        simd[k] = np.array(v)

width = 8.0
lw = 1.5
lw_weights = 1.0
fs = 12
ms = 25
scale = 0.6
n_h_panels = 3
n_v_panels = 3
p_h_factor = 1.3
p_w_factor = 1.0
p_h_off = -0.035
p_v_off = 0.20
alpha = 0.35
title_position = 'center'
colors_dict = {
        'loss_nest': 'tomato',
        'loss_nest_dale': 'gold',
        'loss_tf': 'cornflowerblue',
        'weights': 'grey',
        'weights_hl': 'mediumseagreen',
        'Vm_rec': 'tomato',
        'pseudo_deriv': 'cornflowerblue',
        'ls_rec': 'grey',
        'Vm_out': 'tomato',
        'ls_out': 'grey',
        'target': 'tan',
        'elig': 'mediumseagreen',
        'grad': 'firebrick',
        }

'''
create figure for weights and loss
'''
panel_factory = pf.create_fig_PRL(
    1, scale, width, n_h_panels, n_v_panels, voffset=0.0, scale_height=1.3)
fig = panel_factory.return_figure()

ax_weights_in = panel_factory.new_panel(
    0, 0, 'A', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off, panel_width_factor=p_w_factor)

ax_weights_rec = panel_factory.new_panel(
    1, 0, 'B', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off, panel_width_factor=p_w_factor)

ax_weights_out = panel_factory.new_panel(
    2, 0, 'C', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off, panel_width_factor=p_w_factor)

ax_weights_dale_in = panel_factory.new_panel(
    0, 1, 'D', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off-0.03, panel_width_factor=p_w_factor)

ax_weights_dale_rec = panel_factory.new_panel(
    1, 1, 'E', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off-0.03, panel_width_factor=p_w_factor)

ax_weights_dale_out = panel_factory.new_panel(
    2, 1, 'F', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off-0.03, panel_width_factor=p_w_factor)

ax_loss = panel_factory.new_panel(
    0, 2, 'G', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off + 0.01, voffset=0.1, panel_width_factor=3.5*p_w_factor)

# synaptic weights
hl_indices = [0, 1, 3]
xlim = [0.0, 2.0e6]
xticks = [0.0, 1.0e6, 2.0e6]
xtick_labels = ['0', '1000', '2000']
# input weights
for ts, tsd, ws, wsd in zip(times_in, times_dale_in, weights_in, weights_dale_in):
    ax_weights_in.step(ts, ws, lw=lw_weights, color=colors_dict['weights'], alpha=0.55)
    ax_weights_dale_in.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights'], alpha=0.55)

for ts, tsd, ws, wsd in zip(times_in[hl_indices], times_dale_in[hl_indices], weights_in[hl_indices], weights_dale_in[hl_indices]):
    ax_weights_in.step(ts, ws, lw=lw_weights, color=colors_dict['weights_hl'])
    ax_weights_dale_in.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_hl'])

ax_weights_in.set_xlim(xlim)
ax_weights_in.set_xticks(xticks)
ax_weights_in.set_xticklabels([])
ax_weights_in.set_title('input weights', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

ax_weights_dale_in.set_xlim(xlim)
ax_weights_dale_in.set_xticks(xticks)
ax_weights_dale_in.set_xticklabels(xtick_labels)
ax_weights_dale_in.set_xlabel('training iteration', fontsize=fs)
ax_weights_dale_in.set_title('input weights (dale)', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

# recurrent weights
for ts, tsd, ws, wsd in zip(times_rec, times_dale_rec, weights_rec, weights_dale_rec):
    ax_weights_rec.step(ts, ws, lw=lw_weights, color=colors_dict['weights'])
    ax_weights_dale_rec.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights'])

for ts, tsd, ws, wsd in zip(times_rec[hl_indices], times_dale_rec[hl_indices], weights_rec[hl_indices], weights_dale_rec[hl_indices]):
    ax_weights_rec.step(ts, ws, lw=lw_weights, color=colors_dict['weights_hl'])
    ax_weights_dale_rec.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_hl'])

ax_weights_rec.set_xlim(xlim)
ax_weights_rec.set_xticks(xticks)
ax_weights_rec.set_xticklabels([])
ax_weights_rec.set_title('recurrent weights', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

ax_weights_dale_rec.set_xlim(xlim)
ax_weights_dale_rec.set_xticks(xticks)
ax_weights_dale_rec.set_xticklabels(xtick_labels)
ax_weights_dale_rec.set_xlabel('training iteration', fontsize=fs)
ax_weights_dale_rec.set_title('recurrent weights (dale)', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

# output weights
for ts, tsd, ws, wsd in zip(times_out, times_dale_out, weights_out, weights_dale_out):
    ax_weights_out.step(ts, ws, lw=lw_weights, color=colors_dict['weights'], alpha=0.55)
    ax_weights_dale_out.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights'], alpha=0.55)

for ts, tsd, ws, wsd in zip(times_out[hl_indices], times_dale_out[hl_indices], weights_out[hl_indices], weights_dale_out[hl_indices]):
    ax_weights_out.step(ts, ws, lw=lw_weights, color=colors_dict['weights_hl'])
    ax_weights_dale_out.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_hl'])

ax_weights_out.set_xlim(xlim)
ax_weights_out.set_xticks(xticks)
ax_weights_out.set_xticklabels([])
ax_weights_out.set_title('output weights', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

ax_weights_dale_out.set_xlim(xlim)
ax_weights_dale_out.set_xticks(xticks)
ax_weights_dale_out.set_xticklabels(xtick_labels)
ax_weights_dale_out.set_xlabel('training iteration', fontsize=fs)
ax_weights_dale_out.set_title('output weights (dale)', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

# loss
step_loss = 10
times_loss = step_loss*np.arange(len(loss_tf_mean))
# TF
simd = sim_dict['losses']
ax_loss.plot(times_loss, loss_tf_mean, color=colors_dict['loss_tf'], lw=lw, label=simd['labels'][2])
ax_loss.fill_between(times_loss, (loss_tf_mean - loss_tf_std), (loss_tf_mean + loss_tf_std),
        color=colors_dict['loss_tf'], alpha=alpha)
# NEST Dale's law
ax_loss.plot(times_loss, loss_nest_dale_mean, color=colors_dict['loss_nest_dale'], lw=lw, label=simd['labels'][1])
ax_loss.fill_between(times_loss, (loss_nest_dale_mean - loss_nest_dale_std), (loss_nest_dale_mean + loss_nest_dale_std),
        color=colors_dict['loss_nest_dale'], alpha=alpha)
# NEST
ax_loss.plot(times_loss, loss_nest_mean, color=colors_dict['loss_nest'], lw=lw, label=simd['labels'][0])
ax_loss.fill_between(times_loss, (loss_nest_mean - loss_nest_std), (loss_nest_mean + loss_nest_std),
        color=colors_dict['loss_nest'], alpha=alpha)

ax_loss.set_xlim([0.0, 2000.0])
ax_loss.set_xticks([0.0, 500.0, 1000.0, 1500.0, 2000.0])
ax_loss.set_xticklabels(['0', '500', '1000', '1500', '2000'])

ax_loss.set_ylim([0.0, 200.0])
ax_loss.set_yticks([0.0, 100.0, 200.0])
ax_loss.set_yticklabels(['0', '100', '200'])
ax_loss.set_xlabel('training iteration', fontsize=fs)
ax_loss.set_ylabel('loss', fontsize=fs)
ax_loss.legend(ncol=3, fontsize=fs-4, loc='upper center')

'''
create figure for membrane potential, learning signal, pseudo derivative, ...
'''
seq_len = 1000

lw = 1.0
width = 6.0
n_h_panels = 1
n_v_panels = 6
p_h_factor = 0.15
p_w_factor = 1.19
p_h_off = -0.02
p_v_off = 0.09
step_off = -0.47
title_position = 'center'

panel_factory2 = pf.create_fig_PRL(
    2, scale, width, n_h_panels, n_v_panels, voffset=0.0, scale_height=0.2)
fig2 = panel_factory2.return_figure()

ax_Vm_rec = panel_factory2.new_panel(
    0, 0, 'A', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+5*step_off, panel_width_factor=p_w_factor)

ax_pseudo_deriv = panel_factory2.new_panel(
    0, 1, 'B', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+4*step_off, panel_width_factor=p_w_factor)

ax_elig_rec = panel_factory2.new_panel(
    0, 2, 'C', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+3*step_off, panel_width_factor=p_w_factor)

ax_grad_rec = panel_factory2.new_panel(
    0, 3, 'D', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+2*step_off, panel_width_factor=p_w_factor)

ax_Vm_out = panel_factory2.new_panel(
    0, 4, 'E', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+1*step_off, panel_width_factor=p_w_factor)

ax_ls_out = panel_factory2.new_panel(
    0, 5, 'F', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off, panel_width_factor=p_w_factor)


simd = sim_dict['traces_rec']
from_ind = simd['seq_len']*simd['from_iter']
to_ind = simd['seq_len']*simd['to_iter']
times_rec = np.arange(to_ind - from_ind)

alphas = np.linspace(0.4, 1.0, len(simd['indices']))[::-1]
for (nrn_ind, label, alpha) in zip(simd['indices'], simd['labels'], alphas):
    ax_Vm_rec.plot(times_rec, Vms_rec[nrn_ind,from_ind:to_ind], lw=lw, color=colors_dict['Vm_rec'], alpha=alpha,
            zorder=1, label=label)
    ax_pseudo_deriv.plot(times_rec, pseudo_deriv_rec[nrn_ind,from_ind:to_ind], lw=lw, color=colors_dict['pseudo_deriv'],
            alpha=alpha, label=label)

ax_Vm_rec.set_xlim([times_rec[0], times_rec[-1]])
ax_Vm_rec.set_xticks([0.0, 500.0, 1000.0])
ax_Vm_rec.set_xticklabels([])
ax_Vm_rec.legend(ncol=3, fontsize=fs-4, loc='upper left')
ax_Vm_rec.set_title(r'$V_{m}$ and spikes (recurrent)', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

simd = sim_dict['spikes_rec']
n_idx = len(simd['indices'])
alphas = np.linspace(0.4, 1.0, n_idx)[::-1]
for i, (nrn_ind, alpha) in enumerate(zip(simd['indices'], alphas)):
    for t in np.mod(np.array(simd['spike_times'][nrn_ind]), 1000):
        ax_Vm_rec.axvline(t - 1, i / float(n_idx), (i+1) / float(n_idx), lw=lw, color=colors_dict['Vm_rec'],
                alpha=alpha, zorder=2.5)

ax_pseudo_deriv.set_xlim([times_rec[0], times_rec[-1]])
ax_pseudo_deriv.set_xticks([0.0, 500.0, 1000.0])
ax_pseudo_deriv.set_xticklabels([])
ax_pseudo_deriv.legend(ncol=3, fontsize=fs-4, loc='upper left')
ax_pseudo_deriv.set_title('pseudo derivative (recurrent)', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

simd = sim_dict['syn_traces_rec']
alphas = np.linspace(0.4, 1.0, len(simd['data_labels']))[::-1]
for (k, label, alpha) in zip(simd['data_labels'], simd['labels'], alphas):
    syn_data = simd[k]
    ax_elig_rec.plot((syn_data[simd['idx_t']] - 2) % 1000, syn_data[simd['idx_elig']], lw=lw, color=colors_dict['elig'],
            alpha=alpha, label=label)
    #ax_elig_rec.plot(syn_data[simd['idx_t']] % 1002, syn_data[simd['idx_pseudo']], lw=lw, color=colors_dict['target'],
    #        alpha=alpha)
    #ax_elig_rec.plot((syn_data[simd['idx_t']] % 1002) - 2, syn_data[simd['idx_z_hat']], lw=lw, color=colors_dict['ls_rec'],
    #        alpha=alpha)
    ax_grad_rec.plot((syn_data[simd['idx_t']] - 2) % 1000 - 2, syn_data[simd['idx_grad']], lw=lw, color=colors_dict['grad'],
            alpha=alpha, label=label)

ax_elig_rec.set_xlim([times_rec[0], times_rec[-1]])
ax_elig_rec.set_xticks([0.0, 500.0, 1000.0])
ax_elig_rec.set_xticklabels([])
ax_elig_rec.legend(ncol=3, fontsize=fs-4, loc='upper left')
ax_elig_rec.set_title('eligibility trace (recurrent)', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

ax_grad_rec.set_xlim([times_rec[0], times_rec[-1]])
ax_grad_rec.set_xticks([0.0, 500.0, 1000.0])
ax_grad_rec.set_xticklabels([])
ax_grad_rec.legend(ncol=3, fontsize=fs-4, loc='upper left')
ax_grad_rec.set_title('cumulated gradient (recurrent)', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

simd = sim_dict['traces_out']
from_ind = simd['seq_len']*simd['from_iter']
to_ind = simd['seq_len']*simd['to_iter']
times_out = np.arange(to_ind - from_ind)

ax_Vm_out.plot(times_out, Vms_out[0,from_ind:to_ind], lw=lw, color=colors_dict['Vm_out'], label=r'$y$')
ax_Vm_out.plot(times_out, target_rate[0,from_ind:to_ind], lw=lw, color=colors_dict['target'], label=r'$y^{\ast}$')
ax_ls_out.plot(times_out, learning_signal_out[0,from_ind:to_ind], lw=lw, color=colors_dict['ls_out'], label=r'$y - y^{\ast}$')

ax_Vm_out.set_xlim([times_rec[0], times_rec[-1]])
ax_Vm_out.set_xticks([0.0, 500.0, 1000.0])
ax_Vm_out.set_xticklabels([])
ax_Vm_out.legend(ncol=2, fontsize=fs-4, loc='upper left')
ax_Vm_out.set_title('output and target', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

ax_ls_out.set_xlim([times_rec[0], times_rec[-1]])
ax_ls_out.set_xticks([0.0, 500.0, 1000.0])
ax_ls_out.set_xticklabels(['0', '500', '1000'])
ax_ls_out.set_xlabel(r'$t$', fontsize=fs)
ax_ls_out.legend(ncol=1, fontsize=fs-4, loc='upper left')
ax_ls_out.set_title('error sigal', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

if not args.path is None:
    figpath = args.path + 'fig_1_weights_loss'
    fig.savefig(figpath + '.svg')
    fig.savefig(figpath + '.pdf', dpi=600)
    print('saved figure as {} and {}'.format(figpath + '.svg', figpath + '.pdf'))
    figpath = args.path + 'fig_1_traces'
    fig2.savefig(figpath + '.svg')
    fig2.savefig(figpath + '.pdf', dpi=600)
    print('saved figure as {} and {}'.format(figpath + '.svg', figpath + '.pdf'))
else:
    plt.show()

