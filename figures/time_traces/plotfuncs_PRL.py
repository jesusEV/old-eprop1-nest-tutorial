import matplotlib as mp
import numpy as np
#mp.use('GTKAgg')
from matplotlib.pylab import *
import copy

# font for labels
#font = {'fontname' : 'Arial', 'color' : 'k', 'fontweight' : 'bold', 'fontsize' : 24}

# define some gray levels
black = (0.0,0.0,0.0)
darkgray = (0.25, 0.25, 0.25)
midgray = (0.5, 0.5, 0.5)
lightgray = (0.75, 0.75, 0.75)
white = (1.0, 1.0, 1.0)



class panel_factory():
    """Class that generates a subpanel in the PloS figure"""

    def __init__(self, scale, figure, n_pan_x, n_pan_y, hoffset, voffset, hspace, vspace, panel_width, panel_height):
        self.scale = scale
        self.figure = figure
        self.n_pan_x = n_pan_x
        self.n_pan_y = n_pan_y
        self.voffset = voffset
        self.hoffset = hoffset
        self.voffset = voffset
        self.hspace = hspace
        self.vspace = vspace
        self.panel_width = panel_width
        self.panel_height = panel_height

    def return_figure(self):
        return self.figure

    def new_panel(self, nx, ny, label, label_position = 'center',voffset = 0.,hoffset = 0.,
            panel_height_factor = 1., panel_width_factor = 1.):
        """Create new panel with an axes object at position nx, ny"""

        assert(nx >= 0 and nx < self.n_pan_x)
        assert(ny >= 0 and ny < self.n_pan_y)

        pos = [self.hoffset  + hoffset+ nx*(self.hspace + self.panel_width),
               voffset + self.voffset + (self.n_pan_y-ny-1)*(self.vspace + self.panel_height),
               self.panel_width*panel_width_factor, self.panel_height * panel_height_factor]

        ## generate axes object
        ax = axes(pos)

        ## panel labels
        if label != '' :
        ## workaround to adjust label both for vertically and horizontally aligned panels
            if self.panel_width > self.panel_height :
                y = 1.03
            else :
                y = 1.01
            if label_position == 'center' :
                label_pos = [0.33,y] # position of panel label (relative to each subpanel)
            elif label_position == 'left' :
                label_pos = [-0.08,y] # position of panel label (relative to each subpanel)
            elif label_position == 'leftleft' :
                label_pos = [-0.25 / panel_width_factor,y] # position of panel label (relative to each subpanel)
            elif type(label_position) == float :
                label_pos = [label_position,y] # position of panel label (relative to each subpanel)

            label_fs = self.scale * 12           # fontsize of panel label

            text(label_pos[0],label_pos[1], r'\bfseries{}' + label,\
                   fontdict={'fontsize' : label_fs,
                             'weight' : 'bold',
                             'horizontalalignment' : 'left',
                             'verticalalignment' : 'bottom'},
               transform = ax.transAxes)

            #pl.locator_params(axis='y', nbins=3)
            #pl.locator_params(axis='x', nbins=3)
            ax.spines['right'].set_color('none')
            ax.spines['top'].set_color('none')
            ax.yaxis.set_ticks_position("left")
            ax.xaxis.set_ticks_position("bottom")
            ax.tick_params(length=3)

        return ax

    def new_empty_panel(self, nx, ny, label, label_position = 'left') :
        """Create new panel at position nx, ny"""

        assert(nx >= 0 and nx < self.n_pan_x)
        assert(ny >= 0 and ny < self.n_pan_y)

        pos = [self.hoffset + nx*(self.hspace + self.panel_width),
               self.voffset + (self.n_pan_y-ny-1)*(self.vspace + self.panel_height),
               self.panel_width, self.panel_height]


        ax = axes(pos, frameon = False)
        xticks([])
        yticks([])


        ## panel label
        if label != '' :
        ## workaround to adjust label both for vertically and horizontally aligned panels
            if self.panel_width > self.panel_height :
                y = 1.03
            else :
                y = 1.01
            if label_position == 'center' :
                label_pos = [0.33,y] # position of panel label (relative to each subpanel)
            elif label_position == 'left' :
                label_pos = [0.0,y] # position of panel label (relative to each subpanel)
            elif label_position == 'leftleft' :
                label_pos = [-0.2,y] # position of panel label (relative to each subpanel)
            elif type(label_position) == float :
                label_pos = [label_position,y] # position of panel label (relative to each subpanel)
            label_fs = self.scale * 10           # fontsize of panel label


            text(label_pos[0],label_pos[1], r'\bfseries{}' + label,\
                   fontdict={'fontsize' : label_fs,
                             'weight' : 'bold',
                             'horizontalalignment' : 'left',
                             'verticalalignment' : 'bottom'},
                    transform = ax.transAxes)

        return pos

def create_fig_PRL(fig, scale, width, n_horz_panels, n_vert_panels, scale_height = 1.,
        hoffset = 0.1, voffset =0.18, squeeze = 0.25, aspect_ratio_1 = False, fs_factor=18.0):
    """Create PRL figure"""

    panel_wh_ratio = (1. + np.sqrt(5)) / 2. # golden ratio
    if aspect_ratio_1:
        panel_wh_ratio = 1

    height = width / panel_wh_ratio * n_vert_panels / n_horz_panels

    rcParams['figure.figsize'] = (width, scale_height * height)

    # resolution of figures in dpi
    # does not influence eps output
    rcParams['figure.dpi'] = 300

    # font
    rcParams['font.size'] = scale*fs_factor
    rcParams['legend.fontsize'] = scale*fs_factor
    rcParams['font.family'] = 'serif'
    rcParams['font.serif'] = 'Computer Modern Roman'

    rcParams['lines.linewidth'] = scale*1.0

    # size of markers (points in point plots)
    rcParams['lines.markersize'] = scale * 3.0
    rcParams['patch.linewidth'] = scale * 1.0
    rcParams['axes.linewidth'] = scale * 1.0     # edge linewidth

    # ticks distances
    rcParams['xtick.major.size'] = scale * 4      # major tick size in points
    rcParams['xtick.minor.size'] = scale * 2      # minor tick size in points
    rcParams['lines.markeredgewidth'] = scale * 0.5  # line width of ticks
    rcParams['grid.linewidth'] = scale * 0.5
    rcParams['xtick.major.pad'] = scale * 4      # distance to major tick label in points
    rcParams['xtick.minor.pad'] = scale * 4      # distance to the minor tick label in points
    rcParams['ytick.major.size'] = scale * 4      # major tick size in points
    rcParams['ytick.minor.size'] = scale * 2      # minor tick size in points
    rcParams['ytick.major.pad'] = scale * 4      # distance to major tick label in points
    rcParams['ytick.minor.pad'] = scale * 4      # distance to the minor tick label in points

    # ticks textsize
    rcParams['ytick.labelsize'] = scale * fs_factor
    rcParams['xtick.labelsize'] = scale * fs_factor

    # use latex to generate the labels in plots
    # not needed anymore in newer versions
    # using this, font detection fails on adobe illustrator 2010-07-20
    rcParams['text.usetex'] = True
    #rcParams['text.latex.preamble']=[r"\usepackage{amsmath} \usepackage[helvet]{sfmath}"]
    rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

    rcParams['ps.useafm'] = False   # use of afm fonts, results in small files
    rcParams['ps.fonttype'] = 3    # Output Type 3 (Type3) or Type 42 (TrueType)

    #mp.rcParams['ps.usedistiller'] = 'xpdf'
    #mp.rcParams['ps.usedistiller'] = 'ghostscript'
    #mp.rcParams['ps.distiller.res'] = 12000
    fig = figure(fig)
    clf()

    panel_width = 1. / n_horz_panels*0.75   # relative width of each subpanel
    hspace = 1. / n_horz_panels*squeeze        # horizontal space between subpanels

    panel_height = 1. / n_vert_panels*0.70/scale_height   # relative height of each subpanel
    vspace = 1. / n_vert_panels*0.25         # vertical space between subpanels

    hoffset=hoffset                          # left margin (relative coordinates)
    voffset=voffset / n_vert_panels     # bottom margin (relative coordinates)

    return panel_factory(scale, fig, n_horz_panels, n_vert_panels, hoffset, voffset, hspace, vspace, panel_width, panel_height)
