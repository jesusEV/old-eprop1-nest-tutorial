import numpy as np

'''
The following functions are used to create the input and the target signal for the position learning task.
'''

def _compute_end_points(angle, velocity, time, x0, y0):
    x = x0 + velocity * time * np.cos(angle)
    y = y0 + velocity * time * np.sin(angle)
    return x, y


def _compute_positions(angles, velocities, times, dt, seq_len_steps):
    positions = np.zeros((2, seq_len_steps))
    x_start = 0.0
    y_start = 0.0
    sum_steps = 0
    for (a, v, t) in zip(angles, velocities, times):
        n_steps = int(np.around(t / dt))
        ts = (1 + np.arange(n_steps)) * dt
        positions[0, sum_steps:sum_steps + n_steps] = x_start + v * ts * np.cos(a)
        positions[1, sum_steps:sum_steps + n_steps] = y_start + v * ts * np.sin(a)
        sum_steps += n_steps
        x_start = positions[0, sum_steps - 1]
        y_start = positions[1, sum_steps - 1]

    return np.arange(seq_len_steps) * dt, positions


def generate_input_spikes_and_target(n_in_angle, n_in_velocity, seq_len, n_batch, dt, f_norm, f_var, vmin=0.05, vmax=0.09,
                          xmax=200.0, ymax=200.0, plot_input=False):
    '''
    generates the input spikes that encode the angel and the velocity. It also generates the corresponding target signal
    which is the position in x- and y-direction.
    returns:
        spike_times_all (a list of arrays that contain the spike times of the spike generators)
        target_times_all (array)
        target_signal_all (array of shape(2, len(target_times_all)))
    '''
    seq_len_steps = int(np.around(seq_len / dt))
    n_in = n_in_angle + n_in_velocity
    spike_times_all = []
    target_times_all = np.zeros((n_batch*seq_len_steps))
    target_signal_all = np.zeros((2, n_batch*seq_len_steps))
    for b in np.arange(n_batch):
        ind_start = b*seq_len_steps
        ind_end = (b + 1)*seq_len_steps
        (times, t_cumsum, angles, bin_centers_a,
                velocities, bin_centers_v, end_positions, spike_times) = generate_input_spikes_single_trial(
                        n_in_angle, n_in_velocity, seq_len, dt, f_norm, f_var, vmin, vmax, xmax, ymax)
        if plot_input:
            _plot_input(t_cumsum, angles, bin_centers_a, velocities, bin_centers_v, spike_times)
            _plot_positions(t_cumsum, end_positions, xmax, ymax, target_times=None, target_signal=None)

        spike_times_all.append(spike_times)
        tt, ts = _generate_target_signal(times, angles, velocities, dt, seq_len, xmax, ymax)
        target_times_all[ind_start:ind_end] = tt + ind_start*dt
        target_signal_all[:,ind_start:ind_end] = ts

    spike_times_all = [np.concatenate([spkt[i] + b*seq_len for b, spkt in enumerate(spike_times_all)]) for i in np.arange(n_in)]
    return spike_times_all, target_times_all, target_signal_all


def generate_input_spikes_single_trial(n_in_angle, n_in_velocity, seq_len, dt, f_norm, f_var, vmin=0.05, vmax=0.09,
                          xmax=200.0, ymax=200.0):
    # TODO: remove seed!
    # np.random.seed(302687)
    n_in = n_in_angle + n_in_velocity
    f_var_angle = f_var  # / (2.0*np.pi)**2
    f_var_velocity = f_var  # / (vmax - vmin)**2
    angles = []
    velocities = []
    x_points = []
    y_points = []
    f_gauss_angles = []
    f_gauss_velocities = []
    t_low = int(seq_len / 10.0)
    t_high = int(seq_len / 4.0)
    times = []
    sum_times = 0.0
    seq_len_steps = int(np.around(seq_len / dt))
    # draw times at which a change of angle and velocity occours
    while sum_times < seq_len - t_high:
        t = t_low + int(np.around(np.random.rand() * (t_high - t_low) / dt)) * dt
        times.append(t)
        sum_times += t

    times.append(int(np.around((seq_len - sum_times) / dt)) * dt)
    t_cumsum = np.cumsum(np.concatenate(([0.0, ], times)))

    def gauss_angle(xs, a0): return (
        f_norm * np.exp(-0.5 * np.minimum(np.abs(xs - a0), 1.0 - np.abs(xs - a0))**2 / f_var_angle) /
        np.sqrt(2.0 * np.pi * f_var_angle))

    def gaus_velocity(xs, v0): return (
        f_norm * np.exp(-0.5 * (xs - v0)**2 / f_var_velocity) / np.sqrt(2.0 * np.pi * f_var_velocity))

    rates_angle = np.zeros(n_in_angle)
    rates_velocities = np.zeros(n_in_velocity)
    rates_extended = np.zeros((n_in_angle + n_in_velocity, seq_len_steps))
    n_eval = 1000
    dx_eval_angle = 1.0 / (n_eval * n_in_angle)
    xs_angle = np.linspace(-0.5 / n_in_angle, 0.5 / n_in_angle, n_eval, endpoint=False)
    bin_centers_a = 2.0 * np.pi * (np.arange(n_in_angle) - 0.5) / n_in_angle
    dx_v = (1.0 + 6.0 * np.sqrt(f_var_velocity)) / n_in_velocity
    dx_eval_velocity = dx_v / n_eval
    xmin_v = -3.0 * np.sqrt(f_var_velocity)
    bin_centers_v = (vmax - vmin) * (xmin_v + (0.5 + np.arange(n_in_velocity)) * dx_v) + vmin
    xfirst_v = xmin_v + dx_v
    xs_velocity = np.linspace(xmin_v, xfirst_v, n_eval, endpoint=False)
    x_positions = np.zeros(seq_len_steps)
    y_positions = np.zeros(seq_len_steps)
    end_positions = np.zeros((2, len(t_cumsum)))
    for i, (t, t_cs) in enumerate(zip(times, t_cumsum[:-1])):
        is_in_box = False
        while not is_in_box:
            # draw random angle
            #predef = np.array([[0.15, 0.68, 0.3, 0.89], [0.025, 0.039, 0.029, 0.035]])
            #predef = np.array([[0.9, 0.7, 0.3, 0.15], [0.021, 0.039, 0.025, 0.037]])
            #p_ind = np.random.randint(0, len(predef[0]))
            rnd_a = np.random.rand()
            #rnd_a = predef[0,p_ind]
            rnd_angle = 2.0 * np.pi * rnd_a
            # draw random valocity
            rnd_v = np.random.rand()
            #rnd_v = predef[1,p_ind]
            rnd_velocity = vmin + (vmax - vmin) * rnd_v
            # compute end position of current segment
            x_end, y_end = _compute_end_points(rnd_angle, rnd_velocity, t,
                                               end_positions[0, i], end_positions[1, i])
            if (np.abs(x_end) < xmax and np.abs(y_end) < ymax):
                is_in_box = True
                end_positions[0, i + 1] = x_end
                end_positions[1, i + 1] = y_end
            else:
                print('not in box')

        # save angle and velocity
        angles.append(rnd_angle)
        velocities.append(rnd_velocity)
        # compute rates for rate generators that represent the angle
        for j in np.arange(n_in_angle):
            rates_angle[j] = (np.sum(
                gauss_angle(xs_angle + float(j) / float(n_in_angle), rnd_a)) * dx_eval_angle)

        # compute rates for rate generators that represent the velocity
        for j in np.arange(n_in_velocity):
            rates_velocities[j] = (np.sum(
                gaus_velocity(xs_velocity + float(j) * dx_v, rnd_v)) * dx_eval_velocity)

        n_time_steps = int(np.around(t / dt))
        total_step = int(np.around(t_cs / dt))
        rates = np.tile(np.concatenate((rates_angle, rates_velocities)), (n_time_steps, 1)).T
        rates_extended[:, total_step:total_step + n_time_steps] = rates

    rnd_numbers = np.random.rand(n_in_angle + n_in_velocity, seq_len_steps)
    spike_array = rnd_numbers < (rates_extended / 1000.0)
    # set first column to False since spikes at t = 0 are not allowed
    spike_array[:, 0] = False
    # set the last column to zero since these spikes won't reach their target before the end of the simulation
    # anyway
    spike_array[:, -1] = False
    ind = np.where(spike_array)
    spike_times = []
    for i in np.arange(n_in_angle + n_in_velocity):
        spike_times.append((ind[1][np.where(ind[0] == i)]) * dt)

    return times, t_cumsum, angles, bin_centers_a, velocities, bin_centers_v, end_positions, spike_times


def _plot_input(t_cumsum, angles, bin_centers_a, velocities, bin_centers_v, spike_times, frozen_p_n=None):
    from matplotlib import pyplot as plt
    n_in = len(spike_times)
    n_in_angle = int(n_in / 2)
    n_in_velocity = n_in - n_in_angle
    if frozen_p_n is not None:
        fig1, (axA, axB, axC) = plt.subplots(3, 1, sharex=True)
    else:
        fig1, (axA, axB) = plt.subplots(2, 1, sharex=True)

    for gen_id, t_sp in enumerate(spike_times[:n_in_angle]):
        axA.scatter(t_sp, gen_id * np.ones_like(t_sp), marker='d')

    for gen_id, t_sp in enumerate(spike_times[n_in_angle:]):
        axB.scatter(t_sp, gen_id * np.ones_like(t_sp), marker='d')

    if frozen_p_n is not None:
        for gen_id, t_sp in enumerate(frozen_p_n):
            axC.scatter(t_sp, gen_id * np.ones_like(t_sp), marker='d')

    b0_a = bin_centers_a[0]
    b1_a = bin_centers_a[-1]
    resc_a = n_in_angle / (b1_a - b0_a)
    b0_v = bin_centers_v[0]
    b1_v = bin_centers_v[-1]
    resc_v = n_in_velocity / (b1_v - b0_v)
    axA.vlines(t_cumsum, 0, n_in_angle, color='k')
    axB.vlines(t_cumsum, 0, n_in_velocity, color='k')
    axA.hlines((angles - b0_a) * resc_a, t_cumsum[:-1], t_cumsum[1:], color='b')
    axB.hlines((velocities - b0_v) * resc_v, t_cumsum[:-1], t_cumsum[1:], color='b')
    axA.title.set_text('input angle')
    axA.set_ylabel(r'id spike_gen $\alpha$')
    axB.title.set_text('input velocity')
    axB.set_ylabel(r'id spike_gen $v$')
    axB.set_xlabel(r'$t$[ms]')

    plt.show()


def _plot_positions(t_cumsum, end_positions, xmax, ymax, target_times=None, target_signal=None):
    from matplotlib import pyplot as plt
    lw = 3.0
    if target_signal is None:
        fig1, axA = plt.subplots()
    else:
        fig1, (axA, axB) = plt.subplots(2, 1, sharex=False)

    axA.plot(end_positions[0], end_positions[1], lw=lw, color='g')
    axA.scatter(end_positions[0], end_positions[1], marker='d', color='b')
    axA.set_xlim((-xmax, xmax))
    axA.set_ylim((-ymax, ymax))
    if target_signal is not None:
        axB.plot(target_times, target_signal[0], lw=lw, color='deepskyblue')
        axB.plot(target_times, target_signal[1], lw=lw, color='limegreen')

    plt.show()


def _generate_target_signal(times, angles, velocities, dt, seq_len, xmax, ymax, Vmmax=0.2, Vmmin=-0.2):
    target_times, positions = _compute_positions(angles, velocities, times, dt, int(np.around(seq_len / dt)))
    Vms_x = (0.5 * positions[0] / xmax + 0.5) * (Vmmax - Vmmin) + Vmmin
    Vms_y = (0.5 * positions[1] / ymax + 0.5) * (Vmmax - Vmmin) + Vmmin
    target_signal = np.array([Vms_x, Vms_y])
    return target_times + dt, target_signal


def _generate_fin_pos_target_signal(times, angles, velocities, dt, seq_len, t_to_recall, t_recall,
                                   xmax, ymax, Vmmax=0.1, Vmmin=-0.1):
    _, positions = _compute_positions(angles, velocities, times, dt, int(np.around(seq_len / dt)))
    target_times = t_to_recall + dt * np.arange(int(np.around(t_recall / dt)))
    x_final_position = positions[0, -1]
    y_final_position = positions[1, -1]
    Vms_x = (0.5 * x_final_position / xmax + 0.5) * (Vmmax - Vmmin) + Vmmin
    Vms_y = (0.5 * y_final_position / ymax + 0.5) * (Vmmax - Vmmin) + Vmmin
    target_signal = np.array([Vms_x * np.ones_like(target_times), Vms_y * np.ones_like(target_times)])
    return target_times + dt, target_signal


'''
The following functions are used to generate the target signal for the pattern generation task.
'''


def _sum_of_sines_target(seq_len, n_sines, periods, weights, phases, normalize, seed):
    '''
    !!! copied of TF code from Graz (Bellec et al.) !!!
    Generate a target signal as a weighted sum of sinusoids with random weights and phases.
    :param n_sines: number of sinusoids to combine
    :param periods: list of sinusoid periods
    :param weights: weight assigned the sinusoids
    :param phases: phases of the sinusoids
    :return: one dimensional vector of size seq_len contained the weighted sum of sinusoids
    '''
    np.random.seed(seed)
    if periods is None:
        periods = [np.random.uniform(low=100, high=1000) for i in range(n_sines)]
    assert n_sines == len(periods)
    sines = []
    weights = np.random.uniform(low=0.5, high=2, size=n_sines) if weights is None else weights
    phases = np.random.uniform(low=0., high=np.pi * 2, size=n_sines) if phases is None else phases
    for i in range(n_sines):
        sine = np.sin(np.linspace(0 + phases[i], np.pi * 2 * (seq_len // periods[i]) + phases[i], seq_len))
        sines.append(sine * weights[i])

    output = sum(sines)
    if normalize:
        output = output - output[0]
        scale = max(np.abs(np.min(output)), np.abs(np.max(output)))
        output = output / np.maximum(scale, 1e-6)
    return output

def generate_sin_target(seq_len, n_out, n_sines=4, periods=[1000, 500, 333, 200],
        weights=None, phases=None, normalize=True, seed=302687):
    '''
    returns target signal for pattern generation task which is a superposition of sin waves.
    returns an array of n_out array with seq_len entries.
    '''
    target_sinusoidal_outputs = np.array(
        [_sum_of_sines_target(int(seq_len), n_sines, periods,
            weights, phases, normalize, seed) for i in range(n_out)])
    return target_sinusoidal_outputs


'''
The following function is used to create the input and the target signal for the evidence accumulation task.
'''

def generate_click_task_data(batch_size, seq_len, n_neuron, recall_duration, dt = 1.0, p_group=0.5, f0=0.5,
                             n_cues=7, t_cue=100, t_interval=150,
                             n_input_symbols=4, rnd_seed=None):
    '''
    !!! copied of TF code from Graz (Bellec et al.) !!!
    (with some modifications)
    '''
    if rnd_seed is None:
        rng = np.random.default_rng()
    else:
        rng = np.random.default_rng(rnd_seed)

    n_channel = n_neuron // n_input_symbols

    # randomly assign group A and B
    prob_choices = np.array([p_group, 1 - p_group], dtype=np.float32)
    idx = rng.choice([0, 1], batch_size)
    probs = np.zeros((batch_size, 2), dtype=np.float32)
    # assign input spike probabilities
    probs[:, 0] = prob_choices[idx]
    probs[:, 1] = prob_choices[1 - idx]

    cue_assignments = np.zeros((batch_size, n_cues), dtype=np.int)
    correct_solutions = np.zeros(batch_size, dtype=np.int)
    # for each example in batch, draw which cues are going to be active (left or right)
    for b in range(batch_size):
        cues = rng.choice([0, 1], n_cues, p=probs[b])
        cue_assignments[b, :] = cues
        correct_solutions[b] = np.around(np.mean(cues))

    # generate input nums - 0: left, 1: right, 2:recall, 3:background noise
    # generate input spikes
    input_spike_prob = np.zeros((batch_size, seq_len, n_neuron))
    d_silence = t_interval - t_cue
    for b in range(batch_size):
        for k in range(n_cues):
            # input channels only fire when they are selected (left or right)
            c = cue_assignments[b, k]
            # reverse order of cues
            #idx = sequence_length - int(recall_cue) - k - 1
            idx = k
            input_spike_prob[b, d_silence+idx*t_interval:d_silence+idx*t_interval+t_cue, c*n_channel:(c+1)*n_channel] = f0

    # recall cue
    input_spike_prob[:, -recall_duration:, 2*n_channel:3*n_channel] = f0
    # background noise
    input_spike_prob[:, :, 3*n_channel:] = f0/4.
    rnd_numbers = rng.random((batch_size, seq_len, n_neuron))
    # set first and last entry of a sequence to 0 since spikes at t = 0 are not allowed
    # and lead to unexpected behaviour in tf code for t = T.
    input_spike_prob[:, 0, :] = 0.0
    input_spike_prob[:, -1, :] = 0.0
    spike_array = rnd_numbers < input_spike_prob
    # convert to shape (n_neuron x n_batch*seq_len)
    spike_array = np.array([np.concatenate(spike_array[:,:,n]) for n in np.arange(n_neuron)])
    ind = np.where(spike_array)
    spike_times = []
    for i in np.arange(n_neuron):
        spike_times.append((ind[1][np.where(ind[0] == i)]) * dt)

    # generate targets
    #target_times = np.arange(batch_size*seq_len) + 1
    #target_rates = np.zeros((2, batch_size*seq_len))
    target_times = np.zeros(batch_size)
    target_rates = np.zeros((2, batch_size))
    for b, s in enumerate(correct_solutions):
        start_t = b*seq_len + 1
        #end_t = (b+1)*seq_len
        start_recall = (b+1)*seq_len - recall_duration
        #target_rates[s, start_t:end_t] = 1.0
        target_times[b] = start_t
        target_rates[s, b] = 1.0

    return spike_times, target_times, target_rates, cue_assignments, correct_solutions


'''
The following function is used to create the input and the target signal for the n bit task.
'''

def generate_n_bit_task_data(batch_size, seq_len, stimulus_duration, stimulus_rate,
        dt=1.0, n_channels=3, seed=None):
    '''
    '''
    if seed is None:
        rng = np.random.default_rng()
    else:
        rng = np.random.default_rng(seed)

    input_times = [np.array([]) for i in np.arange(2*n_channels)]
    input_rates = [np.array([]) for i in np.arange(2*n_channels)]
    target_times = [np.array([]) for i in np.arange(n_channels)]
    target_rates = [np.array([]) for i in np.arange(n_channels)]
    # loop over batch size and concatenate data
    for b in np.arange(batch_size):
        t_start = b*seq_len
        its, irs, tts, trs = generate_n_bit_task_data_single_batch(rng, seq_len, stimulus_duration,
                stimulus_rate, dt, n_channels)
        #print('its: {}, irs: {}, tts: {}, trs: {}'.format(len(its), len(irs), len(tts), len(trs)))
        for i, (it, ir) in enumerate(zip(its, irs)):
            #print('it: {}, ir: {}'.format(len(it), len(ir)))
            input_times[i] = np.concatenate((input_times[i], np.array(it) + t_start))
            input_rates[i] = np.concatenate((input_rates[i], np.array(ir)))

        for i, (tt, tr) in enumerate(zip(tts, trs)):
            #print('tt: {}, tr: {}'.format(len(tt), len(tr)))
            target_times[i] = np.concatenate((target_times[i], np.array(tt) + t_start))
            target_rates[i] = np.concatenate((target_rates[i], np.array(tr)))

    # add one more data point for better looking plots
    for i in np.arange(len(target_times)):
        target_times[i] = np.append(target_times[i], [batch_size*seq_len + 1])
        target_rates[i] = np.append(target_rates[i], [target_rates[i][-1]])

    # generate continuous version of target rates which is useful to compute the loss
    seq_len_steps = int(seq_len/dt)
    target_rates_cont = np.zeros((n_channels, batch_size*seq_len_steps))
    for i, (tt, tr) in enumerate(zip(target_times, target_rates)):
        target_rates_cont[i,:] = np.concatenate([tr[j]*np.ones(int(tt[j+1] - tt[j])) for j in np.arange(len(tt)-1)])

    return [np.array(input_times), np.array(input_rates), np.array(target_times), np.array(target_rates),
        np.array(target_rates_cont)]

def generate_n_bit_task_data_single_batch(rng, seq_len, stimulus_duration, stimulus_rate,
        dt, n_channels):
    '''
    '''
    # generate times at which stimuli are presented
    t_sum = 0.0
    input_times_all = np.array([])
    while t_sum < (seq_len - stimulus_duration):
        input_times_all = np.concatenate((input_times_all, np.around(
            rng.uniform(2*stimulus_duration, 4*stimulus_duration, int(seq_len / 3)))))
        t_sum = np.sum(input_times_all)

    input_times_all = np.cumsum(input_times_all)
    input_times_all = input_times_all[input_times_all < (seq_len - stimulus_duration)]
    # choose which stimulus is presented
    stimulus_ids = rng.choice(np.arange(2*n_channels), np.size(input_times_all))
    # sort input for channel and compute output rates and times
    input_times = [[1.0,] for i in np.arange(2*n_channels)]
    input_rates = [[0.0,] for i in np.arange(2*n_channels)]
    target_times = [[1.0,] for i in np.arange(n_channels)]
    target_rates = [[-0.5*0.2] for i in np.arange(n_channels)]
    for t, sid in zip(input_times_all, stimulus_ids):
        # append time of start and stop of stimulus
        input_times[sid].append(t)
        input_times[sid].append(t+stimulus_duration)
        # append corresponding rate
        input_rates[sid].append(stimulus_rate)
        input_rates[sid].append(0.0)
        # append corresponding target rate
        target_times[sid//2].append(t)
        target_rates[sid//2].append(0.2*((sid%2.0)-0.5))

    return input_times, input_rates, target_times, target_rates
