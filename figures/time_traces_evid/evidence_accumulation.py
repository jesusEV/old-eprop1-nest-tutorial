import numpy as np
import nest
import argparse
import json
from eprop_class import eprop1 as eprop1
import input_target_funcs as ipf
import time

time1 = time.time()
parser = argparse.ArgumentParser(description='simulation program')
parser.add_argument('--rnd_seed', nargs='+', type = int, help='seed for random number generator')
args = parser.parse_args()
'''
flags
'''
# if True, input, target, and weights are loaded from files that were created in a previous simulation
cont_simulation = False

# if True, the spikes of the recurrent neurons are recorded. use only for short simulation times because
# this occupies a lot of memory
use_spike_detector = False
use_weight_recorder = True
use_mm_rec = True

# if True, time traces of loss, and other variables are plotted. also the weight matrices are shown before
# and after training
show_plots = False

# if True, the weight matrices after training are saved as npy files
save_weights = False

# if True, the eprop synapses are replaced by static synapses so that no learning can take place but the
# simulation takes less time
test_run = False

'''
path where weight matrices are stored
'''
datapath = './weights_evid_acc/'
affix = '_evid_acc_a2a_test'

'''
definition of network and simulation parameters
'''

if args.rnd_seed is None:
    rnd_seed = 302687
else:
    rnd_seed = args.rnd_seed[0]

n_iter = 10
n_batch = 2
t_cue_spacing = 150.0
seq_len = t_cue_spacing * 7 + 1200.0
n_in = 40
n_regular = 50
n_adaptive = 50
n_rec = n_adaptive + n_regular
n_out = 2
dt = 1.0
input_f0 = 40.0 / 1.0e3
nvp = 4
sim_steps = n_iter*n_batch*seq_len
recall_duration = 150
seq_len_steps = int(np.around(seq_len / dt))
# for a short description of the network parameters see eprop_class.py
network_params = {
        # choose which type of network should be used: 'brunel' or 'all-to-all'
        #'network_type': 'brunel',
        'network_type': 'all_to_all',
        'learning_rate': 5.000e-3,
        'n_batch': n_batch,
        'seq_len': seq_len,
        'dt': dt,
        'regression': False,
        't_recall': float(recall_duration),
        'n_iter': n_iter,
        'n_in': n_in,
        'n_regular': n_regular,
        'n_adaptive': n_adaptive,
        'n_out': n_out,
        'target_firing_rate': 20.0,
        'reg': 1.0,
        'nvp': nvp,
        'use_adam': True,
        'rnd_seed': rnd_seed,
        'test_run': test_run,
        }

if cont_simulation:
    # set the path to the connectivity data
    network_params['load_from_file'] = datapath + 'network' + affix + '.pkl'

'''
create object of eprop class
'''
eprop = eprop1(network_params)

'''
generate network
'''
eprop.generate_network(wr=use_weight_recorder)
if network_params['network_type'] == 'all_to_all':
    [in_nrns, rec_nrns, out_nrns, sgs,
            pgs_background, rgs, mm_out] = eprop.return_nrns_dvcs()
    wr_in, wr_rec, wr_out = eprop.return_weight_recorders()
else:
    [in_nrns, rec_exc_nrns, rec_inh_nrns, out_nrns, sgs,
            pgs_background, rgs, mm_out] = eprop.return_nrns_dvcs()
    rec_nrns = rec_exc_nrns + rec_inh_nrns

print('ids recurrent neurons:\n{}'.format(rec_nrns))
'''
create and connect an additional spike detector so that average firing rate can be computed
'''
if use_spike_detector:
    sd_rec = nest.Create('spike_recorder', 1)
    nest.Connect(rec_nrns, sd_rec)
    spike_times_in_all = []

'''
create and connect multimeter for recurrent neurons
'''
if use_mm_rec:
    mm_rec = nest.Create('multimeter', params={'interval': 1.0, 'record_from': ['V_m', 'V_th', 'learning_signal']})
    nest.Connect(mm_rec, rec_nrns)
'''
simulation
'''
cues_all = np.zeros((n_iter, n_batch, 7), dtype=np.int)
solutions_all = np.zeros((n_iter, n_batch), dtype=np.int)

'''
loop over iterations
'''
for it in np.arange(n_iter):
    print('iteration: {}'.format(it))
    t_iter_start = it*seq_len*n_batch
    (spike_times, target_times, target_rates, cues,
            solutions) = ipf.generate_click_task_data(n_batch, seq_len_steps, n_in, recall_duration, f0=input_f0,
                    rnd_seed=rnd_seed+541*it)
    if use_spike_detector:
        spike_times_in_all.append(spike_times)

    cues_all[it,:,:] = cues
    solutions_all[it,:] = solutions
    nest.SetStatus(sgs, params = [{'spike_times': sp_t + t_iter_start} for sp_t in spike_times])
    nest.SetStatus(rgs, params = [{'amplitude_times': target_times + t_iter_start,
        'amplitude_values': ta_s} for ta_s in target_rates])
    nest.Simulate(seq_len*n_batch)

'''
add some simulation time at the end
'''
nest.Simulate(10.)

'''
read out the membrane potential of the error neurons to compute the loss and the recall error
'''
events_out = nest.GetStatus(mm_out)[0]["events"]
senders_out = events_out['senders']
Vms_out = np.array([events_out['V_m'][np.where(senders_out == i)][2:2 + seq_len_steps*n_batch*n_iter] for i in out_nrns.tolist()])
output_logits = np.array([np.reshape(Vm_out, (n_iter, n_batch, seq_len_steps)) for Vm_out in Vms_out])[:, :, :,
        -recall_duration:] # (n_out x n_iter x n_batch x recall_duration)
softmax_y = np.exp(output_logits) / np.sum(np.exp(output_logits), axis=0)  # sum over output neurons in denominator
# (n_out x n_iter x n_batch x recall_duration)
tiled_targets = np.zeros((n_out, n_iter, n_batch, recall_duration))
for i, sol_i in enumerate(solutions_all):
    for b, sol_b in enumerate(sol_i):
        tiled_targets[sol_b,i,b,:] = 1.0

loss = - np.mean(np.sum(tiled_targets * np.log(softmax_y), axis=0), axis=(1, 2))  # sum over output neurons, mean over
# recall and batches (n_iter)
loss_times = np.arange(n_iter)
y_predict = np.argmax(np.mean(output_logits, axis=3), axis=0) # average over time, argmax over neurons
accuracy = np.mean((solutions_all == y_predict), axis=1)  # averaged over batch here
recall_errors = 1. - accuracy

print('loss:\n{}'.format(loss))
print('loss average: {}, loss variance: {}'.format(np.mean(loss), np.var(loss)))
print('accuracy:\n{}'.format(accuracy))
print('recall_errors:\n{}'.format(recall_errors))

fname_loss = './data/loss_accuracy_error_{}.npy'.format(rnd_seed)
np.save(fname_loss, np.array([loss, accuracy, recall_errors]))
print('saved file {}'.format(fname_loss))

'''
membrane potential of readout neuron
'''
if use_mm_rec:
    times_out = np.array([events_out['times'][np.where(senders_out == i)][2:2 + n_iter * n_batch * int(seq_len)] for i in
        out_nrns.tolist()])
    Vms_out = np.array([events_out['V_m'][np.where(senders_out == i)][2:2 + n_iter * n_batch * int(seq_len)] for i in
        out_nrns.tolist()])
    learning_signal_out = np.array([events_out['learning_signal'][np.where(senders_out == i)][2:2 + n_iter * n_batch * int(seq_len)] for i in
        out_nrns.tolist()])
    target_rate = np.array([events_out['target_rate'][np.where(senders_out == i)][2:2 + n_iter * n_batch * int(seq_len)] for i in
        out_nrns.tolist()])
    results_out = np.array([times_out, Vms_out, learning_signal_out, target_rate])
    np.save('./data/time_traces_out_nest.npy', results_out)

'''
readout of multimeter connected to recurrent neurons
'''
if use_mm_rec:
    events_rec = nest.GetStatus(mm_rec)[0]['events']
    senders_rec = events_rec['senders']
    times_rec = np.array([events_rec['times'][np.where(senders_rec == i)][2:2 + n_iter * n_batch * int(seq_len)] for i in
        rec_nrns.tolist()])
    Vms_rec = np.array([events_rec['V_m'][np.where(senders_rec == i)][2:2 + n_iter * n_batch * int(seq_len)] for i in
        rec_nrns.tolist()])
    pseudo_deriv_rec = np.array([events_rec['V_th'][np.where(senders_rec == i)][2:2 + n_iter * n_batch * int(seq_len)] for i in
        rec_nrns.tolist()])
    learning_signal_rec = np.array([events_rec['learning_signal'][np.where(senders_rec == i)][2:2 + n_iter * n_batch * int(seq_len)] for i in
        rec_nrns.tolist()])
    results_rec = np.array([times_rec, Vms_rec, pseudo_deriv_rec, learning_signal_rec])
    np.save('./data/time_traces_rec_nest.npy', results_rec)

'''
read out spike times and compute average firing rate for each iteration
'''
if use_spike_detector:
    spikes_recurrent = nest.GetStatus(sd_rec, "events")[0]
    rec_spike_times_all = np.array(spikes_recurrent['times']) - 2.0*dt
    senders_spikes_rec = spikes_recurrent['senders']
    spike_times_rec = [rec_spike_times_all[np.where(senders_spikes_rec == i)].tolist() for i in rec_nrns.tolist()]
    spike_count = np.zeros((n_iter, n_rec))
    for n, spk in enumerate(spike_times_rec):
        spike_count[:,n] = np.array([np.sum([(np.array(spk) >= i*seq_len*n_batch) & (np.array(spk) < (i + 1)*seq_len*n_batch)]) for i in
            np.arange(n_iter)])

    avg_firing_rate = 1000.0*np.mean(spike_count, axis=1) / seq_len
    print('average firing rates:\n{}'.format(avg_firing_rate))

    res_dict = {
            'senders': rec_nrns.tolist(),
            'spike_times': spike_times_rec,
            }
    datapath = './data/'
    filename = 'spike_times.json'
    with open(datapath + filename, 'w') as f:
        json.dump(res_dict, f)

'''
read out and store weight recordings
'''
if use_weight_recorder:
    wr_dict = {
            'rec_nrns': rec_nrns.tolist()
            }
    wr_events = wr_in.get('events')
    senders = wr_events['senders']
    targets = wr_events['targets']
    times_wr = wr_events['times']
    weights = wr_events['weights']

    wr_dict['senders_in'] = senders.tolist()
    wr_dict['targets_in'] = targets.tolist()
    wr_dict['times_in'] = times_wr.tolist()
    wr_dict['weights_in'] = weights.tolist()

    wr_events = wr_rec.get('events')
    senders = wr_events['senders']
    targets = wr_events['targets']
    times_wr = wr_events['times']
    weights = wr_events['weights']

    wr_dict['senders_rec'] = senders.tolist()
    wr_dict['targets_rec'] = targets.tolist()
    wr_dict['times_rec'] = times_wr.tolist()
    wr_dict['weights_rec'] = weights.tolist()

    wr_events = wr_out.get('events')
    senders = wr_events['senders']
    targets = wr_events['targets']
    times_wr = wr_events['times']
    weights = wr_events['weights']

    wr_dict['senders_out'] = senders.tolist()
    wr_dict['targets_out'] = targets.tolist()
    wr_dict['times_out'] = times_wr.tolist()
    wr_dict['weights_out'] = weights.tolist()

    datapath = './data/'
    filename = 'nest_wr_data_niter{}.json'.format(n_iter)
    with open(datapath + filename, 'w') as f:
        json.dump(wr_dict, f)

    print('saved {}'.format(filename))

'''
print time
'''
time2 = time.time()
print('execution time: {}'.format(time2 - time1))

'''
plot membrane potential and spikes (if recorded)
'''
if show_plots:
    from matplotlib import pyplot as plt
    cue_colors = ['mediumseagreen', 'tomato']
    scale = 1.0
    figsize = (scale*16,scale*9)
    lw = 3.5
    fs = 22
    if not use_spike_detector:
        # axA: loss, axB: membrane potential of readout neurons
        fig1, (axA, axB) = plt.subplots(2, 1, figsize=figsize, sharex=True)
    else:
        # axA: loss and firing rate, axB: membrane potential of readout neurons
        # axC: input spikes, axD: spikes of recurrent neurons
        fig1, (axA, axB, axC, axD) = plt.subplots(4, 1, figsize=figsize, sharex=True)

    ln1 = axA.plot(loss_times + 1., loss, lw=lw, c='cornflowerblue', label='loss')
    axA.set_xlabel('iteration', fontsize=fs)
    axA.set_ylabel('loss', fontsize=fs)
    for i, (sol_i, cues_i) in enumerate(zip(solutions_all, cues_all)):
        iter_start = i*n_batch#*seq_len_steps
        iter_end = (i + 1)*n_batch#*seq_len_steps
        for b, (sol_b, cues_b) in enumerate(zip(sol_i, cues_i)):
            b_start = iter_start + b#*seq_len_steps
            b_end = iter_start + (b + 1)#*seq_len_steps
            axB.axvline(b_end, c='b', lw=lw/2, ls='--')
            axB.axvspan(b_end - recall_duration / seq_len, b_end, facecolor=cue_colors[sol_b], alpha=0.5)
            for c, cue in enumerate(cues_b):
                cue_start = b_start + c*recall_duration / seq_len
                cue_end = cue_start + recall_duration / seq_len
                axB.axvspan(cue_start, cue_end, facecolor=cue_colors[cue], alpha=0.25)

        axB.axvline(iter_end, c='b', lw=lw, ls='-')

    for n, (Vm_out, c) in enumerate(zip(Vms_out, cue_colors)):
        axB.plot(np.arange(len(Vm_out)) / seq_len, Vm_out, c=c, lw=lw, label=r'$V^{{out,{}}}_{{m}}$'.format(n))

    # highlight correct answers
    y_val = axB.get_ylim()[1]
    end_recall = np.arange(n_iter) + 1.
    end_recall_corr = end_recall[np.where(accuracy == 1.0)[0]]
    begin_recall_corr = end_recall_corr - recall_duration / seq_len
    axB.hlines(y=np.ones_like(begin_recall_corr)*y_val, xmin=begin_recall_corr, xmax=end_recall_corr, color='g', lw=10)
    # highlight wrong answers
    end_recall_wr = end_recall[np.where(accuracy == 0.0)[0]]
    begin_recall_wr = end_recall_wr - recall_duration / seq_len
    axB.hlines(y=np.ones_like(begin_recall_wr)*y_val, xmin=begin_recall_wr, xmax=end_recall_wr, color='r', lw=10)

    axB.set_ylabel(r'$V_m$ [mV]', fontsize=fs)
    axB.legend(fontsize=fs)
    if use_spike_detector:
        axA2 = axA.twinx()
        ln2 = axA2.plot(loss_times, avg_firing_rate, lw=lw, c='tomato', label='average firing rate')
        axA2.set_ylabel(r'$\nu$ [Hz]', fontsize=fs)
        for it, spk_in in enumerate(spike_times_in_all):
            iter_start = it*n_batch#*seq_len_steps
            for n, sp_in in enumerate(spk_in):
                axC.scatter(sp_in / seq_len + iter_start, n*np.ones_like(sp_in), marker='o')

        for n, sp_rec in enumerate(spike_times_rec):
            axD.scatter(np.array(sp_rec) / seq_len, n*np.ones_like(sp_rec), marker='o')

        lns = ln1 + ln2
        lbs = [l.get_label() for l in lns]
        axA2.legend(lns, lbs, fontsize=fs)
        axC.set_ylabel('in ID', fontsize=fs)
        axD.set_xlabel(r'$t$ [ms]', fontsize=fs)
        axD.set_ylabel('rec ID', fontsize=fs)
    else:
        axA.legend(fontsize=fs)
        axB.set_xlabel(r'$t$ [ms]', fontsize=fs)

    plt.tight_layout()
    plt.show()

'''
save weight matrices
'''
if save_weights:
    eprop.store_network(path=datapath, affix=affix)
