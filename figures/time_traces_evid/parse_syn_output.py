import numpy as np
from matplotlib import pyplot as plt
import json
import csv

sim_dict = {
        'syn_traces_rec':{
            'path': ['./data/output91.txt', './data/output92.txt', './data/output93.txt'],
            'idx_t': 0,
            'idx_z_hat': 1,
            'idx_elig': 2,
            'idx_grad': 3,
            'indices': [91, 92, 93],
            'pre_indices': [91, 92, 93],
            }
        }

res_dict = {}
# load synaptic time traces
simd = sim_dict['syn_traces_rec']
for ind, path in zip(simd['indices'], simd['path']):
    syn_list = []
    syn_senders = []
    syn_spike_times = []
    with open(path, 'r', newline='') as file:
        file = csv.reader(file, delimiter=',')
        rows = []
        for row in file:
            if not len(row) == 3:
                rows.append(list(map(float, row)))
            else:
                syn_list.append(np.array(rows).T.tolist())
                rows = []
                syn_senders.append(int(row[1]))
                syn_spike_times.append(float(row[2]))

    syn_senders = np.array(syn_senders)
    syn_spike_times = np.array(syn_spike_times)
    syn_list = np.array(syn_list)

    for ind_pre in simd['pre_indices']:
        if not ind_pre == ind:
            indices = np.where(syn_senders == ind_pre)[0]
            res_dict['{}_to_{}'.format(ind_pre, ind)] = syn_list[indices].tolist()

datapath = './data/'
filename = 'syn_traces.json'
with open(datapath + filename, 'w') as f:
    json.dump(res_dict, f)
