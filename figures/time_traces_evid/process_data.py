import numpy as np
import json

sim_dict = {
        'loss_nest_dale': {
            'paths': [
                ],
            },
        'loss_nest': {
            'paths': [
                './data/loss_accuracy_error_302681.npy',
                './data/loss_accuracy_error_302688.npy',
                './data/loss_accuracy_error_302689.npy',
                ],
            'n_iter': 10,
            },
        'loss_tf': {
            'paths': [
                ],
            },
        'weights_dale':{
            'path': './data/nest_wr_data_niter10.json',
            'fname': 'weight_traces_dale.json',
            },
        'weights':{
            'path': './data/nest_wr_data_niter10.json',
            'fname': 'weight_traces.json',
            },
        }

path = './data/'
# load nest results
# loss
simd = sim_dict['loss_nest']
n_files_loss_nest = len(simd['paths'])
nest_loss = np.zeros((n_files_loss_nest, 3, simd['n_iter']))
for i, fname in enumerate(simd['paths']):
    data = np.load(fname)
    nest_loss[i,:,:] = np.array(data)

loss_nest_mean = np.mean(nest_loss, axis=0)
loss_nest_std = np.std(nest_loss, axis=0)

# load nest results with Dale's law
# loss
#n_files_loss_nest = len(sim_dict['loss_nest_dale']['paths'])
#nest_loss = np.zeros((n_files_loss_nest, 2000))
#for i, fname in enumerate(sim_dict['loss_nest_dale']['paths']):
#    with open(fname, 'r') as d:
#        data = json.load(d)
#        nest_loss[i,:] = np.array(data['loss'])
#
#loss_nest_dale_mean = np.mean(nest_loss, axis=0)
#loss_nest_dale_std = np.std(nest_loss, axis=0)

# evolution of weights
for key in ['weights', 'weights_dale']:
    with open(sim_dict[key]['path'], 'r') as d:
        data = json.load(d)
        rec_nrns = np.array(data['rec_nrns'])
        senders_in = np.array(data['senders_in'])
        targets_in = np.array(data['targets_in'])
        times_in = np.array(data['times_in'])
        weights_in = np.array(data['weights_in'])
        senders_rec = np.array(data['senders_rec'])
        targets_rec = np.array(data['targets_rec'])
        times_rec = np.array(data['times_rec'])
        weights_rec = np.array(data['weights_rec'])
        senders_out = np.array(data['senders_out'])
        targets_out = np.array(data['targets_out'])
        times_out = np.array(data['times_out'])
        weights_out = np.array(data['weights_out'])

    # sort weight recorder data
    step_weights = 10
    times_in_lists = []
    weights_in_lists = []
    times_rec_lists = []
    weights_rec_lists = []
    times_out_lists = []
    weights_out_lists = []
    for i in np.array(rec_nrns)[::1]:
        indices_in = np.where(targets_in == i)[0]
        indices_rec = np.where(targets_rec == i)[0]
        indices_out = np.where(senders_out == i)[0]
        if not len(indices_in) == 0:
            times_in_lists.append(times_in[indices_in][::step_weights].tolist())
            weights_in_lists.append(weights_in[indices_in][::step_weights].tolist())
            times_rec_lists.append(times_rec[indices_rec][::step_weights].tolist())
            weights_rec_lists.append(weights_rec[indices_rec][::step_weights].tolist())
            times_out_lists.append(times_out[indices_out][::step_weights].tolist())
            weights_out_lists.append(weights_out[indices_out][::step_weights].tolist())

    # save weights keeping only every n-th data point
    weights_dict = {
            'times_in': times_in_lists,
            'weights_in': weights_in_lists,
            'times_rec': times_rec_lists,
            'weights_rec': weights_rec_lists,
            'times_out': times_out_lists,
            'weights_out': weights_out_lists,
            }
    fname_weights = sim_dict[key]['fname']
    with open(path+fname_weights, 'w') as f:
        json.dump(weights_dict, f)

    print('wrote file {}'.format(fname_weights))

# load tf results
#n_files_loss_tf = len(sim_dict['loss_tf']['paths'])
#tf_loss = np.zeros((n_files_loss_tf, 2000))
#for i, fname in enumerate(sim_dict['loss_tf']['paths']):
#    with open(fname, 'r') as d:
#        data = json.load(d)
#        tf_loss[i,:] = np.array(data['loss'])
#
#loss_tf_mean = np.mean(tf_loss, axis=0)
#loss_tf_std = np.std(tf_loss, axis=0)

# save losses as npy file keeping only every n-th data point
step_loss = 1
losses = np.array([
    loss_nest_mean[:,::step_loss], loss_nest_std[:,::step_loss],
    #loss_nest_dale_mean[::step_loss], loss_nest_dale_std[::step_loss],
    #loss_tf_mean[::step_loss], loss_tf_std[::step_loss],
    ])

fname_loss = 'losses.npy'
np.save(path+fname_loss, losses, allow_pickle=True)
print('wrote file {}'.format(path+fname_loss))

