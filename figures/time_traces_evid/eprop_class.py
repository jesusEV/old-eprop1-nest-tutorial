import numpy as np
import copy
import nest
import pickle


class eprop1(object):
    def __init__(self, params=None):
        default_params = {
            'network_type': 'all_to_all',           # 'all_to_all' or 'brunel'
            'load_from_file': None,                 # if a path to a .pkl file is specified, load synaptic weights
                                                    # from that file
            'seq_len': 1000.0,                      # duration T of one training period
            't_recall': 0.0,                        # duration of recall period
            'dt': 1.0,                              # resolution of the simulation
            'delay': 1.0,                           # delay of all connections in the network
            'n_batch': 1,                           # batch size
            'regression': True,                     # True: regression task, False: recall task 
            'n_in': 100,                            # number of input neurons
            'background_noise': False,              # if True, poisson noise with rate 'rate_background'
                                                    # is fed to the recurrent network
            'rate_background': 0.0,                 # rate of background noise, if not set explicitely, it will be
                                                    # computes so that in case of brunel network it will be in the
                                                    # asynchronous irregular regime
            'n_regular': 50,                        # number of iaf_psc_delta_eprop
            'n_adaptive': 50,                       # number of aif_psc_delta_eprop
            'n_out': 2,                             # number of error_neruon
            'learning_rate': 1.000e-4,              # learning rate for all plastic synapses
            'target_firing_rate': 20.0,             # target firing rate for rate regularization
            'reg': 0.0,                             # weight factor of rate regularization
            'nvp': 2,                               # number of virtual processes of simulation
            'print_time': False,                    # whether or not to print time during simulation
            'rnd_seed': 302687,                     # numpy random seed
            'tau_v': 20.0,                          # used to compute the parameter beta of adapt nrn
            'connect_mm_to_rec': False,             # connect multimeter also to recurrent neurons
            'test_run': False,                      # True: replace eprop_by static syns with same weight
                                                    # this decreases the simulation time considerably and is useful
                                                    # to evaluate the performance of a network with trained weights
            'use_adam': False,                      # use adam optimizer if set to True
            'adam_params': {                        # for a description of the parameters see:
                'beta1': 0.9,                       # arXiv:1412.6980v9
                'beta2': 0.999,
                'epsilon': 1.0e-8,
            },
            'brunel_params': {                      # parameters for the Brunel network (see also network_type)
                'g': 5.0,                           # ratio inhibitory weight/excitatory weight
                'eta': 2.0,                         # external rate relative to threshold rate
                'epsilon': 0.1,                     # connection probability
                'J': 0.1,                           # postsynaptic amplitude in mV
            },
            'rec_neuron_regular_params': {          # parameters of the recurrent neurons without adaptive threshold
                'V_m': 0.0,
                'V_th': 0.6,
                'V_reset': 0.0,
                'E_L': 0.0,
                'tau_m': 20.0,
                't_ref': 5.0,
                'dampening_factor': 0.3,
            },
            'rec_neuron_adaptive_params': {         # parameters of the recurrent neurons with adaptive threshold
                'V_m': 0.0,
                'V_th': 0.6,
                'V_reset': 0.0,
                'E_L': 0.0,
                'tau_m': 20.0,
                'tau_a': 2000.0,
                't_ref': 5.0,
                'dampening_factor': 0.3,
            },
            'out_neuron_params': {                  # parameters of the readout units (aka error/output neurons)
                'V_m': 0.0,
                'E_L': 0.0,
                'tau_m': 20.0,
                'dampening_factor': 1.0,
            },
            'syn_in_params': {                      # parameters of the synapse from input to recurrent network
                'synapse_model': 'eprop_synapse',
                'weight': 0.0,
                'tau_decay': 20.0,                  # also known as tau_out
            },
            'syn_rec_params': {                     # parameters of the synapses of the recurrent network
                'synapse_model': 'eprop_synapse',
                'weight': 0.0,
                'tau_decay': 20.0,                  # also known as tau_out
            },
            'syn_rec_inh_params': {                 # inhibitory synapses of the recurrent network in case of a
                                                    # Brunel network
                'synapse_model': 'eprop_synapse',
                'weight': 0.0,
                'tau_decay': 20.0,                  # also known as tau_out
            },
            'syn_out_params': {                     # parameters of the synapses recurrent to readout
                'synapse_model': 'eprop_synapse',
                'weight': 0.0,
                'tau_decay': 20.0,
            },
            'synapse_broadcast': {                  # parameters of the synapse for feedback connections
                'synapse_model': 'learning_signal_connection_delayed',
                'weight': 0.0,
            },
            'synapse_target': {                     # parameters of the synapses from target generators to readout
                'synapse_model': 'rate_connection_delayed',
                'weight': 1.0,
                'receptor_type': 2,
            },
            'static_synapse': {                     # synapse used to connect multimeter and spike generators
                'synapse_model': 'static_synapse',
                'weight': 0.0,
            },
            'nrns_dvcs': {},
        }
        self.params = copy.deepcopy(default_params)
        self.set_params(params)
        # decide whether we need to draw new connectivity matrices
        self.weights_from_file = False
        if self.params['load_from_file'] is not None:
            self.weights_from_file = True

    def _update_params(self, params_new, params_old):
        updated_params = {k: params_new.get(k, v) if not isinstance(v, dict) else
                          self._update_params(params_new.get(k, v), v) for k, v in params_old.items()}
        return updated_params

    def set_params(self, params):
        '''
        update parameters and compute derived quantities.
        '''
        if params is not None:
            self.params = self._update_params(copy.deepcopy(params), copy.deepcopy(self.params))

        # set random seed
        np.random.seed(self.params['rnd_seed'])
        # set update interval to sequence length
        self.params['update_interval'] = self.params['seq_len']
        # compute the time until recall period
        if 't_recall' not in params:
            self.params['t_to_recall'] = 0.0
            self.params['t_recall'] = self.params['seq_len']
        else:
            self.params['t_to_recall'] = self.params['seq_len'] - self.params['t_recall']

        # adam optimizer: convert from bool to double
        if self.params['use_adam']:
            self.params['adam_params']['use_adam'] = 1.0
        else:
            self.params['adam_params']['use_adam'] = 0.0

        if 'n_rec' not in params:
            self.params['n_rec'] = self.params['n_regular'] + self.params['n_adaptive']
        else:
            self.params['n_rec'] = params['n_rec']
            self.params['n_regular'] = self.params['n_rec'] // 2
            self.params['n_adaptive'] = self.params['n_rec'] - self.params['n_regular']

        # set neuron params
        self.params['rec_neuron_regular_params']['update_interval'] = self.params['seq_len']
        rho = np.exp(-1 / self.params['rec_neuron_adaptive_params']['tau_a'])  # decay factors for adaptive threshold
        beta_a = 1.7 * (1.0 - rho) / (1.0 - np.exp(-1.0 / self.params['tau_v']))  # this is a heuristic value
        self.params['rec_neuron_adaptive_params']['update_interval'] = self.params['seq_len']
        self.params['rec_neuron_adaptive_params']['beta'] = beta_a
        self.params['out_neuron_params']['update_interval'] = self.params['seq_len']
        self.params['out_neuron_params']['start'] = self.params['t_to_recall']
        self.params['out_neuron_params']['regression'] = self.params['regression']
        # set synapse in params
        self.params['syn_in_params']['learning_rate'] = self.params['learning_rate']
        self.params['syn_in_params']['delay'] = self.params['delay']
        self.params['syn_in_params']['update_interval'] = self.params['update_interval']
        self.params['syn_in_params']['target_firing_rate'] = self.params['target_firing_rate']
        self.params['syn_in_params']['rate_reg'] = self.params['reg']
        self.params['syn_in_params']['batch_size'] = float(self.params['n_batch'])
        self.params['syn_in_params']['use_adam'] = self.params['adam_params']['use_adam']
        self.params['syn_in_params']['beta1_adam'] = self.params['adam_params']['beta1']
        self.params['syn_in_params']['beta2_adam'] = self.params['adam_params']['beta2']
        self.params['syn_in_params']['epsilon_adam'] = self.params['adam_params']['epsilon']
        self.params['syn_in_params']['recall_duration'] = self.params['t_recall']
        # set synapse rec params
        self.params['syn_rec_params']['learning_rate'] = self.params['learning_rate']
        self.params['syn_rec_params']['delay'] = self.params['delay']
        self.params['syn_rec_params']['update_interval'] = self.params['update_interval']
        self.params['syn_rec_params']['target_firing_rate'] = self.params['target_firing_rate']
        self.params['syn_rec_params']['rate_reg'] = self.params['reg']
        self.params['syn_rec_params']['batch_size'] = float(self.params['n_batch'])
        self.params['syn_rec_params']['use_adam'] = self.params['adam_params']['use_adam']
        self.params['syn_rec_params']['beta1_adam'] = self.params['adam_params']['beta1']
        self.params['syn_rec_params']['beta2_adam'] = self.params['adam_params']['beta2']
        self.params['syn_rec_params']['epsilon_adam'] = self.params['adam_params']['epsilon']
        self.params['syn_rec_params']['recall_duration'] = self.params['t_recall']
        # set synapse out params
        self.params['syn_out_params']['learning_rate'] = self.params['learning_rate']
        self.params['syn_out_params']['delay'] = self.params['delay']
        self.params['syn_out_params']['update_interval'] = self.params['update_interval']
        self.params['syn_out_params']['batch_size'] = float(self.params['n_batch'])
        self.params['syn_out_params']['use_adam'] = self.params['adam_params']['use_adam']
        self.params['syn_out_params']['beta1_adam'] = self.params['adam_params']['beta1']
        self.params['syn_out_params']['beta2_adam'] = self.params['adam_params']['beta2']
        self.params['syn_out_params']['epsilon_adam'] = self.params['adam_params']['epsilon']
        self.params['syn_out_params']['recall_duration'] = self.params['t_recall']
        # set broadcast delay
        self.params['synapse_broadcast']['delay'] = self.params['delay']
        # set delay of target signal input
        self.params['synapse_target']['delay'] = self.params['delay']
        # set delay and weight of static synapse
        self.params['static_synapse']['delay'] = self.params['delay']
        self.params['static_synapse']['weight'] = 1.0
        #####################################################
        # compute derived parameters for brunel network #####
        #####################################################
        self.params['brunel_params']['n_exc'] = int(4.0 * self.params['n_rec'] / 5.0)
        self.params['brunel_params']['n_inh'] = self.params['n_rec'] - self.params['brunel_params']['n_exc']
        # number of excitatory synapses per neuron
        self.params['brunel_params']['CE'] = int(self.params['brunel_params']['epsilon'] *
                                                 self.params['brunel_params']['n_exc'])
        # number of inhibitory synapses per neuron
        self.params['brunel_params']['CI'] = int(self.params['brunel_params']['epsilon'] *
                                                 self.params['brunel_params']['n_inh'])
        # amplitude of excitatory postsynaptic potential
        self.params['brunel_params']['J_ex'] = self.params['brunel_params']['J']
        # amplitude of inhibitory postsynaptic potential
        self.params['brunel_params']['J_in'] = -self.params['brunel_params']['g'] * self.params['brunel_params']['J']
        # set inhibitory synapse rec params
        self.params['syn_rec_inh_params']['learning_rate'] = self.params['learning_rate']
        self.params['syn_rec_inh_params']['delay'] = self.params['delay']
        self.params['syn_rec_inh_params']['update_interval'] = self.params['update_interval']
        self.params['syn_rec_inh_params']['target_firing_rate'] = self.params['target_firing_rate']
        self.params['syn_rec_inh_params']['rate_reg'] = self.params['reg']
        # rate of external drive
        if self.params['network_type'] == 'brunel' and 'rate_background' not in params.keys():
            self.params['rate_background'] = (1000.0 *
                                              self.params['brunel_params']['eta'] *
                                              self.params['rec_neuron_regular_params']['V_th'] /
                                              (self.params['brunel_params']['g'] *
                                                  self.params['rec_neuron_regular_params']['tau_m']))
            print('set rate of external drive to {}'.format(self.params['rate_background']))

    def _draw_and_set_weights(self):
        '''
        draw new random weight matrices
        '''
        print('Drawing new weights!')
        n_in = self.params['n_in']
        n_rec = self.params['n_rec']
        n_out = self.params['n_out']
        if self.params['network_type'] == 'all_to_all':
            # in case of the all-to-all network, all synapse weights are drawn from normal distributions
            self.params['syn_in_params']['weight'] = np.random.randn(n_rec, n_in) / np.sqrt(n_in)
            rec_weights = np.random.randn(n_rec, n_rec) / np.sqrt(n_rec)
            np.fill_diagonal(rec_weights, 0.0)      # set diagonal to zero (no autapses)
            self.params['syn_rec_params']['weight'] = rec_weights
            self.params['syn_out_params']['weight'] = np.random.randn(n_out, n_rec) / np.sqrt(n_rec)
            self.params['synapse_broadcast']['weight'] = np.random.randn(n_rec, n_out) / np.sqrt(n_rec)
        elif self.params['network_type'] == 'brunel':
            # in case of the brunel network, the weight of the recurrent connections is the same for all
            # excitatory and inhibitory synapses respectively
            self.params['syn_in_params']['weight'] = np.random.randn(n_rec, n_in) / np.sqrt(n_in)
            self.params['syn_rec_params']['weight'] = self.params['brunel_params']['J_ex']
            self.params['syn_rec_inh_params']['weight'] = self.params['brunel_params']['J_in']
            self.params['syn_out_params']['weight'] = np.random.randn(n_out, n_rec) / np.sqrt(n_rec)
            self.params['synapse_broadcast']['weight'] = np.random.randn(n_rec, n_out) / np.sqrt(n_rec)
        else:
            print('network_type has to be "all_to_all" or "brunel" but got {}'.format(self.params['network_type']))

        # set Wmin and Wmax
        self._create_Wmin_Wmax()

    def _read_weights_from_file(self):
        '''
        read weights from .pkl file specified in params['load_from_file']
        '''
        print('read weights from {}'.format(self.params['load_from_file']))
        with open(self.params['load_from_file'], 'rb') as f:
            network = pickle.load(f)

        # store parameters of incoming connections
        self.params['syn_in_params']['weight'] = network['in_syns'].weight.values
        self.params['syn_in_params']['delay'] = network['in_syns'].delay.values
        self.in_sources = network['in_syns'].source.values
        self.in_targets = network['in_syns'].target.values
        if not self.params['test_run']:
            # if this is training, eprop synapses are used
            self.params['syn_in_params']['Wmin'] = network['in_syns'].Wmin.values
            self.params['syn_in_params']['Wmax'] = network['in_syns'].Wmax.values
            for k, v in self.params['syn_in_params'].items():
                if isinstance(v, float):
                    # if synapse property is a single float, we have to expand it to an array
                    self.params['syn_in_params'][k] = v*np.ones_like(self.params['syn_in_params']['weight'])

        else:
            # if this is a test run, static synapses are used
            self.params['syn_in_params']['synapse_model'] = 'static_synapse'

        # store parameters of recurrent connections
        self.params['syn_rec_params']['weight'] = network['rec_syns'].weight.values
        self.params['syn_rec_params']['delay'] = network['rec_syns'].delay.values
        self.rec_sources = network['rec_syns'].source.values
        self.rec_targets = network['rec_syns'].target.values
        if not self.params['test_run']:
            # if this is training, eprop synapses are used
            self.params['syn_rec_params']['Wmin'] = network['rec_syns'].Wmin.values
            self.params['syn_rec_params']['Wmax'] = network['rec_syns'].Wmax.values
            for k, v in self.params['syn_rec_params'].items():
                if isinstance(v, float):
                    # if synapse property is a single float, we have to expand it to an array
                    self.params['syn_rec_params'][k] = v*np.ones_like(self.params['syn_rec_params']['weight'])

        else:
            # if this is a test run, static synapses are used
            self.params['syn_rec_params']['synapse_model'] = 'static_synapse'

        # store parameters of outgoing connections
        self.params['syn_out_params']['weight'] = network['out_syns'].weight.values
        self.params['syn_out_params']['delay'] = network['out_syns'].delay.values
        self.out_sources = network['out_syns'].source.values
        self.out_targets = network['out_syns'].target.values
        if not self.params['test_run']:
            # if this is training, eprop synapses are used
            self.params['syn_out_params']['Wmin'] = network['out_syns'].Wmin.values
            self.params['syn_out_params']['Wmax'] = network['out_syns'].Wmax.values
            for k, v in self.params['syn_in_params'].items():
                if isinstance(v, float):
                    # if synapse property is a single float, we have to expand it to an array
                    self.params['syn_out_params'][k] = v*np.ones_like(self.params['syn_out_params']['weight'])

        else:
            # if this is a test run, static synapses are used
            self.params['syn_out_params']['synapse_model'] = 'static_synapse'

        # store parameters of feedback connections
        if not self.params['test_run']:
            # the feedback is used only in case of training
            self.params['synapse_broadcast']['weight'] = network['feedback_syns'].weight.values
            self.params['synapse_broadcast']['delay'] = network['feedback_syns'].delay.values
            self.bcast_sources = network['feedback_syns'].source.values
            self.bcast_targets = network['feedback_syns'].target.values
            for k, v in self.params['syn_in_params'].items():
                if isinstance(v, float):
                    # if synapse property is a single float, we have to expand it to an array
                    self.params['synapse_broadcast'][k] = v*np.ones_like(self.params['synapse_broadcast']['weight'])

    def generate_network(self, wr=False):
        '''
        create neurons, devices, and connections
        '''
        # reset kernel
        nest.ResetKernel()
        nest.SetKernelStatus({
            'resolution': self.params['dt'],
            'total_num_virtual_procs': self.params['nvp'],
            'print_time': self.params['print_time'],
        })
        nest.SetKernelStatus({'rng_seed': self.params['rnd_seed']})
        if not self.weights_from_file:
            self._draw_and_set_weights()
        else:
            self._read_weights_from_file()

        if self.params['network_type'] == 'all_to_all':
            self._generate_all_to_all_network(wr=wr)
        elif self.params['network_type'] == 'brunel':
            self._generate_brunel_network()
        else:
            print('network_type has to be "all_to_all" or "brunel" but got {}'.format(self.params['network_type']))

        if not self.weights_from_file:
            # retrive senders, targets, and weights of input, recurrent, and output weights
            # so that they can be returned by return_init_weights()
            # input connection
            conns = self._extract_senders_targets_weights(
                    self.params['nrns_dvcs']['in_nrns'], self.params['nrns_dvcs']['rec_nrns'])
            self.in_sources = conns.source.values
            self.in_targets = conns.target.values
            self.params['syn_in_params']['weight'] = conns.weight.values
            # recurrent connections
            conns = self._extract_senders_targets_weights(
                    self.params['nrns_dvcs']['rec_nrns'], self.params['nrns_dvcs']['rec_nrns'])
            self.rec_sources = conns.source.values
            self.rec_targets = conns.target.values
            self.params['syn_rec_params']['weight'] = conns.weight.values
            # output connections
            conns = self._extract_senders_targets_weights(
                    self.params['nrns_dvcs']['rec_nrns'], self.params['nrns_dvcs']['out_nrns'])
            self.out_sources = conns.source.values
            self.out_targets = conns.target.values
            self.params['syn_out_params']['weight'] = conns.weight.values
            # feedback connections (only present if this is training, not testing)
            if not self.params['test_run']:
                conns = self._extract_senders_targets_weights(
                        self.params['nrns_dvcs']['out_nrns'], self.params['nrns_dvcs']['rec_nrns'])
                self.bcast_sources = conns.source.values
                self.bcast_targets = conns.target.values
                self.params['synapse_broadcast']['weight'] = conns.weight.values

        # create and connect devices
        self._create_connect_dvcs()

    def _generate_all_to_all_network(self, wr):
        '''
        generate a network with all to all connections between the recurrent neurons
        '''
        # create neurons
        n_regular = self.params['n_regular']
        n_adaptive = self.params['n_adaptive']
        in_nrns = nest.Create('parrot_neuron', self.params['n_in'])
        if n_regular == 0:
            rec_nrns_adaptive = nest.Create('aif_psc_delta_eprop', n_adaptive,
                                            params=self.params['rec_neuron_adaptive_params'])
            rec_nrns = rec_nrns_adaptive
            rec_nrns_regular = None
        elif n_adaptive == 0:
            rec_nrns_regular = nest.Create('iaf_psc_delta_eprop', n_regular,
                                           params=self.params['rec_neuron_regular_params'])
            rec_nrns = rec_nrns_regular
            rec_nrns_adaptive = None
        else:
            rec_nrns_regular = nest.Create('iaf_psc_delta_eprop', n_regular,
                                           params=self.params['rec_neuron_regular_params'])
            rec_nrns_adaptive = nest.Create('aif_psc_delta_eprop', n_adaptive,
                                            params=self.params['rec_neuron_adaptive_params'])
            rec_nrns = rec_nrns_regular + rec_nrns_adaptive

        out_nrns = nest.Create('error_neuron', self.params['n_out'], params=self.params['out_neuron_params'])
        self.params['nrns_dvcs']['in_nrns'] = in_nrns
        self.params['nrns_dvcs']['rec_nrns'] = rec_nrns
        self.params['nrns_dvcs']['out_nrns'] = out_nrns
        # create connections
        if not self.weights_from_file:
            conn_spec = {'rule': 'all_to_all', 'allow_autapses': False}
            if not wr:
                nest.Connect(in_nrns, rec_nrns, syn_spec=self.params['syn_in_params'], conn_spec=conn_spec)
                nest.Connect(rec_nrns, rec_nrns, syn_spec=self.params['syn_rec_params'], conn_spec=conn_spec)
                nest.Connect(rec_nrns, out_nrns, syn_spec=self.params['syn_out_params'], conn_spec=conn_spec)
            else:
                # create weight recorders
                wr_in = nest.Create('weight_recorder')
                wr_rec = nest.Create('weight_recorder')
                wr_out = nest.Create('weight_recorder')
                nest.CopyModel('eprop_synapse','eprop_synapse_wr_in', {"weight_recorder": wr_in})
                nest.CopyModel('eprop_synapse','eprop_synapse_wr_rec', {"weight_recorder": wr_rec})
                nest.CopyModel('eprop_synapse','eprop_synapse_wr_out', {"weight_recorder": wr_out})
                self.params['nrns_dvcs']['wr_in'] = wr_in
                self.params['nrns_dvcs']['wr_rec'] = wr_rec
                self.params['nrns_dvcs']['wr_out'] = wr_out
                # split and connect input weights
                syn_in_params = copy.deepcopy(self.params['syn_in_params'])
                in_weights = syn_in_params['weight']
                Wmin_in = syn_in_params['Wmin']
                Wmax_in = syn_in_params['Wmax']
                in_weights_cut = in_weights[:,1:]
                Wmin_in_cut = Wmin_in[:,1:]
                Wmax_in_cut = Wmax_in[:,1:]
                syn_in_params['weight'] = in_weights_cut
                syn_in_params['Wmin'] = Wmin_in_cut
                syn_in_params['Wmax'] = Wmax_in_cut
                nest.Connect(in_nrns[1:], rec_nrns, syn_spec=syn_in_params, conn_spec=conn_spec)
                # weight recorder
                in_weights_wr = in_weights[:,1:2]
                Wmin_in_wr = Wmin_in[:,1:2]
                Wmax_in_wr = Wmax_in[:,1:2]
                syn_in_params['weight'] = in_weights_wr
                syn_in_params['Wmin'] = Wmin_in_wr
                syn_in_params['Wmax'] = Wmax_in_wr
                syn_in_params['synapse_model'] = 'eprop_synapse_wr_in'
                nest.Connect(in_nrns[1], rec_nrns, syn_spec=syn_in_params, conn_spec=conn_spec)
                # split and connect recurrent weights
                syn_rec_params = copy.deepcopy(self.params['syn_rec_params'])
                rec_weights = syn_rec_params['weight']
                Wmin_rec = syn_rec_params['Wmin']
                Wmax_rec = syn_rec_params['Wmax']
                rec_weights_cut = rec_weights[:,:-1]
                Wmin_rec_cut = Wmin_rec[:,:-1]
                Wmax_rec_cut = Wmax_rec[:,:-1]
                syn_rec_params['weight'] = rec_weights_cut
                syn_rec_params['Wmin'] = Wmin_rec_cut
                syn_rec_params['Wmax'] = Wmax_rec_cut
                nest.Connect(rec_nrns[:-1], rec_nrns, syn_spec=syn_rec_params, conn_spec=conn_spec)
                # weight recorder
                rec_shape = np.shape(rec_weights)
                rec_weights_wr = np.reshape(rec_weights[:,-1], (rec_shape[0], 1))
                Wmin_rec_wr = np.reshape(Wmin_rec[:,-1], (rec_shape[0], 1))
                Wmax_rec_wr = np.reshape(Wmax_rec[:,-1], (rec_shape[0], 1))
                syn_rec_params['weight'] = rec_weights_wr
                syn_rec_params['Wmin'] = Wmin_rec_wr
                syn_rec_params['Wmax'] = Wmax_rec_wr
                syn_rec_params['synapse_model'] = 'eprop_synapse_wr_rec'
                nest.Connect(rec_nrns[-1], rec_nrns, syn_spec=syn_rec_params, conn_spec=conn_spec)
                # split and connect readout weights
                syn_out_params = copy.deepcopy(self.params['syn_out_params'])
                out_weights = syn_out_params['weight']
                Wmin_out = syn_out_params['Wmin']
                Wmax_out = syn_out_params['Wmax']
                out_weights_cut = out_weights[1:,:]
                Wmin_out_cut = Wmin_out[1:,:]
                Wmax_out_cut = Wmax_out[1:,:]
                syn_out_params['weight'] = out_weights_cut
                syn_out_params['Wmin'] = Wmin_out_cut
                syn_out_params['Wmax'] = Wmax_out_cut
                nest.Connect(rec_nrns, out_nrns[0], syn_spec=syn_out_params, conn_spec=conn_spec)
                # weight recorder
                out_shape = np.shape(out_weights)
                out_weights_wr = out_weights[:1,:]
                Wmin_out_wr = Wmin_out[:1,:]
                Wmax_out_wr = Wmax_out[:1,:]
                syn_out_params['weight'] = out_weights_wr
                syn_out_params['Wmin'] = Wmin_out_wr
                syn_out_params['Wmax'] = Wmax_out_wr
                syn_out_params['synapse_model'] = 'eprop_synapse_wr_out'
                nest.Connect(rec_nrns, out_nrns[1], syn_spec=syn_out_params, conn_spec=conn_spec)

            nest.Connect(out_nrns, rec_nrns, syn_spec=self.params['synapse_broadcast'], conn_spec=conn_spec)
        else:
            conn_spec = {'rule': 'one_to_one'}
            # input -> recurrent neurons
            nest.Connect(self.in_sources, self.in_targets, conn_spec=conn_spec,
                    syn_spec=self.params['syn_in_params'])
            # recurrent neurons -> recurrent neurons
            nest.Connect(self.rec_sources, self.rec_targets, conn_spec=conn_spec,
                    syn_spec=self.params['syn_rec_params'])
            # recurrent neurons -> out neurons
            nest.Connect(self.out_sources, self.out_targets, conn_spec=conn_spec,
                    syn_spec=self.params['syn_out_params'])
            if not self.params['test_run']:
                # recurrent neurons -> out neurons (feedback connections)
                nest.Connect(self.bcast_sources, self.bcast_targets, conn_spec=conn_spec,
                        syn_spec=self.params['synapse_broadcast'])

        # out neurons <-> out neurons in case it is a classification task
        if (not self.params['regression']) and (not self.params['test_run']):
            # receptor_type 1 denotes the port for the readout signal which is exchanged between the
            # readout units
            synapse_out_out = {'synapse_model': 'rate_connection_delayed', 'weight': 1.0,
                    'delay': self.params['delay'], 'receptor_type': 1}
            nest.Connect(out_nrns, out_nrns, syn_spec=synapse_out_out, conn_spec=conn_spec)

    def _generate_brunel_network(self):
        '''
        generate a random balanced network (brunel network)
        '''
        # create neurons
        n_regular = self.params['n_regular']
        n_adaptive = self.params['n_adaptive']
        n_exc = self.params['brunel_params']['n_exc']
        in_nrns = nest.Create('parrot_neuron', self.params['n_in'])
        if n_regular == 0:
            rec_nrns_adaptive = nest.Create('aif_psc_delta_eprop', n_adaptive,
                                            params=self.params['rec_neuron_adaptive_params'])
            exc_pop = rec_nrns_adaptive[:n_exc]
            inh_pop = rec_nrns_adaptive[n_exc:]
        elif n_adaptive == 0:
            rec_nrns_regular = nest.Create('iaf_psc_delta_eprop', n_regular,
                                           params=self.params['rec_neuron_regular_params'])
            exc_pop = rec_nrns_regular[:n_exc]
            inh_pop = rec_nrns_regular[n_exc:]
        else:
            rec_nrns_regular = nest.Create('iaf_psc_delta_eprop', n_regular,
                                           params=self.params['rec_neuron_regular_params'])
            rec_nrns_adaptive = nest.Create('aif_psc_delta_eprop', n_adaptive,
                                            params=self.params['rec_neuron_adaptive_params'])
            exc_pop = rec_nrns_regular[:n_exc // 2] + rec_nrns_adaptive[:n_exc // 2]
            inh_pop = rec_nrns_regular[n_exc // 2:] + rec_nrns_adaptive[n_exc // 2:]

        rec_nrns = exc_pop + inh_pop
        out_nrns = nest.Create('error_neuron', self.params['n_out'], params=self.params['out_neuron_params'])
        self.params['nrns_dvcs']['rec_nrns'] = rec_nrns
        self.params['nrns_dvcs']['in_nrns'] = in_nrns
        self.params['nrns_dvcs']['rec_nrns_exc'] = exc_pop
        self.params['nrns_dvcs']['rec_nrns_inh'] = inh_pop
        self.params['nrns_dvcs']['out_nrns'] = out_nrns
        # create connections
        if not self.weights_from_file:
            conn_spec = {'rule': 'all_to_all', 'allow_autapses': False}
            conn_spec_exc = {'rule': 'fixed_indegree', 'indegree': self.params['brunel_params']['CE']}
            conn_spec_inh = {'rule': 'fixed_indegree', 'indegree': self.params['brunel_params']['CI']}
            # input -> recurrent neurons
            nest.Connect(in_nrns, rec_nrns, syn_spec=self.params['syn_in_params'], conn_spec=conn_spec)
            # recurrent neurons -> recurrent neurons
            nest.Connect(exc_pop, rec_nrns, conn_spec=conn_spec_exc, syn_spec=self.params['syn_rec_params'])
            nest.Connect(inh_pop, rec_nrns, conn_spec=conn_spec_inh, syn_spec=self.params['syn_rec_inh_params'])
            # recurrent neurons -> out neurons
            nest.Connect(rec_nrns, out_nrns, syn_spec=self.params['syn_out_params'], conn_spec=conn_spec)
            # recurrent neurons -> out neurons (feedback connections)
            nest.Connect(out_nrns, rec_nrns, syn_spec=self.params['synapse_broadcast'], conn_spec=conn_spec)
        else:
            conn_spec = {'rule': 'one_to_one'}
            # input -> recurrent neurons
            nest.Connect(self.in_sources, self.in_targets, conn_spec=conn_spec,
                    syn_spec=self.params['syn_in_params'])
            # recurrent neurons -> recurrent neurons
            nest.Connect(self.rec_sources, self.rec_targets, conn_spec=conn_spec,
                    syn_spec=self.params['syn_rec_params'])
            # recurrent neurons -> out neurons
            nest.Connect(self.out_sources, self.out_targets, conn_spec=conn_spec,
                    syn_spec=self.params['syn_out_params'])
            if not self.params['test_run']:
                # recurrent neurons -> out neurons (feedback connections)
                nest.Connect(self.bcast_sources, self.bcast_targets, conn_spec=conn_spec,
                        syn_spec=self.params['synapse_broadcast'])

        # out neurons <-> out neurons in case it is a classification task
        if (not self.params['regression']) and (not self.params['test_run']):
            # receptor_type 1 denotes the port for the readout signal which is exchanged between the
            # readout units
            synapse_out_out = {'synapse_model': 'rate_connection_delayed', 'weight': 1.0,
                    'delay': self.params['delay'], 'receptor_type': 1}
            nest.Connect(out_nrns, out_nrns, syn_spec=synapse_out_out, conn_spec=conn_spec)

    def _create_Wmin_Wmax(self):
        '''
        generate the values for minimum and maximum weights so that their sign
        matches that of the weights
        '''
        # set Wmin and Wmax to appropriate values depending on the sign of the weight
        # if the weight is positive, Wmin = 0.0 and Wmax = 10.0
        # if the weight is negative, Wmin = -10.0 and Wmax = 0.0
        # input weights
        Wmin = np.zeros_like(self.params['syn_in_params']['weight'])
        Wmax = 10.0*np.ones_like(Wmin)
        ind_negative = np.where(self.params['syn_in_params']['weight'] < 0.0)
        Wmin[ind_negative] = -10.0
        Wmax[ind_negative] = 0.0
        self.params['syn_in_params']['Wmin'] = Wmin
        self.params['syn_in_params']['Wmax'] = Wmax
        # recurrent weights
        if isinstance(self.params['syn_rec_params']['weight'], np.ndarray):
            Wmin = np.zeros_like(self.params['syn_rec_params']['weight'])
            Wmax = 10.0*np.ones_like(Wmin)
            ind_negative = np.where(self.params['syn_rec_params']['weight'] < 0.0)
            Wmin[ind_negative] = -10.0
            Wmax[ind_negative] = 0.0
            self.params['syn_rec_params']['Wmin'] = Wmin
            self.params['syn_rec_params']['Wmax'] = Wmax
        else:
            # in case of a brunel network
            self.params['syn_rec_params']['Wmin'] = 0.0
            self.params['syn_rec_params']['Wmax'] = 10.0
            self.params['syn_rec_inh_params']['Wmin'] = -10.0
            self.params['syn_rec_inh_params']['Wmax'] = 0.0

        # outgoing weights
        Wmin = np.zeros_like(self.params['syn_out_params']['weight'])
        Wmax = 10.0*np.ones_like(Wmin)
        ind_negative = np.where(self.params['syn_out_params']['weight'] < 0.0)
        Wmin[ind_negative] = -10.0
        Wmax[ind_negative] = 0.0
        self.params['syn_out_params']['Wmin'] = Wmin
        self.params['syn_out_params']['Wmax'] = Wmax

    def _create_connect_dvcs(self):
        '''
        function to create and connect devices.
        poisson generators for background noise
        spike generators that provide input to the input neurons
        spike generators connected directly to the recurrent neurons
        rage generators to provide the target signal
        multimeter connected to the error neurons (and optionally also to the recurrent neurons)
        '''
        # create and connect poisson generators for background noise
        if self.params['background_noise']:
            poisson_generators_background = nest.Create(
                'poisson_generator', 1, params={'rate': self.params['rate_background']})
            nest.Connect(poisson_generators_background, self.params['nrns_dvcs']['rec_nrns'],
                    syn_spec={'weight': self.params['brunel_params']['J_ex'], 'delay': self.params['delay']})
        else:
            poisson_generators_background = None

        # create and connect rate generators
        rate_generators = nest.Create('step_rate_generator', self.params['n_out'])
        nest.Connect(rate_generators, self.params['nrns_dvcs']['out_nrns'],
                     syn_spec=self.params['synapse_target'], conn_spec={'rule': 'one_to_one'})
        # create and connect multimeter
        mm_out = nest.Create("multimeter", params={
            "interval": self.params['dt'], "record_from": ['V_m', 'learning_signal', 'target_rate']})
        if self.params['connect_mm_to_rec']:
            nest.Connect(mm_out, self.params['nrns_dvcs']['out_nrns'] + self.params['nrns_dvcs']['rec_nrns'],
                         syn_spec=self.params['static_synapse'])
        else:
            nest.Connect(mm_out, self.params['nrns_dvcs']['out_nrns'], syn_spec=self.params['static_synapse'])

        # crate and connect spike generators
        spike_generators = nest.Create('spike_generator', self.params['n_in'])
        nest.Connect(spike_generators, self.params['nrns_dvcs']['in_nrns'],
                     syn_spec=self.params['static_synapse'], conn_spec={'rule': 'one_to_one'})
        # store all devices in params dictionary
        self.params['nrns_dvcs']['spike_generators'] = spike_generators
        self.params['nrns_dvcs']['poisson_generators_background'] = poisson_generators_background
        self.params['nrns_dvcs']['rate_generators'] = rate_generators
        self.params['nrns_dvcs']['mm_out'] = mm_out

    def return_weight_recorders(self):
        if 'wr_in' in self.params['nrns_dvcs'].keys():
            return (self.params['nrns_dvcs']['wr_in'], self.params['nrns_dvcs']['wr_rec'],
                    self.params['nrns_dvcs']['wr_out'])
        else:
            return (None, None, None)

    def return_nrns_dvcs(self):
        '''
        returns a list of lists of neuron and device ids.
        all-to-all network:
        in neurons (parrot neurons),
        recurrent neurons (regular population followed by adaptive population),
        error neurons,
        spike generators,
        poisson generators to produce background noise,
        rate generators to provide terget signals,
        multimeter

        brunel network:
        in neurons (parrot neurons),
        recurrent neurons excitatory population (regular population followed by adaptive population),
        recurrent neurons inhibitory population (regular population followed by adaptive population),
        error neurons,
        spike generators,
        poisson generators to produce background noise,
        rate generators to provide terget signals,
        multimeter
        '''
        if self.params['network_type'] == 'all_to_all':
            return [
                self.params['nrns_dvcs']['in_nrns'],
                self.params['nrns_dvcs']['rec_nrns'],
                self.params['nrns_dvcs']['out_nrns'],
                self.params['nrns_dvcs']['spike_generators'],
                self.params['nrns_dvcs']['poisson_generators_background'],
                self.params['nrns_dvcs']['rate_generators'],
                self.params['nrns_dvcs']['mm_out']]
        elif self.params['network_type'] == 'brunel':
            return [self.params['nrns_dvcs']['in_nrns'], self.params['nrns_dvcs']['rec_nrns_exc'],
                    self.params['nrns_dvcs']['rec_nrns_inh'], self.params['nrns_dvcs']['out_nrns'],
                    self.params['nrns_dvcs']['spike_generators'],
                    self.params['nrns_dvcs']['poisson_generators_background'],
                    self.params['nrns_dvcs']['rate_generators'], self.params['nrns_dvcs']['mm_out']]
        else:
            print('network_type has to be "all_to_all" or "brunel" but got {}'.format(self.params['network_type']))

    def _extract_senders_targets_weights(self, pop_pre, pop_post):
        '''
        returns an connectivity object that contains the sender ids, target ids, the weights,
        the delays, and for the eprop connections also Wmin and Wmax.
        One can access these values, i.e. the weights, as object.weight.values
        '''
        conns_pre_post = nest.GetConnections(pop_pre, pop_post)
        #keys = conns_pre_post.get().keys()
        if conns_pre_post.get('synapse_model')[0] == 'eprop_synapse':
            # eprop synapses
            return conns_pre_post.get(('source', 'target', 'weight', 'delay',
                'Wmin', 'Wmax'), output='pandas')
        elif conns_pre_post.get('synapse_model')[0] == 'learning_signal_connection_delayed':
            # feedback connections
            return conns_pre_post.get(('source', 'target', 'weight', 'delay'), output='pandas')
        elif conns_pre_post.get('synapse_model')[0] == 'static_synapse':
            # static synapses in case of test run
            return conns_pre_post.get(('source', 'target', 'weight', 'delay'), output='pandas')

    def _extract_weight_matrix(self, pop_pre, pop_post):
        '''
        returns weight matrix W_ij of connection between pop_pre and pop_post,
        where the first index i corresponds to the target and j to the source.
        The shape if the returnd matrix is (len(pop_post), len(pop_pre)).
        '''
        weight_matrix = np.zeros((len(pop_post), len(pop_pre)))
        s_t_w = self._extract_senders_targets_weights(pop_pre, pop_post)
        pop_pre_list = pop_pre.tolist()
        pop_post_list = pop_post.tolist()
        for (s, t, w) in zip(s_t_w.source.values, s_t_w.target.values, s_t_w.weight.values):
            j = np.where(np.array(pop_pre_list) == s)[0][0]
            i = np.where(np.array(pop_post_list) == t)[0][0]
            weight_matrix.itemset(i, j, w)

        return weight_matrix

    def _weight_array_to_matrix(self, sources, targets, weight_array):
        '''
        returns weight matrix W_ij of connection between sources and targets,
        where the first index i corresponds to the target and j to the source.
        '''
        pop_pre = np.unique(sources)
        pop_post = np.unique(targets)
        weight_matrix = np.zeros((len(pop_post), len(pop_pre)))
        for (s, t, w) in zip(sources, targets, weight_array):
            j = np.where(np.array(pop_pre) == s)[0][0]
            i = np.where(np.array(pop_post) == t)[0][0]
            weight_matrix.itemset(i, j, w)

        return weight_matrix

    def return_init_weights(self):
        '''
        returns an array of matrices containing the initial synaptic weights in the following order:
            in->rec, rec->rec, rec->out, out->rec (broadcast), poisson_generators->rec
        This function should be called after generation of the network.
        '''
        #if not self.weights_from_file:
        #    return [self.params['syn_in_params']['weight'], self.params['syn_rec_params']['weight'],
        #            self.params['syn_out_params']['weight'], self.params['synapse_broadcast']['weight']]
        #else:
        # if the weights are loaded from a file, they are stored as an array so that they first
        # have to be reshaped into matrices
        W_matrix_in = self._weight_array_to_matrix(
                self.in_sources, self.in_targets, self.params['syn_in_params']['weight'])
        W_matrix_rec = self._weight_array_to_matrix(
                self.rec_sources, self.rec_targets, self.params['syn_rec_params']['weight'])
        W_matrix_out = self._weight_array_to_matrix(
                self.out_sources, self.out_targets, self.params['syn_out_params']['weight'])
        if not self.params['test_run']:
            W_matrix_bcast = self._weight_array_to_matrix(
                    self.bcast_sources, self.bcast_targets, self.params['synapse_broadcast']['weight'])
        else:
            W_matrix_bcast = np.zeros_like(W_matrix_rec)

        return [W_matrix_in, W_matrix_rec, W_matrix_out, W_matrix_bcast]

    def return_current_weights(self):
        '''
        returns an array of matrices containing the current vaulue of the synaptic weights in the following order:
            in->rec, rec->rec, rec->out, out->rec (broadcast), poisson_generators->rec
        '''
        in_rec = self._extract_weight_matrix(self.params['nrns_dvcs']['in_nrns'], self.params['nrns_dvcs']['rec_nrns'])
        rec_rec = self._extract_weight_matrix(
            self.params['nrns_dvcs']['rec_nrns'],
            self.params['nrns_dvcs']['rec_nrns'])
        rec_out = self._extract_weight_matrix(
            self.params['nrns_dvcs']['rec_nrns'],
            self.params['nrns_dvcs']['out_nrns'])
        return [in_rec, rec_rec, rec_out, self.params['synapse_broadcast']['weight']]

    def return_weight_diff(self):
        '''
        returns an array of matrices of the weight change: (trained weights) - (initial weights).
        The matrices are returned in the following order:
            in->rec, rec->rec, rec->out
        '''
        init_weights = self.return_init_weights()
        trained_weights = self.return_current_weights()
        return [init_weights[i] - trained_weights[i] for i in np.arange(3)]

    def store_network(self, path='./', affix=''):
        '''
        save weight matrices as npy files.
        '''
        if not path[-1] == "/":
            path += "/"

        self.network = {}
        in_nrns = self.params['nrns_dvcs']['in_nrns']
        rec_nrns = self.params['nrns_dvcs']['rec_nrns']
        out_nrns = self.params['nrns_dvcs']['out_nrns']
        self.network['in_nrns'] = np.array(in_nrns)
        self.network['rec_nrns'] = np.array(rec_nrns)
        self.network['out_nrns'] = np.array(out_nrns)
        self.network['in_syns'] = self._extract_senders_targets_weights(in_nrns, rec_nrns)
        self.network['rec_syns'] = self._extract_senders_targets_weights(rec_nrns, rec_nrns)
        self.network['out_syns'] = self._extract_senders_targets_weights(rec_nrns, out_nrns)
        self.network['feedback_syns'] = self._extract_senders_targets_weights(out_nrns, rec_nrns)

        f_name = path + 'network' + affix + '.pkl'
        with open(f_name, 'wb') as f:
            pickle.dump(self.network, f, pickle.HIGHEST_PROTOCOL)

        print('wrote network weights: {}'.format(f_name))

    def generate_frozen_poisson_noise(self, rate=10.0, n_spike_trains=None, remove_silent_generators=False):
        '''
        generates an array of arrays of spike times with Poisson statistics with specified rate. If
        remove_silent_generators is False, the array contains n_rec arrays with spike times. If remove_silent_generators
        is True, spike trains that do not contain any spikes are removed, thus, the number of arrays can be smaller than
        n_rec. If n_spike_trains is provided, then n_rec is replaced by this value. The spike times are in the range
        (0, seq_len).
        !!! Important note !!!
        This function has to be executed before the network is created because it calls nest.ResetKernel().
        '''
        # generate frozen poisson noise using Nest's poisson_generetors
        if n_spike_trains is None:
            n_pg = self.params['n_in']                 # number of poisson generators

        nest.ResetKernel()
        nest.SetKernelStatus({'resolution': self.params['dt'], 'total_num_virtual_procs': self.params['nvp'],
                              'print_time': self.params['print_time']})
        pgs = nest.Create('poisson_generator', n=n_pg, params={'rate': rate})
        prrt_nrns_pg = nest.Create('parrot_neuron', n_pg)
        nest.Connect(pgs, prrt_nrns_pg, {'rule': 'one_to_one'})
        sd = nest.Create('spike_recorder', n_pg)
        nest.Connect(prrt_nrns_pg, sd, {'rule': 'one_to_one'})
        nest.Simulate(self.params['seq_len'])
        counter_empty = 0
        spike_times = []
        for i, ssd in enumerate(nest.GetStatus(sd)):
            t_sd = np.unique(ssd['events']['times'])
            if remove_silent_generators:
                if len(t_sd) == 0:
                    counter_empty += 1
                else:
                    spike_times.append(t_sd)
            else:
                spike_times.append(t_sd)

        if remove_silent_generators:
            print("There are {} silent spike generators".format(counter_empty))
            print("Remove unused poisson generators")
            n_pg -= counter_empty
        return spike_times

    def print_parameters(self, dict_to_print=None, indent=0):
        '''
        print parameters of network (more or less) nicely
        '''
        if dict_to_print is None:
            dict_to_print = self.params
            print('------------------------')
            print('network parameters:')
            print('------------------------')

        for key, value in dict_to_print.items():
            if not isinstance(value, dict):
                print('    ' * indent + '{}:  {}'.format(key, value))
            else:
                print('    ' * indent + '{}:'.format(key))
                self.print_parameters(dict_to_print=value, indent=indent + 1)
