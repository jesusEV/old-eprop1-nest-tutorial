import numpy as np
import os
import matplotlib.pyplot as plt
import nest
from tools import generate_click_task_data
import json

'''
Plot Results
'''
plot = False

'''
paths were results are stored
'''
savepath = 'results/'
if not os.path.exists(savepath):
    os.mkdir(savepath)

datapath = './nest_weights/'
if not os.path.exists(datapath):
    os.mkdir(datapath)

'''
flags
'''
# if True, input, target, and weights loaded from files that were created in a previous simulation
cont_simulation = False
# if True, the weight matrices after training saved as npy and the input + target as a json file
save_weights = False
# if True, the membrane potential of the recurrent neurons recorded
record_rec_nrns = False

'''
parameters
'''
# === task ===
# duration of presenting one cue plus the consecutive break
t_cue_spacing = 150
# number of cues given before decision
n_cues = 7
# duration after cues comprising background noise and recall period
t_post_cues = 1200
# duration of one training period/one example
seq_len = int(t_cue_spacing * n_cues + t_post_cues)
# length of time window for the network to make a decision
recall_duration = 150.
# firing rate of frozen input noise in Hz
f0 = 40.
# firing rate of frozen input noise in kHz
input_f0 = f0 / 1000.
# probability with which one input group is present
p_group = 0.3
# duration of presenting one cue
t_cue = 100
# number of input populations, e.g. 4 = left, right, recall, noise
n_input_symbols = 4
# starting point of recall
recall_start = float(seq_len - recall_duration)

# === simulation parameters ===
# fix numpy random seed
seed = 302687
np.random.seed(seed)
# batch size
n_batch = 64
# number of iterations
n_iter = 20
# number of simulation steps
sim_steps = n_iter*n_batch*seq_len
# temporal resolution of the simulation
resolution = 1.
# dendritic delay of all synapses
delay = resolution
# number of virtual processes for the simulation
nvp = 4

# === neurons  ===
# threshold value of V_m above which the neurons spikes
thr = 0.6
# membrane time constant of recurrent neuron
tau_v = 20.
# membrane time constant of output neuron
tau_out = 20.
# number of refractory time steps
n_ref = 5
# membrane capacitance
C_m = tau_v
# reset value for V_m after a spike
V_reset = 0.
# leak reversal potential
E_L = 0.
# duration of refractory period
t_ref = n_ref*resolution
# leak conductance
g_L = C_m / tau_v
# external current input
I_e = 0.
# initial value of the membrane potential
V_m = 0.

# === adam optimizer ===
# if >= 1. use adam optimizer
use_adam = 1.
# exponential decay rate for the first moment estimate
beta1 = 0.9
# exponential decay rate for the second moment estimate
beta2 = 0.999
# small constant used for numerical stabilization
epsilon = 1.e-8

# === adaptation ===
# time constant of the adaptive threshold
tau_a = 2000.
# prefactor of the adaptive threshold
beta_a = 1.7 * (1. - np.exp(-1 / tau_a)) / (1. - np.exp(-1. / tau_v))

# ==== regularization ===
# prefactor of firing rate regularization
reg_f = 1.
# target firing rate for rate regularization
reg_rate = 10.
# regulariztion rate
regularization_f0 = reg_rate / 1000.

# === network ===
# number of adaptive neurons
n_adaptive = 50
# number of regular neurons
n_regular = 50
# number of total recurrent neurons
n_rec = n_adaptive + n_regular
# number of input neurons
n_in = 40
# number of output neurons
n_out = 2

# === plasticity ===
# learning rate
learning_rate = 0.005
# interval for updating the synapse weights
update_interval = float(seq_len)
# *Je* TODO: include documentation about learning types.
learning_type = 1 # *Je* TODO: improve learning types system (maybe using strings)
# if <= 1. the membrane potentials and spike trains are resetted after each example
keep_traces = 0.

'''
affix for filenames
'''
affix = '_bs{}'.format(n_batch)


def _extract_weight_matrix(pop_pre, pop_post):
    '''
    returns weight matrix W_ij of connection between pop_pre and pop_post, where the first index i
    corresponds to the target and j to the source.

    The shape if the returnd matrix is (len(pop_post), len(pop_pre)).
    '''
    conns_pre_post = nest.GetConnections(pop_pre, pop_post)
    sources = np.array(list(conns_pre_post.sources()))
    targets = np.array(list(conns_pre_post.targets()))
    weights = np.array(conns_pre_post.get('weight'))
    weight_matrix = np.zeros((len(pop_post), len(pop_pre)))
    pop_pre_list = pop_pre.tolist()
    pop_post_list = pop_post.tolist()

    for (s, t, w) in zip(sources, targets, weights):
        j = np.where(np.array(pop_pre_list) == s)[0][0]
        i = np.where(np.array(pop_post_list) == t)[0][0]
        weight_matrix.itemset(i, j, w)

    return weight_matrix


def save_current_weights(in_nrns, rec_nrns, out_nrns, out_rec, path='./', affix=''):
    '''
    save weight matrices as npy files.
    '''
    if not path[-1] == "/":
        path += "/"

    in_rec = _extract_weight_matrix(in_nrns, rec_nrns)
    rec_rec = _extract_weight_matrix(rec_nrns, rec_nrns)
    rec_out = _extract_weight_matrix(rec_nrns, out_nrns)

    np.save(path + 'in_weights' + affix, in_rec)
    print('wrote weights in->rec: {}'.format(path + 'in_weights' + affix))
    np.save(path + 'rec_weights' + affix, rec_rec)
    print('wrote weights rec->rec: {}'.format(path + 'rec_weights' + affix))
    np.save(path + 'out_weights' + affix, rec_out)
    print('wrote weights rec->out: {}'.format(path + 'out_weights' + affix))
    np.save(path + 'broadcast_weights' + affix, out_rec)
    print('wrote broadcast weights: {}'.format(path + 'broadcast_weights' + affix))


def reshape_tf_data(data):
    '''
    Reshape 3D arrays of dimensions (n_batch, seq_len, n_in) to (n_in, n_batch*seq_len) and 2D
    arrays of dimensions (nbatch, seq_len) to (nbatch*seq_len).
    '''
    tf_shape = np.shape(data)
    if len(tf_shape) == 2:
        reshaped_data = np.array(data.reshape(tf_shape[0]*tf_shape[1]), dtype=int)

    elif len(tf_shape) == 3:
        reshaped_data = np.array(np.swapaxes(
            np.swapaxes(data, 0, 1), 0, 2).reshape(tf_shape[2], tf_shape[0]*tf_shape[1]), dtype=int)
    return reshaped_data


def get_data(n_batch):
    '''
    Generate input spikes, sequence of active populations and targets of the evidence accumulation
    task.  '''
    input_spikes, input_nums, target_nums, _ = generate_click_task_data(
        batch_size=n_batch, seq_len=seq_len, n_neuron=n_in, recall_duration=int(recall_duration),
        p_group=p_group, t_cue=t_cue, n_cues=n_cues, t_interval=t_cue_spacing, f0=input_f0,
        n_input_symbols=n_input_symbols)
    return reshape_tf_data(input_spikes), reshape_tf_data(input_nums), reshape_tf_data(target_nums)


'''
reset NEST kernel
'''
nest.ResetKernel()
nest.SetKernelStatus({'resolution': resolution, 'total_num_virtual_procs': nvp, 'print_time': True})

'''
neuron parameters
'''
neuron_params_adaptive = {
    'C_m': C_m,
    't_ref': t_ref,
    'V_reset': V_reset,
    'E_L': E_L,
    'tau_m': tau_v,
    'beta': beta_a,
    'tau_a': tau_a,
    'update_interval': update_interval,
    'V_th': thr,
    'V_m': V_m
    }

neuron_params_regular = {
    'C_m': C_m,
    'tau_m': tau_v,
    't_ref': t_ref,
    'E_L': E_L,
    'V_reset': V_reset,
    'V_m': V_m,
    'update_interval': update_interval,
    'V_th': thr,
    }

readout_params = {
    'C_m': C_m,
    'tau_m': tau_out,
    'E_L': E_L,
    'I_e': I_e,
    'V_m': V_m,
    'learning_type': learning_type,
    'update_interval': update_interval,
    'start': recall_start,
    }

'''
draw random weights
'''
if not cont_simulation:
    out_weights = np.random.uniform(
            -np.sqrt(6. / (n_rec + n_out)), np.sqrt(6. / (n_rec + n_out)), (n_rec, n_out)).T
    in_weights = np.random.randn(n_in, n_rec).T / np.sqrt(n_in)
    rec_weights = np.random.randn(n_rec, n_rec).T / np.sqrt(n_rec)
    np.fill_diagonal(rec_weights, 0.)  # set diagonal to zero (no autapses)
    rand_feedback_weights = np.random.randn(n_rec, n_out)
else:
    in_weights = np.load(datapath + 'in_weights' + affix + '.npy', allow_pickle=True)
    rec_weights = np.load(datapath + 'rec_weights' + affix + '.npy', allow_pickle=True)
    out_weights = np.load(datapath + 'out_weights' + affix + '.npy', allow_pickle=True)
    rand_feedback_weights = np.load(datapath + 'broadcast_weights' + affix + '.npy',
                                    allow_pickle=True)

'''
draw spikes and targets for task
'''

input_spikes_all = np.zeros((n_in, sim_steps))
target_nums_all = np.zeros((n_batch*n_iter))
target_rates = np.zeros((n_out, sim_steps))

for iteration in np.arange(n_iter):
    start = iteration*n_batch
    end = (iteration + 1)*n_batch

    input_spikes, input_nums, target_nums = get_data(n_batch)
    input_spikes_all[:, start*seq_len:end*seq_len] = input_spikes
    target_nums_current = target_nums[::seq_len]
    target_nums_all[start:end] = target_nums_current

    for batch, target_num in enumerate(target_nums_current):
        target_rates[target_num, (start + batch)*seq_len:(start + batch + 1)*seq_len] = 1.

spike_generator_dicts = []
for in_idx in range(n_in):
    spks = input_spikes_all[in_idx]
    spktimes = np.array(np.unique(spks*np.arange(sim_steps))[1:], dtype=np.float32)
    spike_generator_dicts.append({'spike_times': spktimes})

spike_generators = nest.Create('spike_generator', n_in)
nest.SetStatus(spike_generators, spike_generator_dicts)

'''
create neurons, generators and devices
'''
in_nrns = nest.Create('parrot_neuron', n_in)
neurons_reg = nest.Create('iaf_psc_delta_eprop', n_regular, params=neuron_params_regular)
neurons_ad = nest.Create('aif_psc_delta_eprop', n_adaptive, params=neuron_params_adaptive)
rec_nrns = neurons_reg + neurons_ad
out_nrns = nest.Create('error_neuron', n_out, params=readout_params)

target_generators = []
for target_rate in target_rates:
    target_generators.append(
        nest.Create('step_rate_generator', params={'amplitude_times': np.arange(1., sim_steps),
                                                   'amplitude_values': target_rate[:-1]}))
mm_out_params = {'interval': resolution,
                 'record_from': ['V_m', 'len_eprop_hist', 'learning_signal']}

if record_rec_nrns:
    mm_rec = nest.Create("multimeter", params=mm_out_params)
    sd = nest.Create('spike_recorder')
    sd_in = nest.Create('spike_recorder')

mm_out = nest.Create('multimeter', params=mm_out_params)

'''
create connections
'''


def create_Wmin_Wmax(weight_matrix):
    Wmin = np.zeros_like(weight_matrix)
    Wmax = np.zeros_like(weight_matrix)

    signs = np.sign(weight_matrix)

    Wmin[np.where(signs == -1.)] = -100.
    Wmax[np.where(signs == 1.)] = 100.
    return Wmin, Wmax


Wmin_in, Wmax_in = create_Wmin_Wmax(in_weights)
Wmin_rec, Wmax_rec = create_Wmin_Wmax(rec_weights)
Wmin_out, Wmax_out = create_Wmin_Wmax(out_weights)

synapse_in = {'synapse_model': 'eprop_synapse', 'learning_rate': learning_rate,
              'weight': in_weights, 'delay': delay, 'update_interval': update_interval,
              'tau_decay': tau_out, 'keep_traces': keep_traces, 'target_firing_rate': reg_rate,
              'rate_reg': reg_f, 'batch_size': float(n_batch), 'use_adam': use_adam,
              'beta1_adam': beta1, 'beta2_adam': beta2, 'epsilon_adam': epsilon,
              'recall_duration': recall_duration, 'Wmin': Wmin_in, 'Wmax': Wmax_in}
synapse_rec = {'synapse_model': 'eprop_synapse', 'learning_rate': learning_rate,
               'weight': rec_weights, 'delay': delay, 'update_interval': update_interval,
               'tau_decay': tau_out, 'keep_traces': keep_traces, 'target_firing_rate': reg_rate,
               'rate_reg': reg_f, 'batch_size': float(n_batch), 'use_adam': use_adam,
               'beta1_adam': beta1, 'beta2_adam': beta2, 'epsilon_adam': epsilon,
               'recall_duration': recall_duration, 'Wmin': Wmin_rec, 'Wmax': Wmax_rec}
synapse_out = {'synapse_model': 'eprop_synapse', 'weight': out_weights, 'delay': delay,
               'update_interval': update_interval, 'keep_traces': keep_traces,
               'learning_rate': learning_rate, 'batch_size': float(n_batch), 'use_adam': use_adam,
               'beta1_adam': beta1, 'beta2_adam': beta2, 'epsilon_adam': epsilon,
               'recall_duration': recall_duration,
               'Wmin': Wmin_out, 'Wmax': Wmax_out}
synapse_broadcast = {'synapse_model': 'learning_signal_connection_delayed',
                     'weight': rand_feedback_weights, 'delay': delay}
synapse_out_out = {'synapse_model': 'rate_connection_delayed', 'weight': 1., 'delay': delay,
                   'receptor_type': 1}
synapse_target = {'synapse_model': 'rate_connection_delayed', 'weight': 1., 'delay': delay,
                  'receptor_type': 2}
static_synapse = {'synapse_model': 'static_synapse', 'weight': 1., 'delay': delay}

# NEST does not support a direct connection of devises to neurons via plastic synapses. This is why
# we have the parrot neurons between the spike generators and the recurrent network. Parrot neurons
# simply send a spike whenever they are receiving one.
conn_ata = {'rule': 'all_to_all', 'allow_autapses': False}
nest.Connect(spike_generators, in_nrns, syn_spec=static_synapse, conn_spec={'rule': 'one_to_one'})
nest.Connect(in_nrns, rec_nrns, syn_spec=synapse_in, conn_spec=conn_ata)
nest.Connect(rec_nrns, rec_nrns, syn_spec=synapse_rec, conn_spec=conn_ata)
nest.Connect(rec_nrns, out_nrns, syn_spec=synapse_out, conn_spec=conn_ata)
nest.Connect(out_nrns, rec_nrns, syn_spec=synapse_broadcast, conn_spec=conn_ata)
nest.Connect(out_nrns, out_nrns, syn_spec=synapse_out_out, conn_spec=conn_ata)

for i in range(n_out):
    nest.Connect(target_generators[i], out_nrns[i], syn_spec=synapse_target)

if record_rec_nrns:
    nest.Connect(mm_rec, rec_nrns, syn_spec=static_synapse)
    nest.Connect(rec_nrns, sd, syn_spec=static_synapse)
    nest.Connect(in_nrns, sd_in, syn_spec=static_synapse)

nest.Connect(mm_out, out_nrns, syn_spec=static_synapse)

# need to reshape since nest.Connect destroys the original shape
rand_feedback_weights = np.reshape(rand_feedback_weights, (n_rec, n_out))

'''
run simulation
'''
nest.Simulate(sim_steps + 2.*delay)  # add extra simulation time at the end to comensate delays

'''
process data of recording devices
'''
times_all = np.arange(sim_steps)

events_out = nest.GetStatus(mm_out)[0]['events']
senders_out = events_out['senders']

Vms_out = np.array(
    [events_out['V_m'][np.where(senders_out == i)][1:1 + sim_steps] for i in out_nrns.tolist()])

output_logits = Vms_out.reshape(n_out, n_iter, n_batch, seq_len)[:, :, :, -int(recall_duration):]
vms_out = Vms_out.reshape(n_out, n_iter, n_batch, seq_len)

if record_rec_nrns:
    events_rec = nest.GetStatus(mm_rec)[0]["events"]
    senders_rec = events_rec['senders']
    Vms_rec = \
        [events_rec['V_m'][np.where(senders_rec == i)][1:1 + sim_steps] for i in rec_nrns.tolist()]
    learning_signals_rec = \
        [events_rec['learning_signal'][np.where(senders_rec == i)][1:1 + sim_steps]
         for i in rec_nrns.tolist()]

    spikes_in = nest.GetStatus(sd_in, "events")[0]
    in_spike_times_all = np.array(spikes_in['times']) - 2.*delay
    senders_spikes_in = spikes_in['senders']

    spikes_recurrent = nest.GetStatus(sd, "events")[0]
    rec_spike_times_all = np.array(spikes_recurrent['times']) - 2.*delay
    senders_spikes_rec = spikes_recurrent['senders']
    results_dict = {
        'spikes_in': in_spike_times_all.tolist(),
        'senders_in': senders_spikes_in.tolist(),
        'spikes_rec': rec_spike_times_all.tolist(),
        'senders_rec': senders_spikes_rec.tolist(),
        'learning_signals_rec': [ls.tolist() for ls in learning_signals_rec],
        }

    with open(savepath + 'spikes_evidence_acc.json', 'w') as f:
        json.dump(results_dict, f)

'''
calculate recall errors
'''
targets_reshaped = target_nums_all.reshape(n_iter, n_batch)
tiled_targets = \
    target_rates.reshape(n_out, n_iter, n_batch, seq_len)[:, :, :, -int(recall_duration):]

# sum over output neurons in denominator
softmax_y = np.exp(output_logits) / np.sum(np.exp(output_logits), axis=0)
# sum over output neurons, mean over
loss = - np.mean(np.sum(tiled_targets * np.log(softmax_y), axis=0), axis=(1, 2))

# recall and batches (n_iter)

# average over time, argmax over neurons
y_predict = np.argmax(np.mean(output_logits, axis=3), axis=0)
# averaged over batch here
accuracy = np.mean((targets_reshaped == y_predict), axis=1)
recall_errors = 1. - accuracy
print('\naccuracy: {}'.format(accuracy))

results_dict = {
    'recal_errors': recall_errors.tolist(),
    'loss': loss.tolist(),
    'output_logits': output_logits.tolist(),
    'vms_out': vms_out.tolist(),
    }

with open(savepath + 'results_evidence_acc.json', 'w') as f:
        json.dump(results_dict, f)

'''
plot results
axA: recall errors over training
axB: loss over training
'''
if plot:
    lw = 3.
     
    fig1, (axA, axB) = plt.subplots(2, figsize=(15, 8))
      
    axA.plot(np.arange(len(recall_errors)), recall_errors, lw=lw)
    axA.set_xticks(np.arange(len(recall_errors)))
    axA.set_xlabel('iterations')
    axA.set_ylabel('recall errors')
      
    axB.plot(np.arange(len(loss)), loss, lw=lw)
    axB.set_xticks(np.arange(len(recall_errors)))
    axB.set_xlabel('iterations')
    axB.set_ylabel('loss')
      
    fig1.tight_layout()
    plt.show()
      
    if save_weights:
        save_current_weights(in_nrns, rec_nrns, out_nrns, rand_feedback_weights, path=datapath,
                             affix=affix)
